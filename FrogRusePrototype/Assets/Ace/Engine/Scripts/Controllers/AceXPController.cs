﻿using UnityEngine;
using System.Collections;

public class AceXPController : AceController
{
    #region Constants
    public delegate void OnXPUpdated(uint level, float normalizedProgress);
    #endregion

    #region Variables
    [SerializeField] AceDatabaseCommunicator m_DB;
	[SerializeField] AceAccountController m_AccountController;
	OnXPUpdated m_OnXPUpdated;
    #endregion

    #region Functions

	public void AddXP(uint xpGained)
	{
		AddXP(AceAccountController.Account.Email, xpGained);
	}

    public void AddXP(string playerEmail, uint xpGained)
    {
		if (!string.IsNullOrEmpty(playerEmail))
		{
			m_DB.AddXP(playerEmail, xpGained, OnAddedXP);
		}  
    }

	public void AddOnLevelUpdatedCB(OnXPUpdated cb)
    {
		m_OnXPUpdated += cb;
    }

	void OnAddedXP(AceXPResponse response)
    {
		Debug.Log(string.Format("PrevXP {0} CurrXP {1} AddedXP {2}", response.PrevXP, response.CurrXP, response.AddedXP));

        // set current xp in player data
		//m_AccountController.AddXP(response.AddedXP);
		m_AccountController.SetXPData(response.CurrXP, response.CurrLevel, response.NormalizedProgressToNextLevel);
		
        // check for level up
        //CalculateLevel(response.CurrXP);

		BroadcastPlayerXPData();
    }

	public void BroadcastPlayerXPData()
	{
		if (m_OnXPUpdated != null)
		{
			AcePlayerAccount player = AceAccountController.Account;
			m_OnXPUpdated(player.Level, player.NormalizedLevelProgress);
		}
	}

    /*void CalculateLevel(uint totalXP)
    {
        //float normalizedLevelProgress;
        //LevelData.Level level = LevelData.CalculateLevel(totalXP, out normalizedLevelProgress);

        // update listeners
        if (m_OnXPUpdated != null)
        {
            m_OnXPUpdated(level.Id, normalizedLevelProgress);
        }
    }*/
    #endregion
}
