using UnityEngine;
using System.Collections;

public class AceGameLevelController : AceController
{
	#region Variables
	[SerializeField] AceDatabaseCommunicator m_DBCommunicator;
	uint m_CurrentLevel = 0;
	#endregion

	#region Properties
	public uint CurrentLevel { get { return m_CurrentLevel; } } 
	#endregion

	#region Functions
	public void SetCurrentLevel(uint level)
	{
		m_CurrentLevel = level;
	}

	public void SaveGameLevelProgress(string username, string internalGameName, uint level, AceDatabaseCommunicator.OnReceivedGameLevelProgress cb)
	{
		if (!string.IsNullOrEmpty(username))
		{
			m_DBCommunicator.SetGameLevelProgress(username, internalGameName, level, cb != null ? cb : OnSetGameLevelProgress);
		}
	}

	void OnSetGameLevelProgress(AceGameLevelResponse levelResponse)
	{
		Debug.Log(levelResponse.InternalGameName + " " + levelResponse.Level);
	}

	public void LoadGameLevelProgress(string username, string internalGameName, AceDatabaseCommunicator.OnReceivedGameLevelProgress cb)
	{
		m_DBCommunicator.GetGameLevelProgress(username, internalGameName, cb);
	}
	#endregion
}
