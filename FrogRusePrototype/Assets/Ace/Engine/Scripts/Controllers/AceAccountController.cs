using UnityEngine;
using System.Collections;

public class AceAccountController : AceController 
{
	#region Variables
	[SerializeField] AceDatabaseCommunicator m_DBCommunicator;
	[SerializeField] AceMenuController m_MenuController;
	[SerializeField] AcePlayServiceGame m_PlayService;
	static AcePlayerAccount m_PlayerAccount = new AcePlayerAccount(string.Empty, string.Empty, string.Empty);
	#endregion

	#region Properties
	public static AcePlayerAccount Account { get { return m_PlayerAccount; } }
	#endregion

	#region Functions
	public void CreateAccount(string email, string password, string username)
	{
		m_DBCommunicator.CreateAccount(email, password, username, OnCreatedAccount);
	}

	void OnCreatedAccount(AcePlayerAccount account)
	{
		Debug.Log("OnCreatedAccount");
	}

	public void AddXP(uint xp)
	{
		m_PlayerAccount.XP += xp;
	}

	public void Login(string username, string password)
	{
		m_DBCommunicator.Login(username, password, OnLoggedIn);
	}

	void OnLoggedIn(AcePlayerAccount account)
	{
		m_PlayerAccount = account;
		m_MenuController.PushMenu(0);
		m_PlayService.GetAllAchievements();
	}

	public void SetXPData(uint xp, uint level, float normalizedLevelProgess)
	{
		m_PlayerAccount.SetXPData(xp, level, normalizedLevelProgess);
	}
	#endregion
}
