﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class AceLabel : AceMenuItem
{
	#region Variables
	[SerializeField]
	Text m_Text;
	#endregion

	#region Properties
	public string Text
	{
		get { return m_Text.text; }
		set { m_Text.text = value; }
	}

	public Color color 
	{
		get { return m_Text.color; }
		set { m_Text.color = value; }
	}
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		if (m_Text == null)
		{
			m_Text = GetComponent<Text>();
		}
	}

	#endregion
}
