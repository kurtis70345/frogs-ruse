using UnityEngine;
using System.Collections;

public class AceValueModifierWidget : MonoBehaviour
{
	#region Variables
	[SerializeField] AceLabel m_Title;
	[SerializeField] AceLabel m_Value;
	[SerializeField] AceButton m_MinusButton;
	[SerializeField] AceButton m_PlusButton;
	[SerializeField] float m_ModAmount = 1;
	[SerializeField] float m_CurrentValue = 0;
	[SerializeField] float m_MinValue = 0;
	[SerializeField] float m_MaxValue = 100;
	[SerializeField] string m_Precision = "f0";
	#endregion

	#region Properties
	public float CurrentValue
	{
		get { return m_CurrentValue; }
		set
		{
			m_CurrentValue = value;
			UpdateValue();
		}
	}
	#endregion

	#region Functions
	public void Decrement()
	{
		m_CurrentValue -= m_ModAmount;
		UpdateValue();
	}

	public void Increment()
	{
		m_CurrentValue += m_ModAmount;
		UpdateValue();
	}

	void UpdateValue()
	{
		m_CurrentValue = Mathf.Clamp(m_CurrentValue, m_MinValue, m_MaxValue);
		m_Value.Text = m_CurrentValue.ToString(m_Precision);
	}
	#endregion
}
