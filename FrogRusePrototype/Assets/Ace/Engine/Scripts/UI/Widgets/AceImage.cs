﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AceImage : AceMenuItem
{
	#region Variables
	[SerializeField] Image m_Image;
	RectTransform m_RectTransform;
	#endregion

	#region Properties
	public Rect Dimensions
	{
		get { return m_RectTransform.rect; }
	}

	public Color color
	{
		get { return m_Image.color; }
		set { m_Image.color = value; }
	}

	public Sprite sprite
	{
		get { return m_Image.sprite; }
	}

	public float Width
	{
		get { return m_RectTransform.sizeDelta.x; }
		set { m_RectTransform.sizeDelta = new Vector2(value, m_RectTransform.sizeDelta.y); }
	}

	public float Height
	{
		get { return m_RectTransform.sizeDelta.y; }
		set { m_RectTransform.sizeDelta = new Vector2(m_RectTransform.sizeDelta.x, value); }
	}

	public bool IsRayCastTarget
	{
		get { return m_Image.raycastTarget; }
		set { m_Image.raycastTarget = value; } 
	}
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		m_RectTransform = GetComponent<RectTransform>();
		if (m_Image == null)
		{
			m_Image = GetComponent<Image>();
		}
	}

	public void SetDimensions(float width, float height)
	{
		//Rect r = m_RectTransform.rect;
		//m_RectTransform.rect.Set(r.x, r.y, width, height);
		m_RectTransform.sizeDelta = new Vector2(width, height);
	}
	
	#endregion
}
