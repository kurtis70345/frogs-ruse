﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(InputField))]
public class AceInputField : AceMenuItem 
{
	#region Variables
	[SerializeField] InputField m_Input;
	#endregion

	#region Properties
	public string InputText
	{
		get { return m_Input.text; }
		set { m_Input.text = value; }
	}

	public bool IsInteractable 
	{
		get { return m_Input.interactable; }
		set { m_Input.interactable = value; }
	}
	#endregion

	#region Functions
	
	#endregion
}
