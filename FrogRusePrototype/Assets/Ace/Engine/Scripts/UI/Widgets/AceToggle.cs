using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Toggle))]
public class AceToggle : AceMenuItem
{
    #region Variables
    public Toggle m_ToggleButton;
	public AceImage m_Background;
	public AceImage m_CheckMark;
	public Text m_Label;
    #endregion

    #region Properties
	public Toggle Button
    {
        get { return m_ToggleButton; }
    }
	
	public bool IsChecked
	{
		get { return Button.isOn; }
		set { Button.isOn = value; }
	}
	
	public AceImage Background
	{
		get { return m_Background; }	
	}
	
	public AceImage CheckMark
	{
		get { return m_CheckMark; }	
	}
	
	public string Text
	{
		get { return m_Label.text; }
		set {m_Label.text = value; }
	}

	public bool IsInteractable 
	{
		get { return m_ToggleButton.interactable; }
		set { m_ToggleButton.interactable = value; }
	}

    #endregion

    #region Functions

    public void SetState(int index)
    {
		IsChecked = index == 0 ? false : true;
    }

    /*public void SetCheckMarkImage(string imageName)
    {
        m_CheckMark.spriteName = imageName;
    }*/

    /*public void SetBackgroundImage(string imageName)
    {
        m_Background.spriteName = imageName;
        //m_Background.MakePixelPerfect();
    }*/

    public void SetEnabled(bool bEnabled)
    {
        //m_Background.Color = bEnabled ? Color.white : DisabledColor;
        //m_ToggleButton.GetComponent<Collider>().enabled = bEnabled;
    }

	public void SetToggleGroup(ToggleGroup group)
	{
		m_ToggleButton.group = group;
	}

public void SetCheckedWithoutCallback(bool isChcked)
	{
		int listenerCount = m_ToggleButton.onValueChanged.GetPersistentEventCount();
		for (int i = 0; i < listenerCount; i++)
		{
			m_ToggleButton.onValueChanged.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
		}
		IsChecked = isChcked;
		for (int i = 0; i < listenerCount; i++)
		{
			m_ToggleButton.onValueChanged.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
		}
	}
    #endregion
}
