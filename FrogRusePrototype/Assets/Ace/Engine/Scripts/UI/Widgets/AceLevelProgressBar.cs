using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AceLevelProgressBar : AceProgressBar
{
    #region Variables
	[SerializeField] AceLabel m_CurrLevelText;
	[SerializeField] AceLabel m_NextLevelText;

    #endregion

    #region Functions
	public void SetLevels(uint currLevel, uint nextLevel)
	{
		m_CurrLevelText.Text = currLevel.ToString();
		m_NextLevelText.Text = nextLevel.ToString();
	}
    #endregion
}
