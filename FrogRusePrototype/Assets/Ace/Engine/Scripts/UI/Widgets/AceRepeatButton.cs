﻿using UnityEngine;

public class AceRepeatButton : MonoBehaviour
{
    public float interval = 0.125f;

    bool mIsPressed = false;
    float mNextClick = 0f;

    void OnPress(bool isPressed) { mIsPressed = isPressed; mNextClick = Time.realtimeSinceStartup + interval; }

    void Update()
    {
        if (mIsPressed && Time.realtimeSinceStartup >= mNextClick)
        {
            mNextClick = Time.realtimeSinceStartup + interval;

            // Do what you need to do, or simply:
            SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
        }
    }
}