﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(AceImage))]
[RequireComponent(typeof(Button))]
public class AceButton : AceMenuItem
{
	#region Variables
	[SerializeField]
	AceImage m_Image;

	[SerializeField]
	Button m_Button;

	[SerializeField]
	AceLabel m_Text;
	#endregion
	
	#region Properties
	public Rect Dimensions { get { return m_Image.Dimensions; } }
	public string Text
	{
		get { return m_Text.Text; }
		set { m_Text.Text = value; }
	}

	public bool IsInteractable 
	{
		get { return m_Button.interactable; }
		set { m_Button.interactable = value; }
	}
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		if (m_Image == null)
		{
			m_Image = GetComponent<AceImage>();
		}

		if (m_Button == null)
		{
			m_Button = GetComponent<Button>();
		}


	}

	public void AddOnClick(UnityAction clickAction)
	{
		m_Button.onClick.AddListener(clickAction);
	}

	public void Select()
	{
		m_Button.Select();
	}
	#endregion
}
