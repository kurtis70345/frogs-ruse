﻿using UnityEngine;
using System.Collections;

public class AceMenuItem : MonoBehaviour 
{
	#region Variables

	#endregion

	#region Functions
	public void Show() { gameObject.SetActive(true); }
	public void Hide() { gameObject.SetActive(false); }
	public void Hide(bool tf) { gameObject.SetActive(!tf); }
	#endregion
}
