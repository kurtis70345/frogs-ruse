﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES
public class AceComboBox : AceMenuItemSprite
{	
	#region Variables
	public UIPopupList m_ComboBox;
	#endregion
	
	#region Properties
	public UIPopupList ComboBox
	{
		get { return m_ComboBox; }	
	}
	
	public bool IsOpen
	{
		get { return ComboBox.isOpen; }	
	}
	
	public string Selection
	{
		get { return ComboBox.value; }	
	}
	
	public int NumItems
	{
		get { return ComboBox.items.Count; }	
	}
	#endregion
	
	#region Function
	// Use this for initialization
	public override void Awake()
	{
		m_Transform = transform;
		m_ComboBox = GetComponentInChildren<UIPopupList>();
		if (m_ComboBox == null)
		{
			Debug.LogError(string.Format("AceMenuItemComboBox {0} doesn't have a UIPopupList", name));
		}
	}
	
	public void AddItem(string itemName)
	{
		ComboBox.items.Add(itemName);
	}

    public void InsertItem(int index, string itemName)
    {
        if (index < 0 || index >= ComboBox.items.Count)
        {
            AddItem(itemName);
        }
        else
        {
            ComboBox.items.Insert(index, itemName);
        }
    }
	
	public void RemoveItem(string item)
	{
		ComboBox.items.Remove(item);	
	}
	
	public void RemoveAllItems()
	{
		ComboBox.items.Clear();
	}
	
	public void SetSelection(string selection)
	{
		ComboBox.value = selection;
	}
	
	public void SetSelection(int selection)
	{
		SetSelection(ComboBox.items[selection]);
	}
	
	public void SetItem(int index, string itemName)
	{
		if (index < 0 || index >= NumItems)
		{
			Debug.LogError(string.Format("SetItem failed on Combobox {0} using index {1}", name, index));
			return;
		}
		
		ComboBox.items[index] = itemName;	
	}
	
	public int GetSelectedIndex()
	{
		return FindIndexOfItem(Selection);
	}
	
	public int FindIndexOfItem(string item)
	{
		return ComboBox.items.FindIndex(s => s == item);
	}
	public string GetItem(int index)
	{
		if (index < 0 || index >= NumItems)
		{
			Debug.LogError(string.Format("GetItem failed on Combobox {0} using index {1}", name, index));
			return string.Empty;
		}
		
		return ComboBox.items[index];	
	}
	#endregion
}

#endif
