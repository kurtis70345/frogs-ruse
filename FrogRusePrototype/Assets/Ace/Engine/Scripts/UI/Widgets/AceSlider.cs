using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Slider))]
public class AceSlider : AceMenuItem
{
	#region Variables
	Slider m_Slider;
	#endregion
	
	#region Properties
	public Slider Slider
	{
		get { return m_Slider; }
	}
	#endregion
	
	#region Functions
	// Use this for initialization
	public void Awake()
    {
		m_Slider = GetComponent<Slider>();
		if (m_Slider == null)
		{
			Debug.LogError(string.Format("MenuItemSlider {0} doesn't have a Slider component", name));	
		}
	}
	
	public void SetValue(float val)
	{
		val = Mathf.Clamp01(val);
		Slider.value = val;
	}
	#endregion
}
