using UnityEngine;
using System.Collections;

public class AceScreenFader : MonoBehaviour 
{
	//--------------------------------------------------------------------
	//                        Public parameters
	//--------------------------------------------------------------------
	public Texture fadeTexture;
	int drawDepth = -1000;
	
	//--------------------------------------------------------------------
	//                       Private variables
	//--------------------------------------------------------------------
	private float alpha = 1.0f; 	
	private bool m_bIsFading = false;
	private Color m_Color = Color.white;
	private Rect m_Position;
	
	// Use this for initialization
	void Start()
	{
		alpha = 1;
		m_Position = new Rect(0, 0, Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void OnGUI() 
	{
		if (!m_bIsFading)
		{
			return;	
		}
		
	    alpha = Mathf.Clamp01(alpha);   
	    
		m_Color.a = alpha;
		GUI.color = m_Color;
	    GUI.depth = drawDepth;
		//GUI.color.a = alpha;
		
		//fadeTexture.
	    GUI.DrawTexture(m_Position, fadeTexture);
	}
	
	//--------------------------------------------------------------------
	//                       Runtime functions
	//--------------------------------------------------------------------
	
	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	
	public void FadeIn(float fadeTime)
	{
		StopCoroutine("StartFading");
		m_bIsFading = true;
		StartCoroutine(StartFading(1, 0, fadeTime));
	}
	
	//--------------------------------------------------------------------
	
	public void FadeOut(float fadeTime)
	{    
		StopCoroutine("StartFading");
		m_bIsFading = true;
		StartCoroutine(StartFading(0, 1, fadeTime));
	}
	
	IEnumerator StartFading(float a, float b, float t)
	{
		alpha = a;
		float timePassed = 0;
		while (timePassed < t)
		{
			alpha = Utils.Interpolate(Utils.InterpolationStyle.Smooth_Step, a, b, timePassed / t);
			alpha = Mathf.Clamp01(alpha);
			timePassed += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		
		if (alpha <= 0)
		{
			m_bIsFading = false;
		}
		
		GUI.color = Color.white;
	}

}


