using UnityEngine;
using System;
using System.Collections.Generic;
/*
abstract public class AceMenuItemEffect {

    public enum EffectMessageType
    {
        Move_Pivot,

        NUM_MESSAGE_TYPES
    };
	

    public struct EffectMessage
    {
        // the type of message sent
        public EffectMessageType m_Type;

        // the data sent in the message
        public object m_Data;

        public EffectMessage(EffectMessageType type)
        {
            m_Type = type;
            m_Data = null;
        }

        public EffectMessage(EffectMessageType type, object Data)
        {
            m_Type = type;
            m_Data = Data;
        }
    }

    // -----> Variables
    public delegate void OnEffectFinishedCallback(AceMenuItem effectItem, AceMenuItemEffect effect);

    protected AceMenuItem m_EffectedItem;  
    protected Vector3 m_InterpolatedValue = new Vector3();
	protected Utils.InterpolationStyle m_InterStyle = Utils.InterpolationStyle.Lerp;
	List<OnEffectFinishedCallback> m_OnFinishedCallbacks;
    float m_fSpeed = 1.0f;
    float m_fAccumSpeed = 0;
    float m_fCompletionTime = 1.0f;
    float m_fTimePassed = 0;
    float m_fDelay = -1;
    float m_fDelayTimer = 0;
	
	protected float TimePassed
	{
		get { return m_fTimePassed; }	
	}

    public AceMenuItemEffect(AceMenuItem effectedItem, float fCompletionTime, OnEffectFinishedCallback cb)
    {
        m_EffectedItem = effectedItem;

        // how long it will take for this effect to have performed it's duties
        m_fSpeed = 1.0f / fCompletionTime;
        m_fCompletionTime = fCompletionTime;

        // setup the callback
		AddOnFinishedCallback(cb);
    }

    public AceMenuItemEffect(AceMenuItem effectedItem)
    {
        m_EffectedItem = effectedItem;
    }
	
	// Update is called once per frame
	public virtual bool Update() 
    {
        m_fDelayTimer += Time.deltaTime;

        if (!DelayTimerFinished())
        {
            return false;       
        }

        m_fAccumSpeed += m_fSpeed * Time.deltaTime;
        m_fTimePassed += Time.deltaTime;
        if (IsEffectFinished())
        {
            OnEffectFinished();
			return false;
        }

        return true;
	}

    protected bool DelayTimerFinished()
    {
        return m_fDelayTimer >= m_fDelay;
    }

    protected virtual bool IsEffectFinished()
    {
        return m_fTimePassed >= m_fCompletionTime;
    }

    protected float GetTimeRatio()
    {
        return Mathf.Clamp01(m_fTimePassed / m_fCompletionTime);
    }

    protected virtual void OnEffectFinished()
    {
        if (m_OnFinishedCallbacks != null)
        {
			for (int i = 0; i < m_OnFinishedCallbacks.Count; i++)
			{
				m_OnFinishedCallbacks[i](m_EffectedItem, this);
			}
        }
        
        //m_EffectedItem.RemoveEffect(this);
    }

    public virtual void ReceiveMessage(EffectMessage message) { }

    public virtual void Add(AceMenuItemEffect effect)
    {
        Debug.LogError("You can only Add MenuItemEffect in MenuItemEffect_Composite");
    }

    public virtual void Remove(AceMenuItemEffect effect)
    {
        Debug.LogError("You can only Remove MenuItemEffect in MenuItemEffect_Composite");
    }
	
	public virtual void RemoveAll()
	{
		Debug.LogError("You can only RemoveAll MenuItemEffect in MenuItemEffect_Composite");
	}
	
	public virtual bool HasEffect(Type t)
	{
		Debug.LogError("You can only call HasEffect in MenuItemEffect_Composite");
		return false;
	}

    public void AddOnFinishedCallback(OnEffectFinishedCallback cb)
    {
		if (cb == null)
		{
			return;	
		}
		
		if (m_OnFinishedCallbacks == null)
		{
			m_OnFinishedCallbacks = new List<OnEffectFinishedCallback>();	
		}
		
		m_OnFinishedCallbacks.Add(cb);
    }
	
	public void ClearFinishedCallbacks()
	{
		if (m_OnFinishedCallbacks != null)
		{
			m_OnFinishedCallbacks.Clear();
		}
	}

    public void SetDelayTime(float delayTime)
    {
        m_fDelay = delayTime;
    }
	
	
	public virtual void Reset()
	{
		m_fDelayTimer = 0;
		m_fTimePassed = 0;
	}
	
	public float PercentCompleted
	{
		get { return Mathf.Clamp01(m_fTimePassed / m_fCompletionTime); }
	}
	
	public Utils.InterpolationStyle Interpolation
	{
		get { return m_InterStyle; }
		set { m_InterStyle = value; }
	}
	
	
}

public class MenuItemEffect_Orbit : AceMenuItemEffect
{
    public struct Params
    {
        // the position being orbited around
        public Vector3 m_PivotPoint;

        public float m_fDistanceFromPivot;

        public Utils.Axis m_RotationAxis;
    };

    Params m_Params;// = new Params();
    Vector3 m_Position = new Vector3();
    Vector3 m_CurrentDirection = new Vector3();

    public MenuItemEffect_Orbit(AceMenuItem effectedItem, float fCompletionTime, Params effectParams, 
        Vector3 targetDirection, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
        Init(effectedItem, effectParams);

        m_InterpolatedValue.z = Utils.AngleBetween(m_CurrentDirection, targetDirection);
    }

    public MenuItemEffect_Orbit(AceMenuItem effectedItem, float fCompletionTime, Params effectParams, 
        float fAmountToRotate, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
        Init(effectedItem, effectParams);

        m_InterpolatedValue.z = fAmountToRotate;
    }

    void Init(AceMenuItem effectedItem, Params effectParams)
    {
        m_Params = effectParams;
        m_Position = effectedItem.GetPosition();

        // establish current direction
        m_CurrentDirection = m_Position - m_Params.m_PivotPoint;
        m_CurrentDirection.Normalize();

        m_InterpolatedValue.y = 0;
		
		m_InterStyle = Utils.InterpolationStyle.Ease_Out;
    }

    public override bool Update()
    {
        if (!base.Update())
        {
            return false;
        }

        //m_InterpolatedValue.x = Mathf.LerpAngle(m_InterpolatedValue.y, m_InterpolatedValue.z, GetTimeRatio());
		m_InterpolatedValue.x = Utils.Interpolate(m_InterStyle, m_InterpolatedValue.y, m_InterpolatedValue.z, GetTimeRatio());
		
        // rotate vector
        Vector3 rotatedDirection = Utils.RotateVector(m_Params.m_RotationAxis, m_InterpolatedValue.x, m_CurrentDirection, true);

        // move away from the pivot in the direction of the rotated vector
        m_Position = m_Params.m_PivotPoint + rotatedDirection * m_Params.m_fDistanceFromPivot;

        m_EffectedItem.SetPosition(m_Position);
        return true;
    }

    //public override void ReceiveMessage(EffectMessage message)
    //{
    //    switch (message.m_Type)
    //    {
    //        case EffectMessageType.Move_Pivot:
    //            m_Params.m_PivotPoint = (Vector3)message.m_Data;
    //            break;
    //    }
    //}
}

public class MenuItemEffect_Move : AceMenuItemEffect
{
    Vector3 m_Position = new Vector3();
    Vector3 m_EndPosition;

    public MenuItemEffect_Move(AceMenuItem effectedItem, float fCompletionTime, Vector3 targetPos,
        bool bMoveRelative, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
		//Interpolation = Utils.InterpolationStyle.Lerp;
		Interpolation = Utils.InterpolationStyle.Smooth_Step;
        m_Position = effectedItem.GetPosition();
        
        SetMovePosition(targetPos, bMoveRelative);
    }
	
	public void SetMovePosition(Vector3 targetPos, bool bMoveRelative)
	{
		m_Position = m_EffectedItem.GetPosition();
		if (!bMoveRelative)
        {
            m_EndPosition = targetPos;
        }
        else
        {
            m_EndPosition = m_Position + targetPos;
        } 
	}

    public override bool Update()
    {
        if (!base.Update())
        {
			if (DelayTimerFinished())
			{
				m_EffectedItem.SetPosition(m_EndPosition);
			}
            return false;
        }

		m_InterpolatedValue = Utils.Interpolate(m_InterStyle, m_Position, m_EndPosition, GetTimeRatio());
        m_EffectedItem.SetPosition(m_InterpolatedValue);
        return true;
    }
}

public class MenuItemEffect_Curve : AceMenuItemEffect
{
    Vector3 m_Position;
    Vector3 m_MidPosition;
    Vector3 m_EndPosition;
    //Utils.Axis m_CurveAxis = Utils.Axis.Y_Axis;

    public MenuItemEffect_Curve(AceMenuItem effectedItem, float fCompletionTime, Vector3 targetPos, float fCurveHeight,
        bool bMoveRelative, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
        m_Position = effectedItem.GetPosition();
        
        if (!bMoveRelative)
        {
            m_EndPosition = targetPos;
        }
        else
        {
            m_EndPosition = m_Position;
            m_EndPosition += targetPos;
        }

        Vector3 toTarget = m_EndPosition - m_Position;
        float rotAmount = fCurveHeight < 0 ? -90 : 90;
        toTarget = Utils.RotateVector(Utils.Axis.Z_Axis, rotAmount, toTarget, true);
        m_MidPosition = Vector3.Lerp(m_Position, m_EndPosition, 0.5f);
        m_MidPosition += toTarget.normalized * fCurveHeight;
    }

    public override bool Update()
    {
        if (!base.Update())
        {
            return false;
        }

        m_InterpolatedValue = Utils.FollowCurve(m_Position, m_MidPosition, m_EndPosition, GetTimeRatio());
        //m_InterpolatedValue.y = Screen.height - m_InterpolatedValue.y;
        m_EffectedItem.SetPosition(m_InterpolatedValue);
        return true;
    }
}

public class MenuItemEffect_Scale : AceMenuItemEffect
{
    // ------> Variables
    //Vector2 m_CurrSize = new Vector2();
    //Vector2 m_TargetSize = new Vector2();

    public MenuItemEffect_Scale(AceMenuItem effectedItem, float fCompletionTime, float fTargetScale, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
		m_InterStyle = Utils.InterpolationStyle.Ease_Out;
        //m_CurrSize = effectedItem.GetDimensions();
        //m_TargetSize = fTargetScale * m_CurrSize;
        //Debug.Log(string.Format("targetSize {0} target scale {1} currSize = {2}", m_TargetSize, fTargetScale, m_CurrSize));

    }

    public override bool Update()
    {
        //if (!base.Update())
        //{
        //    return false;
        //}
        //m_InterpolatedValue = Utils.Interpolate(m_InterStyle, m_CurrSize, m_TargetSize, GetTimeRatio());
        //m_EffectedItem.SetWidthHeight(m_InterpolatedValue.x, m_InterpolatedValue.y);
        //return true;
        return true;
    } 
}

public class MenuItemEffect_GrowShrink : AceMenuItemEffect
{
	// ------> Variables
    Vector2 m_CurrSize = new Vector2();
    float m_fAmplitude;
	float m_fAngularFrequency;

    public MenuItemEffect_GrowShrink(AceMenuItem effectedItem, float fCompletionTime,
	                                 float amplitude, float numTimesToGrowShrink, bool bGrowFirst, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
        m_CurrSize = effectedItem.GetDimensions();
        m_fAmplitude = amplitude;
		m_fAngularFrequency = numTimesToGrowShrink / fCompletionTime * (Mathf.PI * 2.0f);
		
		if (!bGrowFirst)
		{
			m_fAngularFrequency = -m_fAngularFrequency;
		}
    }

    public override bool Update()
    { 
		if (!base.Update())
        {
			m_EffectedItem.SetWidthHeight(m_CurrSize.x, m_CurrSize.y);
            return false;
        }
		
		m_InterpolatedValue.x = m_fAmplitude * Mathf.Sin(TimePassed * m_fAngularFrequency);
		m_InterpolatedValue.y = m_fAmplitude * Mathf.Sin(TimePassed * m_fAngularFrequency);
		
        m_EffectedItem.SetWidthHeight(m_CurrSize.x + m_InterpolatedValue.x, m_CurrSize.y + m_InterpolatedValue.y);
		
		
        return true;
    } 
}

public class MenuItemEffect_Alpha : AceMenuItemEffect
{
    public MenuItemEffect_Alpha(AceMenuItem effectedItem, float fCompletionTime, OnEffectFinishedCallback cb, float fTargetAlpha)
        : base(effectedItem, fCompletionTime, cb)
    {
        // start val
        m_InterpolatedValue.x = effectedItem.Alpha;

        m_InterpolatedValue.y = 0;

        // end val
       SetTargetAlpha(fTargetAlpha);
    }

    public override bool Update()
    {
        if (!base.Update())
        {
            return false;
        }

        m_InterpolatedValue.y = Mathf.Lerp(m_InterpolatedValue.x, m_InterpolatedValue.z, GetTimeRatio());
        
        // set alpha..
        m_EffectedItem.Alpha = m_InterpolatedValue.y;
        return true;
    }
	
	public override void Reset ()
	{
		base.Reset ();
		m_InterpolatedValue.x = m_EffectedItem.Alpha;
		m_InterpolatedValue.y = 0;
	}
	
	public void SetTargetAlpha(float fTargetAlpha)
	{
		m_InterpolatedValue.z = Mathf.Clamp01(fTargetAlpha);
	}
}

public class MenuItemEffect_Chase : AceMenuItemEffect
{
	Vector3 m_Position;
    Transform m_ChasedTransform;

    public MenuItemEffect_Chase(AceMenuItem effectedItem, float fCompletionTime, 
	                            Transform chasedTransform, OnEffectFinishedCallback cb)
        : base(effectedItem, fCompletionTime, cb)
    {
        m_Position = effectedItem.GetPosition();
		m_ChasedTransform = chasedTransform;
    }

    public override bool Update()
    {
        if (!base.Update())
        {
            return false;
        }

        m_InterpolatedValue = Vector3.Lerp(m_Position, m_ChasedTransform.position, GetTimeRatio());
        m_EffectedItem.SetPosition(m_InterpolatedValue);
        return true;
    }
}

public class MenuItemEffect_Composite : AceMenuItemEffect
{
    List<AceMenuItemEffect> m_CurrentEffects = new List<AceMenuItemEffect>();

    public MenuItemEffect_Composite(AceMenuItem effectedItem) 
        : base(effectedItem)
    {
    }

    public override bool Update()
    {
        for (int i = 0; i < m_CurrentEffects.Count; i++)
        {
            m_CurrentEffects[i].Update();
        }

        return true;
    }

    public override void ReceiveMessage(AceMenuItemEffect.EffectMessage message)
    {
        for (int i = 0; i < m_CurrentEffects.Count; i++)
        {
            m_CurrentEffects[i].ReceiveMessage(message);
        }
    }

    public override void Add(AceMenuItemEffect effect)
    {
        if (effect != null && !m_CurrentEffects.Contains(effect))
        {
            m_CurrentEffects.Add(effect);
        }
    }

    public override void Remove(AceMenuItemEffect effect)
    {
        m_CurrentEffects.Remove(effect);
    }
	
	public override void RemoveAll()
	{
		m_CurrentEffects.Clear();
	}
	
	public override bool HasEffect(Type t)
	{
 		for (int i = 0; i < m_CurrentEffects.Count; i++)
		{
			if (m_CurrentEffects[i].GetType() == t)
			{
				return true;	
			}
		}
		return false;
	}
}
*/