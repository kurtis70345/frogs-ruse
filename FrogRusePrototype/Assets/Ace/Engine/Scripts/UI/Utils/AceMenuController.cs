﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceMenuController : AceController 
{
	#region Variables
	[SerializeField] AceMenu[] m_Menus;
	Stack<int> m_MenuStack = new Stack<int>();
	#endregion

	#region Functions
	public virtual void Start()
	{
		foreach (AceMenu menu in m_Menus)
		{
			menu.Init();
		}
	}

	public void UpdateLocalizedText()
	{
		foreach (AceMenu menu in m_Menus)
		{
			menu.UpdatedLocalizedText();
		}
	}


	/*void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			AceLocalization.instance.LoadLanguage("en");
			UpdateLocalizedText();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			AceLocalization.instance.LoadLanguage("sp");
			UpdateLocalizedText();
		}
	}*/

	public void ShowMenu(int menuId)
	{
		m_Menus[menuId].Show();
	}
	
	public void HideMenu(int menuId)
	{
		HideMenu(menuId, true);
	}
	
	public void HideMenu(int menuId, bool hide)
	{
		m_Menus[menuId].Hide(hide);
	}
	
	public void HideAllMenus()
	{
		for (int i = 0; i < m_Menus.Length; i++)
		{
			m_Menus[i].Hide();
		}
	}
	
	public void PushMenu(int menuId)
	{
		PushMenu(menuId, true);
	}
	
	public void PushMenu(int menuId, bool hideCurrentMenu)
	{
		if (hideCurrentMenu)
		{
			m_Menus[m_MenuStack.Peek()].Hide();
		}
		
		m_Menus[menuId].Show();
		m_MenuStack.Push(menuId);
	}
	
	public void PopMenu()
	{
		HideMenu(m_MenuStack.Pop());
		ShowMenu(m_MenuStack.Peek());
	}
	#endregion
}
