using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AceMenu : MonoBehaviour 
{
	#region Variables
	[SerializeField] AceMenuController m_MenuController;
    public AceButton[] m_Buttons;
    public AceToggle[] m_Toggles;
    public AceLabel[] m_TextItems;
	protected List<AceMenuItem> m_MenuItems = new List<AceMenuItem>();
	#endregion

	#region Functions
	protected T GetMenuController<T>() where T : AceMenuController
	{
		return m_MenuController as T;
	}

    virtual public void OnMainMenuButtonPressed()
    {
		Time.timeScale = 1.0f;
		Utils.LoadLevel(0);
		Debug.Log("OnMainMenuButtonPressed!");
    }

	virtual public void OnRestartLevelButtonPressed()
    {
		Utils.ReLoadLevel();
		//Debug.Log("OnRestartLevelButtonPressed!");
    }

	virtual public void OnNextLevelButtonPressed()
	{
		Utils.LoadLevel(Application.loadedLevel + 1);
		//Debug.Log("OnNextLevelButtonPressed!");
	}

	protected GameObject AddToLayout(LayoutGroup layoutGroup, GameObject prefab)
	{
		GameObject layoutItem = (GameObject)GameObject.Instantiate(prefab);
		layoutItem.transform.SetParent(layoutGroup.transform);
		return layoutItem;
	}
	
	protected T AddToLayout<T>(LayoutGroup layoutGroup, GameObject prefab) where T : MonoBehaviour
	{
		GameObject layoutItem = (GameObject)GameObject.Instantiate(prefab);
		layoutItem.transform.SetParent(layoutGroup.transform);
		return layoutItem.GetComponent<T>();
	}

    #region Messages
	public virtual void Init()
	{

	}

    virtual public void Hide() 
	{ 
		Hide(true);
	}
    virtual public void Show() 
	{
		Hide(false);
	}
	
	virtual public void Hide(bool tf)
	{
		gameObject.SetActive(!tf);	
	}

	public void UpdatedLocalizedText()
	{
		bool wasShowing = IsShowing();
		Hide(false);
		AceLocalize[] localizedTexts = GetComponentsInChildren<AceLocalize>();
		for (int i = 0; i < localizedTexts.Length; i++)
		{
			localizedTexts[i].Localize();
		}

		Hide(!wasShowing);
	}


	public virtual bool IsShowing()	
	{
		return gameObject.activeSelf;
	}

	public virtual void ShowHelp()
	{
		
	}

	public void PopMenu()
	{
		m_MenuController.PopMenu();
	}
	
    #endregion
	#endregion

}
