using UnityEngine;
using System.Collections;

public class AceYesNoMenu : AceMenu
{
	#region Constants
	enum Buttons
	{
		Yes,
		No,
        Ok,
	}
	
	public delegate void OnButtonPressed();
	#endregion
	
	#region Variables
    [SerializeField] GameObject m_MainPanel;
	OnButtonPressed m_YesCallback;
	OnButtonPressed m_NoCallback;
	public static AceYesNoMenu instance;
	#endregion
	
	#region Properties
	AceButton YesButton { get { return m_Buttons[(int)Buttons.Yes]; } }
	AceButton NoButton { get { return m_Buttons[(int)Buttons.No]; } }
    AceButton OkButton { get { return m_Buttons[(int)Buttons.Ok]; } }
	AceLabel MainText { get { return m_TextItems[0]; } }
	#endregion
	
	#region Functions
	void Awake()
	{
		Init ();
	}

	public override void Init()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy (gameObject);
		}
	}

	public void Display(string text)
	{
		Hide(false);
		SetText(text);
        SetCallback(null, null);
        UseOkButton();
	}
	
	public void Display(string text, OnButtonPressed yesCB)
	{
		Hide(false);
		SetText(text);
		SetCallback(yesCB, null);
        UseOkButton();
	}
	
	public void Display(string text, OnButtonPressed yesCB, OnButtonPressed noCB)
	{
		Hide(false);
		SetText(text);
		SetCallback(yesCB, noCB);
        UseYesNoButtons();
	}
	
	public void SetText(string text)
	{
		MainText.Text = text;
	}
	
	public void SetButtonLabels(string yesButtonText, string noButtonText)
	{
		YesButton.Text = yesButtonText;
		NoButton.Text = noButtonText;
	}
	
	public void SetCallback(OnButtonPressed yesCB)
	{
		m_YesCallback = yesCB;
	}
	
	public void SetCallback(OnButtonPressed yesCB, OnButtonPressed noCB)
	{
		m_YesCallback = yesCB;
		m_NoCallback = noCB;
	}
	
	public void OnYesButtonPressed()
	{
        Hide(true);
        OnButtonPressed tempCB = m_YesCallback;
        m_YesCallback = null;
        if (tempCB != null)
		{
            tempCB();	
		}
	}
	
	public void OnNoButtonPressed()
	{
        Hide(true);
        OnButtonPressed tempCB = m_NoCallback;
        m_NoCallback = null;
        if (tempCB != null)
		{
            tempCB();	
		}
	}

    void UseYesNoButtons()
    {
        YesButton.gameObject.SetActive(true);
        NoButton.gameObject.SetActive(true);
        OkButton.gameObject.SetActive(false);
    }

    void UseOkButton()
    {
        YesButton.gameObject.SetActive(false);
        NoButton.gameObject.SetActive(false);
        OkButton.gameObject.SetActive(true);
    }

    public override void Hide(bool tf)
    {
        m_MainPanel.SetActive(!tf);
    }
	#endregion
}
