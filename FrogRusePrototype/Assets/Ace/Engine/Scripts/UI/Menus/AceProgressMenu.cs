using UnityEngine;
using System.Collections;

public class AceProgressMenu : AceMenu
{
    #region Constants
    enum Text
    {
        Header
    }
    #endregion

    #region Variables
    public AceProgressBar m_ProgressBar;
    MonoBehaviour m_ProgressCoroutineOwner;
    string m_CoroutineName = "";
    #endregion

    #region Properties
    AceLabel HeaderText
    {
        get { return m_TextItems[(int)Text.Header]; }
    }
    #endregion

    #region Functions
    public void Display(MonoBehaviour progressCoroutineOwner, string coroutineName, float progress, string title)
    {
        Show();
        m_ProgressCoroutineOwner = progressCoroutineOwner;
        m_CoroutineName = coroutineName;
        SetProgress(progress);
        HeaderText.Text = title;
    }

    /// <summary>
    /// progress should be a value between 0 and 1
    /// </summary>
    /// <param name="progress"></param>
    public void SetProgress(float progress)
    {
        m_ProgressBar.SetProgress(progress);
    }

    public void CancelProgress()
    {
        if (m_ProgressCoroutineOwner != null)
        {
            m_ProgressCoroutineOwner.StopCoroutine(m_CoroutineName);
        }
        
        Hide();
    }
    #endregion
}
