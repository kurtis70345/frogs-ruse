using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.IO;

public class AceSaveLoadMenu : AceMenu
{
	#region Constants
	enum Buttons
	{
		Save,
		Load,
		Cancel
	}
	#endregion

	#region Variables
	[SerializeField] GameObject m_SaveLoadHandler;
	[SerializeField] AceInputField m_FileName;
	[SerializeField] AceToggle m_FileOption;
	[SerializeField] LayoutGroup m_LayoutGroup;
	[SerializeField] string m_PersistentDataSubFolder = "";
	[SerializeField] string m_FileExtension = "txt";
 	string m_Directory = "";
	#endregion

	#region Properteis
	AceButton SaveButton { get { return m_Buttons[(int)Buttons.Save]; } }
	AceButton LoadButton { get { return m_Buttons[(int)Buttons.Load]; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		InitDirectory();
	}

	string InitDirectory()
	{
		m_Directory = Application.persistentDataPath;
		if (!string.IsNullOrEmpty(m_PersistentDataSubFolder))
		{
			m_Directory += "/" + m_PersistentDataSubFolder;
		}
		Debug.Log("Save Load Directory: " + m_Directory);
		return m_Directory;
	}

	/*void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			ShowLoadDialog();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			ShowSaveDialog();
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			Hide ();
		}
	}*/

	public void SetDirectory(string directory)
	{
		m_Directory = directory;
	}

	public void SetExtension(string extension)
	{
		m_FileExtension = extension;
	}

	public string GetFilePath()
	{
		if (string.IsNullOrEmpty(m_FileName.InputText))
		{
			Debug.LogError("No file name select");
		}

		return string.Format("{0}/{1}.{2}", m_Directory, m_FileName.InputText, m_FileExtension);
	}

	void PopulateList(string directory, string ext)
	{
		if (string.IsNullOrEmpty(directory))
		{
			directory = InitDirectory();
		}

		Utils.DeleteChildren(m_LayoutGroup.transform);
		string[] files = Directory.GetFiles(directory, "*." + ext);

		for (int i = 0; i < files.Length; i++)
		{
			AceToggle fileOption = AddToLayout<AceToggle>(m_LayoutGroup, m_FileOption.gameObject);
			fileOption.gameObject.SetActive(true);
			fileOption.name = fileOption.Text = Path.GetFileNameWithoutExtension(files[i]);
			fileOption.SetToggleGroup(m_LayoutGroup.GetComponent<ToggleGroup>());
		}
	}

	public void OnSelectedFileOption(AceToggle fileOption)
	{
		m_FileName.InputText = fileOption.Text;
	}

	public void ShowLoadDialog()
	{
		Show ();
		PopulateList(m_Directory, m_FileExtension);
		SaveButton.Hide();
		LoadButton.Show();
		m_FileName.IsInteractable = false;
	}

	public void ShowSaveDialog()
	{
		Show ();
		PopulateList(m_Directory, m_FileExtension);
		SaveButton.Show();
		LoadButton.Hide();
		m_FileName.IsInteractable = true;
	}

	public void OnSaveButtonPressed()
	{
		ExecuteEvents.Execute<IAceLevelBuilder>(m_SaveLoadHandler, null, (x, y) => x.SaveLevel(GetFilePath()));
		Hide();
	}

	public void OnLoadButtonPressed()
	{
		ExecuteEvents.Execute<IAceLevelBuilder>(m_SaveLoadHandler, null, (x, y) => x.LoadLevel(GetFilePath()));
		Hide();
	}

	public void OnCancelButtonPressed()
	{
		Hide();
	}
	#endregion
}
