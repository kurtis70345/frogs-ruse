﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AceLoadingMenu : AceMenu
{
    #region Variables
    static AceLoadingMenu m_Instance;
    public AcePanel m_Panel;
	//public UIAnchor m_Anchor;
	//public UIStretch m_BackgroundStretcher;
	public GameObject m_PublisherLogo;
	public GameObject m_CompanyLogo;
	int m_NumTimesShown;
    #endregion

    #region Functions
    void Awake()
    {
        if (m_Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public static AceLoadingMenu Get()
    {
        if (m_Instance == null)
        {
            m_Instance = FindObjectOfType(typeof(AceLoadingMenu)) as AceLoadingMenu;
            if (m_Instance == null)
            {
                m_Instance = (AceLoadingMenu)Resources.Load("Menu_Loading", typeof(AceLoadingMenu));
                DontDestroyOnLoad(m_Instance.gameObject);
            }
        }
        return m_Instance;
    }

    void OnLevelWasLoaded(int level)
    {
        Hide();
	
    }

    public override void Hide(bool tf)
    {
		if (!tf)
		{
			//Debug.Log("SHOWING LOADING SCREEN!");
			m_NumTimesShown += 1;

			if (m_NumTimesShown >= 2)
			{
				ShowLogos(false);
			}
		}
        m_Panel.gameObject.SetActive(!tf);
    }

	void ShowLogos(bool show)
	{
		m_PublisherLogo.SetActive(show);
		m_CompanyLogo.SetActive(show);
	}
    #endregion
}
