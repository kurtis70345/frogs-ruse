using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AceAchievementUnlockedMenu : AceMenu
{
	#region Constants
	enum Text
	{
		Title,
		Description,
		Points
	}
	#endregion

	#region Variables
	[SerializeField] CanvasGroup m_MainPanelGroup;
	[SerializeField] float m_DisplayTime = 3;
	[SerializeField] float m_FadeTime = 2;
	static AceAchievementUnlockedMenu m_Instance;
	#endregion

	#region Propreties
	AceLabel TitleText { get { return m_TextItems[(int)Text.Title]; } }
	AceLabel DescriptionText { get { return m_TextItems[(int)Text.Description]; } }
	AceLabel PointsText { get { return m_TextItems[(int)Text.Points]; } }
	static public AceAchievementUnlockedMenu instance { get { return m_Instance; } }
	#endregion

	#region Functions
	void Awake()
	{
		Init();
	}

	public override void Init()
	{
		if (m_Instance == null)
		{
			m_Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy (gameObject);
		}
	}
	
	public void Display(string title, string description, uint points)
	{
		m_MainPanelGroup.alpha = 1;
		TitleText.Text = title;
		DescriptionText.Text = description;
		PointsText.Text = points.ToString();

		StartCoroutine(DisplayThenFade(m_DisplayTime, m_FadeTime));
	}

	IEnumerator DisplayThenFade(float displayTime, float fadeTime)
	{
		Show();
		yield return new WaitForSeconds(displayTime);

		float t = 0;
		while (t < fadeTime)
		{
			m_MainPanelGroup.alpha = Mathf.InverseLerp(1, 0, t / fadeTime);
			t += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		m_MainPanelGroup.alpha = 0;
		Hide();
	}

	public override void Hide (bool tf)
	{
		m_MainPanelGroup.gameObject.SetActive(!tf);
	}
	#endregion
}
