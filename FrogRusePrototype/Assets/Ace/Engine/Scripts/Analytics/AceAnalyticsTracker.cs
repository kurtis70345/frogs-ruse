﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceAnalyticsTracker : MonoBehaviour
{
    #region Variables
    public List<AceAnalytics> m_AnalyticsSystems = new List<AceAnalytics>();
    static AceAnalyticsTracker m_Instance;
    #endregion

    #region Functions
    void Awake()
    {
        if (m_Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public static AceAnalyticsTracker Get()
    {
        if (m_Instance == null)
        {
            m_Instance = FindObjectOfType(typeof(AceAnalyticsTracker)) as AceAnalyticsTracker;
            if (m_Instance == null)
            {
                m_Instance = (AceAnalyticsTracker)Resources.Load("AnalyticsTracker", typeof(AceAnalyticsTracker));
                DontDestroyOnLoad(m_Instance.gameObject);
            }
        }
        return m_Instance;
    }

    public void SendView(string appScreen)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].SendView(appScreen);
        }
    }

    public void SendEvent(string category, string action, string label)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].SendEvent(category, action, label);
        }
    }

    public void SendTiming(string category, long intervalInMilliseconds)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].SendTiming(category, intervalInMilliseconds);
        }
    }
    
    public void SendKey(string key, string value)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].SendKey(key, value);
        }
    }

    public void ClearKey(string key)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].ClearKey(key);
        }
    }

    public void SetLogLevel(int level)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].SetLogLevel(level);
        }
    }

    public void EnableDataSending(bool enable)
    {
        for (int i = 0; i < m_AnalyticsSystems.Count; i++)
        {
            m_AnalyticsSystems[i].EnableDataSending(enable);
        }
    }

    void OnLevelWasLoaded(int level)
    {
        SendView(Application.loadedLevelName);
    }
    #endregion
}
