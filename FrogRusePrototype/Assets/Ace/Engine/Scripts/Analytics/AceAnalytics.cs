﻿using UnityEngine;
using System.Collections;

public class AceAnalytics : MonoBehaviour
{
    #region Variables

    #endregion

    #region Functions
    void Awake()
    {
        StartTracking();
    }

    protected virtual void StartTracking() { }
    public virtual void SendView(string appScreen) { }
    public virtual void SendEvent(string category, string action, string label) { }
    public virtual void SendTiming(string category, long intervalInMilliseconds) { }
    public virtual void SendKey(string key, string value) { }
    public virtual void ClearKey(string key) { }
    public virtual void SetLogLevel(int level) { }
    public virtual void EnableDataSending(bool enable) { }
    #endregion
}
