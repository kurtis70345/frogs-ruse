﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES
public class AceAnalytics_Google : AceAnalytics
{
    #region Variables

    #endregion

    #region Functions
    protected override void StartTracking()
    {
        AndroidGoogleAnalytics.instance.StartTracking();
    }

    public override void SendView(string appScreen) 
    {
        AndroidGoogleAnalytics.instance.SendView(appScreen);
    }

    public override void SendEvent(string category, string action, string label)
    {
        AndroidGoogleAnalytics.instance.SendEvent(category, action, label);
    }

    public override void SendTiming(string category, long intervalInMilliseconds)
    {
        AndroidGoogleAnalytics.instance.SendTiming(category, intervalInMilliseconds);
    }

    public override void SendKey(string key, string value) 
    {
        AndroidGoogleAnalytics.instance.SetKey(key, value);
    }

    public override void ClearKey(string key)
    {
        AndroidGoogleAnalytics.instance.ClearKey(key);
    }

    public override void SetLogLevel(int level) 
    {
        AndroidGoogleAnalytics.instance.SetLogLevel((GPLogLevel)level);
    }

    public override void EnableDataSending(bool enable) 
    {
        AndroidGoogleAnalytics.instance.SetDryRun(!enable);
    }
    #endregion
}
#endif