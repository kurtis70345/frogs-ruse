﻿using UnityEngine;
using System.Collections;

public class AceGameLevelResponse
{
	#region Variables
	readonly string m_GameName;
	readonly string m_InternalGameName;
	readonly uint m_Level;
	readonly uint m_NumLevels;
	#endregion

	#region Properties
	public string GameName { get { return m_GameName; } }
	public string InternalGameName { get { return m_InternalGameName; } }
	public uint Level { get { return m_Level; } }
	public uint NumLevels { get { return m_NumLevels; } }
	#endregion

	#region Functions
	public AceGameLevelResponse(string gameName, string internalGameName, uint level, uint numLevels)
	{
		m_GameName = gameName;
		m_InternalGameName = internalGameName;
		m_Level = level;
		m_NumLevels = numLevels;
	}
	#endregion	
}
