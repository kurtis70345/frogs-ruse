using UnityEngine;
using System.Collections;

public class AceLevelEditor : MonoBehaviour
{
	#region Variables
	[SerializeField] AceLevelBuilder m_LevelBuilder;
	#endregion

	#region Functions
	//void ShowSaveDialog(string savePath);
	//void ShowLoadDialog();

	public virtual void SaveLevel(string path)
	{
		m_LevelBuilder.SaveLevel(path);
	}

	public virtual void LoadLevel(string path)
	{
		m_LevelBuilder.LoadLevel(path);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			SaveLevel(Application.dataPath + "/test.bytes");
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			LoadLevel(Application.dataPath + "/test.bytes");
		}
	}
	#endregion
}
