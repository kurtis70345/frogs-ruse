using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

abstract public class AceGridLevelBuilder : AceLevelBuilder, IAceGridBuilder
{
	#region Constants
	public enum CursorMode
	{
		Place,
		Selection,
	}
	#endregion

	#region Variables
	[SerializeField] AceGridObjectDataBuilder m_DataBuilder;
	[SerializeField] AceGrid m_Grid;
	[SerializeField] AceGridObjectFactory m_GridObjectFactory;
	[SerializeField] AceGridObjectPlacer m_GridPlacer;
	[SerializeField] GameObject m_GridBuilderMenu;

	string m_SelectedGridObjectId = "";
	List<AceGridObject> m_SelectedObjects = new List<AceGridObject>();
	CursorMode m_CursorMode = CursorMode.Place;
	#endregion

	#region Properties
	protected AceGridObjectDataBuilder GridDataBuilder { get { return m_DataBuilder; } }
	protected AceGrid Grid { get { return m_Grid; } }
	protected AceGridObjectFactory GridObjectFactory { get { return m_GridObjectFactory; } }
	protected AceGridObjectPlacer Placer { get { return m_GridPlacer; } }
	protected CursorMode CurrentCursorMode { get { return m_CursorMode; } }
	#endregion

	#region Functions
	public virtual void ResetGrid()
	{
		m_GridPlacer.DeletePlacedObjects();
		m_SelectedObjects.Clear();
		ExecuteEvents.Execute<IAceGridBuilderView>(m_GridBuilderMenu, null, (x,y) => x.UpdateSelectedObject(null));
	}
	#endregion

	#region IAceGridBuilder implementation

	public void SetCurrentGridObject (string gridObjectId)
	{
		m_SelectedGridObjectId = gridObjectId;
		Placer.SetTrackedObject(m_GridObjectFactory.CreateGridObject(m_SelectedGridObjectId));
	}

	public void SelectPlacedGridObject(AceGridObject gridObject, bool clearOtherSelections)
	{
		if (gridObject != null)
		{
			if (clearOtherSelections)
			{
				m_SelectedObjects.Clear();
			}
			m_SelectedObjects.Add(gridObject);
			ExecuteEvents.Execute<IAceGridBuilderView>(m_GridBuilderMenu, null, (x,y) => x.UpdateSelectedObject(gridObject));
		}
	}
	
	public void RotateSelectedObjects (float degrees)
	{
		foreach (AceGridObject gridObject in m_SelectedObjects)
		{
			gridObject.transform.Rotate(0, degrees, 0);
			ExecuteEvents.Execute<IAceGridBuilderView>(m_GridBuilderMenu, null, (x,y) => x.UpdateSelectedObject(gridObject));
		}
	}

	public void SetCursorMode(AceGridLevelBuilder.CursorMode cursorMode)
	{
		m_CursorMode = cursorMode;
		m_GridPlacer.TrackMousePos = m_CursorMode == CursorMode.Place;
		m_SelectedObjects.Clear();

		if (m_CursorMode == CursorMode.Selection)
		{
			ExecuteEvents.Execute<IAceGridBuilderView>(m_GridBuilderMenu, null, (x,y) => x.UpdateSelectedObject(null));
		}
	}

	#endregion
}
