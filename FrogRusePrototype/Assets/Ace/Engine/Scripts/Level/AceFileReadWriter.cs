﻿using UnityEngine;
using System.Collections;

public class AceFileReadWriter : MonoBehaviour 
{
	#region Variables
	AceFileHandler m_FileHandler;
	#endregion

	#region Functions
	// Use this for initialization
	public virtual void Awake () 
	{
		m_FileHandler = new AceFileHandler_dat();
	}

	public void SetHandler(AceFileHandler handler)
	{
		m_FileHandler = handler;	
	}

	/*public void SetOnGridObjectCreatedCallback(OnGridObjectCreated cb)
	{
		m_OnGridObjectCreatedCallback += cb;
	}*/
	
	public bool SaveLevel<T>(string fileNameAndPath, byte[] levelData)
	{
		#if !UNITY_WEBPLAYER
		// the data is already serialized, so just stick it in a file
		System.IO.File.WriteAllBytes(fileNameAndPath, levelData);
		
		// I load the data to make sure that the level is a legit and there are no errors
		return m_FileHandler.DeserializeFile<T>(fileNameAndPath) != null;
		#else
		Debug.LogError("this function doesn't work on web");
		return false;
		#endif
	}
	
	public bool SaveLevel<T>(string fileNameAndPath, T levelData)
	{
		bool success = m_FileHandler.Serialize<T>(fileNameAndPath, levelData);
		
		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh();
		#endif
		return success;
	}
	
	public T LoadLevel<T>(string fileNameAndPath)
	{
		return LoadLevel<T>(fileNameAndPath, true);
	}
	
	public T LoadLevel<T>(string fileNameAndPath, bool handleLoadedData)
	{
		Debug.Log(fileNameAndPath);
		T levelSaveData = m_FileHandler.DeserializeFile<T>(fileNameAndPath);
		if (handleLoadedData)
		{
			HandleLoadData<T>(levelSaveData);
		}
		return levelSaveData;
	}
	
	public T LoadLevel<T>(TextAsset textAsset)
	{
		T levelSaveData = m_FileHandler.DeserializeFile<T>(textAsset);
		HandleLoadData(levelSaveData);
		return levelSaveData;
	}

	public virtual void HandleLoadData<T>(T loadingLevelData)
	{

	}

	#endregion
}
