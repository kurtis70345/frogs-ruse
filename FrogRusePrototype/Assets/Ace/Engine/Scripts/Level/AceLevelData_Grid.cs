using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable()]
public class AceLevelData_Grid : AceLevelData
{
	#region Variables
	[SerializeField]
	List<AceGridObjectData> m_GridObjects = new List<AceGridObjectData>();
	#endregion
	
	#region Properties
	public IEnumerable<AceGridObjectData> GridObjectData { get { return m_GridObjects; } }
	#endregion

	#region Functions
	public AceLevelData_Grid() { }

	public void AddGridObjectData(AceGridObjectData[] data)
	{
		m_GridObjects.AddRange(data);
	}

	public void AddGridObjectData(IEnumerable<AceGridObjectData> data)
	{
		m_GridObjects.AddRange(data);
	}
	#endregion
}
