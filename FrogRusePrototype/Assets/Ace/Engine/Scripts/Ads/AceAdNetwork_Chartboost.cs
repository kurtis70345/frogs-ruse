﻿using UnityEngine;
using System.Collections;

#if false
using ChartboostSDK;

public class AceAdNetwork_Chartboost : AceAdNetwork 
{
	#region Variables
	bool m_AppsCached;
	#endregion

	#region Functions
	override public void Start()
	{
		Debug.Log("AceAdNetwork_Chartboost!");
		Chartboost.didCacheMoreApps += didCacheMoreApps;
		Chartboost.didFailToLoadMoreApps += didFailToLoadMoreApps;
		Chartboost.didDisplayMoreApps += didDisplayMoreApps;
		//Chartboost.setAutoCacheAds(true);
		StartCoroutine(WaitToCacheApps(3));
	}
	
	void didDisplayMoreApps(CBLocation location)
	{
		Debug.Log("didDisplayMoreApps: " + location);
	}

	IEnumerator WaitToCacheApps(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Chartboost.cacheMoreApps(CBLocation.Default);
	}

	/*void OnLevelWasLoaded(int level)
	{
		m_AppsCached = false;
		//StartCoroutine(WaitToCacheApps(3));
	}*/

	public override void ShowIntersitial()
	{
		//Chartboost.showInterstitial(CBLocation.Default);
	}

	void didFailToLoadMoreApps(CBLocation location, CBImpressionError error) {
		Debug.Log(string.Format("didFailToLoadMoreApps: {0} at location: {1}", error, location));
	}

	public override void ShowMoreApps()
	{
		if (m_AppsCached)
		{
			Debug.Log("Show More Apps!!");
			Chartboost.showMoreApps(CBLocation.Default);
		}
		else
		{
			Debug.Log("apps not cached yet");
		}
	}

	void didCacheMoreApps(CBLocation location) 
	{
		Debug.Log(string.Format("didCacheMoreApps at location: {0}", location));
		m_AppsCached = true;
	}
	#endregion
}
#endif
