﻿using UnityEngine;
using System.Collections;

public class AceAdNetwork : MonoBehaviour 
{
	#region Constants
	public enum AdResult
	{
		Failed,
		Skipped,
		Finished,
	}
	public delegate void OnFinishedAd(AdResult result);
	#endregion

	#region Variables
	public string m_AppKey = "";
	public string m_UserId = "";
	protected bool m_CanReceiveVideos;
	protected OnFinishedAd m_OnFinishedAd;
	#endregion

	#region Functions
	// Use this for initialization
	public virtual void Start()
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public virtual void ShowIntersitial(OnFinishedAd onFinishedAd)
	{

	}

	public virtual void ShowIntersitial(string zoneId, OnFinishedAd onFinishedAd)
	{

	}

	public virtual void ShowMoreApps()
	{

	}

	public virtual void ShowOfferWall()
	{

	}

	public virtual void ShowVideo(OnFinishedAd onFinishedAd)
	{

	}

	public virtual bool IsInitialized()
	{
		return false;
	}
	#endregion
}
