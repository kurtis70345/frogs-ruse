﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_UNITY_ADS
using UnityEngine.Advertisements;

public class AceAdNetwork_UnityAds : AceAdNetwork
{
	#region Constants
	//const string UnskippableVideoZoneId = "rewardedVideoZone";//"rewardedVideoZone";//"18474-1415684202";
	public const string CommunityDownloadAd = "defaultVideoAndPictureZone";
	public const string LevelCompleteAd = "rewardedVideoZone";
	#endregion

	#region Variables
	public string m_iOSAppKey = "";
	ShowOptions m_ShowOptions = new ShowOptions();
	#endregion

	#region Functions
	void Awake()
	{
		//Advertisement.debugLevel =  Advertisement.DebugLevel.ERROR | Advertisement.DebugLevel.WARNING | Advertisement.DebugLevel.INFO | Advertisement.DebugLevel.DEBUG;
	}
	// Use this for initialization
	public override void Start()
	{
		if (Advertisement.isSupported) 
		{
#if UNITY_ANDROID
			Advertisement.Initialize (m_AppKey);
#elif UNITY_IOS
			Advertisement.Initialize (m_iOSAppKey);
#endif
			m_ShowOptions.resultCallback = OnShowResult;
		} 
		else
		{
			Debug.LogWarning("Unity Ads Platform not supported");
		}
	}

	public override bool IsInitialized ()
	{
		return Advertisement.isInitialized;
	} 

	void ShowAd(string zoneId, OnFinishedAd onFinishedAd)
	{
		if (IsInitialized() && Advertisement.IsReady(zoneId) && m_ShowOptions != null)
		{ 
			m_OnFinishedAd = onFinishedAd;
			Advertisement.Show(zoneId, m_ShowOptions);
		}
		else 
		{
			Debug.Log("ad is not ready!!!");
			if (onFinishedAd != null)
			{
				onFinishedAd(AdResult.Failed);
			}
		}
	}

	public override void ShowIntersitial(OnFinishedAd onFinishedAd)
	{
		ShowAd(null, onFinishedAd);
	}

	public override void ShowIntersitial(string zoneId, OnFinishedAd onFinishedAd)
	{
		ShowAd(zoneId, onFinishedAd);
	}

	void OnShowResult(ShowResult result)
	{
		AdResult adResult = AdResult.Finished;
		if (m_OnFinishedAd != null)
		{
			switch (result)
			{
			case ShowResult.Failed:
				adResult = AdResult.Failed;
				break;

			case ShowResult.Skipped:
				adResult = AdResult.Skipped;
				break;

			case ShowResult.Finished:
			default:
				adResult = AdResult.Finished;
				break;
			}

			m_OnFinishedAd(adResult);
			m_OnFinishedAd = null;
		}
	}

	public void UnityAdsCampaignsAvailable() 
	{
		Debug.Log ("ADS: CAMPAIGNS READY!");
		//m_IsInitialized = true;
	}
	
	public void UnityAdsCampaignsFetchFailed() 
	{
		Debug.Log ("ADS: CAMPAIGNS FETCH FAILED!");
	}
	
	public void UnityAdsShow() 
	{
		Debug.Log ("ADS: SHOW");
	}
	
	public void UnityAdsHide() 
	{
		Debug.Log ("ADS: HIDE");
	}
	
	public void UnityAdsVideoStarted() 
	{
		Debug.Log ("ADS: VIDEO STARTED!");
	}
	#endregion
}
#endif