﻿using UnityEngine;
using System;
using System.Collections;

public class AceWebProxy : MonoBehaviour
{
	#region Constants
	public delegate void OnReceivedPayload(WebPayload payload);

	public class WebPayload
	{
		#region Variables
		string m_Json;
		int m_RequestId;
		Delegate m_AppCallback;
		#endregion

		#region Properties
		public string Json { get { return m_Json; } }
		public int RequestId { get { return m_RequestId; } }
		public Delegate AppCallback { get { return m_AppCallback; } }
		#endregion

		#region Functions
		public WebPayload(string json, int requestId, Delegate appCallback)
		{
			m_Json = json;
			m_RequestId = requestId;
			m_AppCallback = appCallback;
		}
		#endregion
	}
	#endregion

	#region Variables
	[SerializeField] bool m_PrintResponse = false;
	#endregion

	#region Functions

	public void Request(string url)
	{
		StartCoroutine(MakeRequest(url, null, null, 0, null));
	}

	public void Request(string url, OnReceivedPayload receivedPayloadCb, int requestId, Delegate appCallback)
	{
		StartCoroutine(MakeRequest(url, null, receivedPayloadCb, requestId, appCallback));
	}

	public void Request(string url, WWWForm form)
	{
		StartCoroutine(MakeRequest(url, form, null, 0, null));
	}

	public void Request(string url, WWWForm form, OnReceivedPayload receivedPayloadCb, int requestId, Delegate appCallback)
	{
		StartCoroutine(MakeRequest(url, form, receivedPayloadCb, requestId, appCallback));
	}

	IEnumerator MakeRequest(string url, WWWForm form, OnReceivedPayload receivedPayloadCb, int requestId, Delegate appCallback)
	{
		WWW www = null;
		if (form != null)
		{
			www = new WWW(url, form);
		}
		else
		{
			www = new WWW(url);
		}
		
		yield return www;
		
		while (!www.isDone) { yield return new WaitForEndOfFrame(); }

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogError(www.error);
			yield break;
		}

		if (m_PrintResponse)
		{
			Debug.Log("WebRequestResponse: " + www.text);
		}

		if (receivedPayloadCb != null)
		{
			string error = AceJsonInterpreter.GetString(www.text, "error");
			if (string.IsNullOrEmpty(error))
			{
				WebPayload payload = new WebPayload(www.text, requestId, appCallback);
				receivedPayloadCb(payload);
			}
			else
			{
				AceYesNoMenu.instance.Display(AceTranslator.Translate(error));
			}
		}
	}
	#endregion
}
