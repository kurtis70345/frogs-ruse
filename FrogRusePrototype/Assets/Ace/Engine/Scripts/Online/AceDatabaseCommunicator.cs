﻿using UnityEngine;
using System.Collections;

abstract public class AceDatabaseCommunicator : MonoBehaviour 
{
	#region Constants
	public delegate void OnCreateAccount(AcePlayerAccount account);
	public delegate void OnLogin(AcePlayerAccount account);
	public delegate void OnAddXP(AceXPResponse xpResponse);
	public delegate void OnReceivedGameLevelProgress(AceGameLevelResponse levelResponse);
	#endregion

	#region Variables
	[SerializeField] AceWebProxy m_WebProxy;
	#endregion

	#region Properties
	protected AceWebProxy WebProxy { get { return m_WebProxy; } }
	#endregion

	#region Functions
	virtual public void CreateAccount(string email, string password, string username, OnCreateAccount cb) { }
	virtual public void Login(string username, string password, OnLogin cb) { }
	virtual public void AddXP(string username, uint xp, OnAddXP cb) { }
	virtual public void SetGameLevelProgress(string username, string internalGameName, uint level, OnReceivedGameLevelProgress cb) { }
	virtual public void GetGameLevelProgress(string username, string internalGameName, OnReceivedGameLevelProgress cb) { }
	#endregion
}
