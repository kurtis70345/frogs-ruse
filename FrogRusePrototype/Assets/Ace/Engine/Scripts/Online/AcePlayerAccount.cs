﻿using UnityEngine;
using System.Collections;

public class AcePlayerAccount
{
	#region Variables
	readonly string m_Username = "";
	readonly string m_Email = "";
	readonly string m_Password = "";
	uint m_XP;
	uint m_Level;
	float m_NormalizedLevelProgress;
	#endregion

	#region Properties
	public string Username { get { return m_Username; } }
	public string Email { get { return m_Email; } }
	public string Password { get { return m_Password; } } 
	public uint Level { get { return m_Level; } }
	public float NormalizedLevelProgress { get { return m_NormalizedLevelProgress; } }
	public uint XP
	{
		get { return m_XP; }
		set { m_XP = value; }
	}
	#endregion

	#region Functions 
	public AcePlayerAccount(string username, string email, string password)
	{
		m_Username = username;
		m_Email = email;
		m_Password = password;
	}

	public AcePlayerAccount(string username, string email, string password, uint xp, uint level, float normalizedProgress)
	{
		m_Username = username;
		m_Email = email;
		m_Password = password;
		m_XP = xp;
		m_Level = level;
		m_NormalizedLevelProgress = normalizedProgress;
	}

	public void SetXPData(uint xp, uint level, float normalizedLevelProgess)
	{
		m_XP = xp;
		m_Level = level;
		m_NormalizedLevelProgress = normalizedLevelProgess;
	}
	#endregion
}