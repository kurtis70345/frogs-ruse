﻿using UnityEngine;
using System;
using System.Collections;

public class AceWebRequester : MonoBehaviour 
{
	#region Constants
	public delegate void OnRequestFinished(WWW www);
	#endregion

	#region Variables

	#endregion

	#region Functions
	public void Request(string url, OnRequestFinished cb)
	{
		StartCoroutine(MakeRequest(url, null, cb));
	}

	public void Request(string url, WWWForm form, OnRequestFinished cb)
	{
		StartCoroutine(MakeRequest(url, form, cb));
	}

	IEnumerator MakeRequest(string url, WWWForm form, OnRequestFinished cb)
	{
		WWW www = null;
		yield return StartCoroutine(PerformRequest(url, form, www));

		if (cb != null)
		{
			cb(www);
		}
	}

	IEnumerator PerformRequest(string url, WWWForm form, WWW www)
	{
		if (form != null)
		{
			www = new WWW(url, form);
		}
		else
		{
			www = new WWW(url);
		}
		
		yield return www;

		while (!www.isDone) { yield return new WaitForEndOfFrame(); }
	}
	#endregion
}
