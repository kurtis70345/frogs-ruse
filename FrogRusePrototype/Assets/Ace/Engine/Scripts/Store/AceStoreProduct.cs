﻿using UnityEngine;
using System.Collections;


public class AceStoreProduct
{
    #region Constants
    public enum PurchaseState
    {
        PURCHASED = 0,
        CANCELED = 1,
        REFUNDED = 2
    }
    #endregion

    #region Variables
    public readonly string orderId = "";
    public readonly string packageName = "";
    public readonly string SKU = "";

    public readonly string developerPayload = "";
    public readonly string signature = "";
    public readonly string token = "";
    public readonly PurchaseState state;

    public readonly float cost;
    public readonly bool requiresRealMoney = true;
    public readonly int amountGiven = 1;
    public readonly bool isOneTimePurchase = true;
	public readonly string costAsString = "";
    #endregion

    #region Properties
    
    #endregion

    #region Functions
#if ALLOW_ACE_DEPENDENCIES
    public AceStoreProduct(GooglePurchaseTemplate productData)
    {
        orderId = productData.orderId;
        packageName = productData.packageName;
        SKU = productData.SKU;
        developerPayload = productData.developerPayload;
        signature = productData.signature;
        token = productData.token;
        state = (PurchaseState)productData.state;
        requiresRealMoney = true;
        cost = 0;
    }
#endif

	public AceStoreProduct(string _sku, string _price, bool _requiresRealMoney, bool _isOneTimePurchase)
	{
		requiresRealMoney = _requiresRealMoney;
		costAsString = _price;
		SKU = _sku;
		isOneTimePurchase = _isOneTimePurchase;
	}

    public AceStoreProduct(string _name, string _sku, float _cost, bool _requiresRealMoney, int _amountGiven, bool _isOneTimePurchase)
    {
        packageName = _name;
        SKU = _sku;
        cost = _cost;
        requiresRealMoney = _requiresRealMoney;
        amountGiven = _amountGiven;
        isOneTimePurchase = _isOneTimePurchase;
    }

    public AceStoreProduct(string _sku)
    {
        SKU = _sku;
    }
    #endregion
}
