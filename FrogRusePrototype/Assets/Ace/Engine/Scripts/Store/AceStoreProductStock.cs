﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AceStoreProductStock
{
    #region Variables
    // key is the sku
    protected readonly Dictionary<string, AceStoreProduct> m_AvailableProducts = new Dictionary<string, AceStoreProduct>();
    #endregion

    #region Functions
    public AceStoreProductStock()
    {
        
    }

	public List<AceStoreProduct> GetAllRealMoneyProducts()
	{
		List<AceStoreProduct> realMoneySkus = new List<AceStoreProduct>();
		foreach (KeyValuePair<string, AceStoreProduct> kvp in m_AvailableProducts)
		{
			if (kvp.Value.requiresRealMoney)
			{
				realMoneySkus.Add(kvp.Value);
			}
		}
		return realMoneySkus;
	}

    public void AddProduct(string name, string sku, float cost)
    {
        AddProduct(name, sku, cost, false, 1, true);
    }

    public void AddProduct(string name, string sku, float cost, bool requiresRealMoney)
    {
        AddProduct(name, sku, cost, requiresRealMoney, 1, true);
    }

    public void AddProduct(string name, string sku, float cost, bool requiresRealMoney, bool isOneTimePurchase)
    {
        AddProduct(name, sku, cost, requiresRealMoney, 1, isOneTimePurchase);
    }

    public void AddProduct(string name, string sku, float cost, bool requiresRealMoney, int amountGiven, bool isOneTimePurchase)
    {
        if (m_AvailableProducts.ContainsKey(sku))
        {
            Debug.LogError("You already have a product with the sku " + sku);
        }
        else
        {
            m_AvailableProducts.Add(sku, new AceStoreProduct(name, sku, cost, requiresRealMoney, amountGiven, isOneTimePurchase));
        }
    }

    public int GetAmountGiven(string sku)
    {
        int amountGiven = 0;
        if (DoesProductExist(sku))
        {
            amountGiven = GetProduct(sku).amountGiven;
        }
        return amountGiven;
    }

    public bool DoesProductExist(string sku)
    {
        bool retVal = m_AvailableProducts.ContainsKey(sku);
        if (!retVal)
        {
            Debug.LogError("sku " + sku + " doesn't exist!");
        }
        return retVal;
    }

    public bool RequiresRealMoney(string sku)
    {
        bool requiresRealMoney = false;
        if (DoesProductExist(sku))
        {
            requiresRealMoney = GetProduct(sku).requiresRealMoney;
        }
        return requiresRealMoney;
    }

    public AceStoreProduct GetProduct(string sku)
    {
        return m_AvailableProducts[sku];
    }

	public string GetProductName(string sku)
	{
		string name = "";
		if (DoesProductExist(sku))
		{
			name = GetProduct(sku).packageName;
		}
		return name;
	}

    public float GetProductPrice(string sku)
    {
        float cost = 0;
        if (DoesProductExist(sku))
        {
            cost = GetProduct(sku).cost;
        }
        return cost;
    }

    public int GetProductPriceAsInt(string sku)
    {
        return (int)GetProductPrice(sku);
    }

    public bool IsOneTimePurchase(string sku)
    {
        bool isOneTimePurchase = true;
        if (DoesProductExist(sku))
        {
            isOneTimePurchase = GetProduct(sku).isOneTimePurchase;
        }
        return isOneTimePurchase;
    }
    #endregion
}
