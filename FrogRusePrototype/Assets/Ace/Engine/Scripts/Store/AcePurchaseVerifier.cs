﻿using UnityEngine;
using System.Collections;

public class AcePurchaseVerifier : MonoBehaviour
{
    #region Constants
    public class VerificationResults
    {
        public bool success = false;
        public string error = "";
        public AceStoreProduct product;

        public VerificationResults(bool _success, string _error, AceStoreProduct _product)
        {
            success = _success;
            error = _error;
            product = _product;
        }
    }

    public delegate void OnVerifyResults(VerificationResults results);
    #endregion

    #region Variables
	public const string m_AppleUrl = "http://54.187.151.16/VerifyIAP_apple.php";
	public const string m_GoogleUrl = "http://54.187.151.16/VerifyIAP_google.php";
    static AcePurchaseVerifier m_Instance;
    #endregion

    #region Functions
    public static AcePurchaseVerifier Get()
    {
        if (m_Instance == null)
        {
            m_Instance = FindObjectOfType<AcePurchaseVerifier>();
            if (m_Instance == null)
            {
                GameObject go = new GameObject("Purchase Verifier");
                m_Instance = go.AddComponent<AcePurchaseVerifier>();
                DontDestroyOnLoad(m_Instance.gameObject);
            }
        }
        return m_Instance;
    }

    public void VerifyApplePurchase(string receipt, bool isSandbox, AceStoreProduct product, OnVerifyResults cb)
    {
        WWWForm form = new WWWForm();
        form.AddField("receipt", receipt);
#if ALLOW_ACE_DEPENDENCIES
        StartCoroutine(Verify(m_AppleUrl, form, product, cb));
#endif
    }

    public void VerifyGooglePurchase(string originalJson, string signature, string publicKey, AceStoreProduct product, OnVerifyResults cb)
    {
        WWWForm form = new WWWForm();
        
        form.AddField("publicKey", publicKey);
        form.AddField("signature", signature);
        form.AddField("originalJson", originalJson);

#if ALLOW_ACE_DEPENDENCIES
        StartCoroutine(Verify(m_GoogleUrl, form, product, cb));
#endif
    }

#if ALLOW_ACE_DEPENDENCIES
    IEnumerator Verify(string url, WWWForm form, AceStoreProduct product, OnVerifyResults cb)
    {
		Debug.Log("verifying: " + url);
        WWW www = new WWW(url, form);

        yield return www;
        Debug.Log(www.text);
        IDictionary results = (IDictionary)ANMiniJSON.Json.Deserialize(www.text);
        if (cb != null)
        {
            bool success = false;
            bool.TryParse(results["success"].ToString(), out success);
            string error = results["error"].ToString();
            cb(new VerificationResults(success, error, product));
        }
    }
#endif
    #endregion
}