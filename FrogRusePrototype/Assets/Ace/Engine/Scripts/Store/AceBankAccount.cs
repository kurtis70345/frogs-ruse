﻿using UnityEngine;
using System.Collections;

public class AceBankAccount : MonoBehaviour
{
    #region Variables

    #endregion

    #region Properties
    public ulong CurrentAmount
    {
        get { return /*PersistentData.Get().NumCrystals*/0; }
    }
    #endregion

    #region Functions
    public void Deposit(int val)
    {
        //PersistentData.Get().DepositCrystals(val);
    }

    public void Withdraw(int val)
    {
        //PersistentData.Get().WithdrawCrystals(val);
    }
    #endregion
}
