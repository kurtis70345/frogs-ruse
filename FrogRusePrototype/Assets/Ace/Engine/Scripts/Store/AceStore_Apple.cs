﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES
using UnionAssets.FLE;


public class AceStore_Apple : AceStore
{
    public AceStore_Apple(AceStoreProductStock stock, AceStoreShopper shopper)
        : base(stock, shopper)
    {
        //IOSInAppPurchaseManager.instance.addEventListener(IOSInAppPurchaseManager.TRANSACTION_FAILED, OnTransactionFailed);
        IOSInAppPurchaseManager.instance.addEventListener(IOSInAppPurchaseManager.RESTORE_TRANSACTION_FAILED, OnRestoreTransactionFailed);
        IOSInAppPurchaseManager.instance.addEventListener(IOSInAppPurchaseManager.VERIFICATION_RESPONSE, OnVerificationResponce);
    }

    protected override string GetStoreName()
    {
        return "Apple";
    }

    public override void PurchaseProduct(string sku)
	{
        IOSInAppPurchaseManager.instance.buyProduct(sku);
	}

    public override void LoadStore(string key)
    {
        IOSInAppPurchaseManager.instance.loadStore();
    }

    protected override void DisplayMessage(string title, string message)
    {
        IOSNativePopUpManager.showMessage(title, message);
    }

    protected override void AddEventListener(StoreEvent storeEvent)
    {  
        base.AddEventListener(storeEvent);
        switch (storeEvent)
        {
            case StoreEvent.ON_BILLING_SETUP_FINISHED:
                IOSInAppPurchaseManager.instance.addEventListener(IOSInAppPurchaseManager.STORE_KIT_INITIALIZED, OnStoreKitInited);
                break;

            case StoreEvent.ON_PRODUCT_PURCHASED:
			IOSInAppPurchaseManager.instance.addEventListener(IOSInAppPurchaseManager.TRANSACTION_COMPLETE, OnProductBought);
                break;

            case StoreEvent.ON_RETRIEVE_PRODUCT_FINISHED:
                //AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
                break;

            default:
                Debug.LogError("unhandled store event: " + storeEvent);
                break;
        }
    }

    public override string GetProductPriceAsString(string sku)
    {
        string price = "";
        if (m_ProductStock.DoesProductExist(sku) && !m_ProductStock.RequiresRealMoney(sku))
        {
            price = m_ProductStock.GetProductPrice(sku).ToString("f2");
        }
        else
        {
            IOSProductTemplate product = IOSInAppPurchaseManager.instance.products.Find(pt => pt.id == sku);
            if (product != null)
            {
                price = product.localizedPrice;
            }
            else
            {
                Debug.LogError("product " + sku + " doesn't exist");
            }
        }
        return price;
    }


    private void OnProductBought(CEvent e)
    {
        IOSStoreKitResponse responce = e.data as IOSStoreKitResponse;
        //Debug.Log("STORE KIT GOT BUY: " + responce.productIdentifier + " RECIPT: " + responce.receipt);

        RecordTransaction(responce.productIdentifier, responce.receipt, "");
        AcePurchaseVerifier.Get().VerifyApplePurchase(responce.receipt, true, new AceStoreProduct(responce.productIdentifier), OnPurchaseVerification);
        //ProcessPurchasedProduct(new AceStoreProduct(responce.productIdentifier));

        //Debug.Log("Success" + "Product " + responce.productIdentifier + " is purchased");
    }

    private static void OnRestoreTransactionFailed()
    {
        Debug.Log("Error" + "Restore Failed");
    }

    private static void OnTransactionFailed(CEvent e)
    {
        IOSStoreKitResponse responce = e.data as IOSStoreKitResponse;
        Debug.Log("Error" + responce.error);
    }

    private static void OnVerificationResponce(CEvent e)
    {
        IOSStoreKitVerificationResponse responce = e.data as IOSStoreKitVerificationResponse;

        //ShowMessage("Verification", "Transaction verification status: " + responce.status.ToString());
        Debug.Log("ORIGINAL JSON ON: " + responce.originalJSON);
    }

    private static void OnStoreKitInited()
    {
        IOSInAppPurchaseManager.instance.removeEventListener(IOSInAppPurchaseManager.STORE_KIT_INITIALIZED, OnStoreKitInited);
        //ShowMessage("StoreKit Inited", "Avaliable products cound: " + IOSInAppPurchaseManager.instance.products.Count.ToString());
    }

    static void ShowMessage(string title, string message)
    {
        IOSNativePopUpManager.showMessage(title, message);
    }
}
#endif