using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES
using UnionAssets.FLE;

public class AceStore_Google : AceStore
{
    #region Function
    public AceStore_Google(AceStoreProductStock stock, AceStoreShopper shopper, string storeKey)
        : base(stock, shopper)
    {
		AndroidNativeSettings.Instance.base64EncodedPublicKey = storeKey;
    }

    protected override string GetStoreName()
    {
        return "Google";
    }

    public override void AddProduct(string sku)
    {
        base.AddProduct(sku);
        AndroidInAppPurchaseManager.instance.addProduct(sku);
    }

    public override void LoadStore(string key)
    {
        //Debug.Log("LoadStore: " + key);
        AndroidInAppPurchaseManager.instance.loadStore(key);
    }

    public override void PurchaseProduct(string sku)
    {
        AndroidInAppPurchaseManager.instance.purchase(sku);
    }

    public override void ConsumeProduct(string sku)
    {
        if (IsProductPurchased(sku))
        {
            AndroidInAppPurchaseManager.instance.consume(sku);
        }
    }

    public override string GetProductPriceAsString(string sku)
    {
        string price = "";
        if (m_ProductStock.DoesProductExist(sku) && !m_ProductStock.RequiresRealMoney(sku))
        {
            price = m_ProductStock.GetProductPrice(sku).ToString("f2");
        }
        else
        {
            GoogleProductTemplate product = AndroidInAppPurchaseManager.instance.inventory.GetProductDetails(sku);
            if (product != null)
            {
                price = product.price;
            }
            else
            {
                Debug.LogError("product " + sku + " doesn't exist");
            }
        }
        return price;
    }

    //public override string GetProductDisplayName(string sku)
    //{
    //    string name = "";
    //    GoogleProductTemplate product = AndroidInAppPurchaseManager.instance.inventory.GetProductDetails(sku);
    //    if (product != null)
    //    {
    //        name = product.title;
    //    }
    //    else
    //    {
    //        Debug.LogError("product " + sku + " doesn't exist");
    //    }

    //    return name;
    //}

    protected override void AddEventListener(StoreEvent storeEvent)
    {
        base.AddEventListener(storeEvent);
        switch (storeEvent)
        {
            case StoreEvent.ON_BILLING_SETUP_FINISHED:
                AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);
                break;

            case StoreEvent.ON_PRODUCT_CONSUMED:
                AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_PRODUCT_CONSUMED, OnProductConsumed);
                break;

            case StoreEvent.ON_PRODUCT_PURCHASED:
                AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_PRODUCT_PURCHASED, OnProductPurchased);
                break;

            case StoreEvent.ON_RETRIEVE_PRODUCT_FINISHED:
                AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
                break;

            default:
                Debug.LogError("unhandled store event: " + storeEvent);
                break;
        }
    }

    private void OnProductPurchased(CEvent e)
    {
        BillingResult result = e.data as BillingResult;

        if (result.isSuccess)
        {
            //Debug.Log("result.purchase.developerPayload: " + result.purchase.developerPayload);
            //Debug.Log("result.purchase.orderId: " + result.purchase.orderId);
            //Debug.Log("result.purchase.signature: " + result.purchase.signature);
            RecordTransaction(result.purchase.SKU, result.purchase.signature, result.purchase.orderId);

            AcePurchaseVerifier.Get().VerifyGooglePurchase(result.purchase.originalJson, result.purchase.signature, m_Shopper.GetStoreKey(),
                new AceStoreProduct(result.purchase), OnPurchaseVerification);
            //ProcessPurchasedProduct(new AceStoreProduct(result.purchase));
        }
        else
        {
            //DisplayMessage("Product Purchase Failed", result.response.ToString() + " " + result.message);
        }

        Debug.Log("Purchased Response: " + result.response.ToString() + " " + result.message);
    }

    private void OnProductConsumed(CEvent e)
    {
        BillingResult result = e.data as BillingResult;

        if (result.isSuccess)
        {
            //RecordTransaction(result.purchase.SKU, result.purchase.signature, result.purchase.orderId);
            ProcessConsumedProduct(new AceStoreProduct(result.purchase));
        }
        else
        {
            //DisplayMessage("Product Cousume Failed", result.response.ToString() + " " + result.message);
        }

        Debug.Log("Consume Response: " + result.response.ToString() + " " + result.message);
    }


    private void OnBillingConnected(CEvent e)
    {
        BillingResult result = e.data as BillingResult;
        AndroidInAppPurchaseManager.instance.removeEventListener(AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);

        if (result.isSuccess)
        {
            //Store connection is Successful. Next we loading product and customer purchasing details
            AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
            AndroidInAppPurchaseManager.instance.retrieveProducDetails();
        }
        else
        {
            //DisplayMessage("Failed to Connect", result.response.ToString() + " " + result.message);
        }

        //AndroidMessage.Create("Connection Response", result.response.ToString() + " " + result.message);
        Debug.Log("Connection Response: " + result.response.ToString() + " " + result.message);
    }

    private void OnRetriveProductsFinised(CEvent e)
    {
        BillingResult result = e.data as BillingResult;
        AndroidInAppPurchaseManager.instance.removeEventListener(AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
        if (result.isSuccess)
        {
            m_IsInited = true;
            //UpdateStoreData();
            //_isInited = true;
            m_Shopper.StoreInitialized();
        }
        else
        {
			//DisplayMessage("Retrieve Products Failed", result.response.ToString() + " " + result.message);
        }
    }

    public override bool IsProductPurchased(string sku)
    {
        return AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(sku);
    }

    protected override void DisplayMessage(string title, string message)
    {
        AndroidMessage.Create(title, message);
    }
    #endregion
}
#endif