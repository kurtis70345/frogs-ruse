﻿using UnityEngine;
using System.Collections;


abstract public class AceStore
{
    #region Constants
    public enum StoreEvent
    {
        ON_PRODUCT_PURCHASED,
        ON_PRODUCT_CONSUMED,
        ON_BILLING_SETUP_FINISHED,
        ON_RETRIEVE_PRODUCT_FINISHED,

        NUM_EVENTS
    }

    public delegate void OnProcessProduct(AceStoreProduct product);
    #endregion

    #region Variables
    protected bool m_IsInited = false;
    protected AceStoreProductStock m_ProductStock;
    protected AceStoreShopper m_Shopper;
    OnProcessProduct m_PurchasedProductCBs;
    OnProcessProduct m_ConsumedProductCBs;
    #endregion

    #region Functions
    public AceStore(AceStoreProductStock productStock, AceStoreShopper shopper)
    {
        m_ProductStock = productStock; 
        m_Shopper = shopper;
        SetupCallbacks();
    }

    protected void SetupCallbacks() 
    {
        for (int i = 0; i < (int)StoreEvent.NUM_EVENTS; i++)
        {
            AddEventListener((StoreEvent)i);
        }
    }

    public void AddOnProcessPurchasedProduct(OnProcessProduct cb)
    {
        m_PurchasedProductCBs += cb;
    }

    public void AddOnProcessConsumedProduct(OnProcessProduct cb)
    {
        m_ConsumedProductCBs += cb;
    }

    protected void ProcessPurchasedProduct(AceStoreProduct product)
    {
        m_Shopper.OnProcessPurchasedProduct(product);
        /*if (m_PurchasedProductCBs != null)
        {
            //Debug.Log("ProcessPurchasedProduct callback is not null");
            m_PurchasedProductCBs(product);
        }*/
    }

    protected void ProcessConsumedProduct(AceStoreProduct product)
    {
        // NOTE THIS DOES NOTHING RIGHT NOW
        //m_Shopper.OnProcessPurchasedProduct(product);
        //if (m_ConsumedProductCBs != null)
        //{
        //    m_ConsumedProductCBs(product);
        //}
    }

    public float GetProductPrice(string sku)
    {
        float price = 0;
        string priceAsString = GetProductPriceAsString(sku);
        if (!float.TryParse(priceAsString, out price))
        {
            Debug.LogError("failed to find price for item " + sku + " price: " + priceAsString);
        }
        return price;
    }

    public string GetProductDisplayName(string sku) 
    { 
        AceStoreProduct product = m_ProductStock.GetProduct(sku);
        string productName = "";
        if (product != null)
        {
            productName = product.packageName;
        }
        else
        {
            Debug.LogError("GetProductDisplayName failed on sku " + sku);
        }
        return productName;
    }

    protected void RecordTransaction(string sku, string receipt, string orderId)
    {
        if (!string.IsNullOrEmpty(m_Shopper.m_TransactionRecordUrl))
        {
            m_Shopper.StartCoroutine(RecordTransactionCoroutine(sku, receipt, orderId));
        }
        else
        {
            Debug.LogError("Can't record transaction because the url is blank");
        }
    }

    IEnumerator RecordTransactionCoroutine(string sku, string receipt, string orderId)
    {
        WWWForm form = new WWWForm();
        form.AddField("sku", sku);
        form.AddField("receipt", receipt);
        form.AddField("order_id", orderId);
        form.AddField("platform", GetStoreName());

        WWW www = new WWW(m_Shopper.m_TransactionRecordUrl, form);
        yield return www;
    }

    public void OnPurchaseVerification(AcePurchaseVerifier.VerificationResults results)
    {
        if (results.success)
        {
            ProcessPurchasedProduct(results.product);
        }
        else
        {
            //DisplayMessage("Error", "The purchase failed to complete but you were not charged");
        }
    }

    virtual public void AddProduct(string sku) { }
    virtual public void LoadStore(string key) { }
    virtual public void PurchaseProduct(string sku) { }
    virtual public void ConsumeProduct(string sku) { } 
    virtual public bool IsProductPurchased(string sku) { return false; }
    virtual public string GetProductPriceAsString(string sku) { return m_ProductStock.GetProductPrice(sku).ToString("f2"); }
    protected virtual void DisplayMessage(string title, string message) { }
    virtual protected void AddEventListener(StoreEvent storeEvent) { }
    virtual protected string GetStoreName() { return "DefaultStore"; }
    #endregion
}
