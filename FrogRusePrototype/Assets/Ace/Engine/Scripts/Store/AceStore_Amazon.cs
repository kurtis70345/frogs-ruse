﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if ALLOW_ACE_DEPENDENCIES
#if true
public class AceStore_Amazon : AceStore
{
	public AceStore_Amazon(AceStoreProductStock stock, AceStoreShopper shopper)
		: base(stock, shopper)
	{

	}
}
#else
public class AceStore_Amazon : AceStore
{
	#region Variables
	//bool m_LoadedProducts = false;
	string m_UserId = "";
	List<AceStoreProduct> m_AvailableProducts = new List<AceStoreProduct>();
	#endregion

    #region Function
	public AceStore_Amazon(AceStoreProductStock stock, AceStoreShopper shopper)
        : base(stock, shopper)
    {
		AmazonIAPManager.itemDataRequestFinishedEvent += itemDataRequestFinishedEvent;
		AmazonIAPManager.onSdkAvailableEvent += onSdkAvailableEvent;
		AmazonIAPManager.purchaseSuccessfulEvent += purchaseSuccessfulEvent;
		AmazonIAPManager.onGetUserIdResponseEvent += onGetUserIdResponseEvent;
		Debug.Log("hi AMAZON STORE!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		AmazonIAP.initiateItemDataRequest(GetAmazonProductSkus());
    }

	string[] GetAmazonProductSkus()
	{
		string[] productSkus = null;
		List<AceStoreProduct> realMoneyProducts = m_ProductStock.GetAllRealMoneyProducts();
		if (realMoneyProducts.Count > 0)
		{
			productSkus = new string[realMoneyProducts.Count];
			for (int i = 0; i < realMoneyProducts.Count; i++)
			{
				productSkus[i] = realMoneyProducts[i].SKU;
			}
		}
		return productSkus;
	}

	void onGetUserIdResponseEvent(string userId)
	{
		Debug.Log("userId: " + userId);
		m_UserId = userId;
	}

	void onSdkAvailableEvent(bool isAvailable)
	{
		//Debug.Log("onSdkAvailableEvent!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		if (isAvailable && string.IsNullOrEmpty(m_UserId))
		{
			Debug.Log("initiateGetUserIdRequest");
			AmazonIAP.initiateGetUserIdRequest();
		}
	}

	void itemDataRequestFinishedEvent(List<string> skus, List<AmazonItem> products)
	{
		skus.ForEach(s => Debug.Log("unavailable sku: " + s));
		Debug.Log("itemDataRequestFinishedEvent: " + products.Count);
		//m_LoadedProducts = true;
		for (int i = 0; i < products.Count; i++)
		{
			m_AvailableProducts.Add(new AceStoreProduct(products[i].sku, products[i].price, true, products[i].type.ToLower() != "consumable"));
		}
	}

    protected override string GetStoreName()
    {
        return "Amazon";
    }

    public override void PurchaseProduct(string sku)
    {
		AmazonIAP.initiatePurchaseRequest(sku);
    }

    public override string GetProductPriceAsString(string sku)
    {
        string price = "";
        if (m_ProductStock.DoesProductExist(sku) && !m_ProductStock.RequiresRealMoney(sku))
        {
            price = m_ProductStock.GetProductPrice(sku).ToString("f2");
        }
        else
        {
			AceStoreProduct product = m_AvailableProducts.Find(p => p.SKU == sku);
			if (product != null)
			{
				price = product.costAsString;
			}
			else
			{
				Debug.LogError("GetProductPriceAsString failed on " + sku);
			}
        }
        return price;
    }

	private void purchaseSuccessfulEvent(AmazonReceipt receipt)
	{
		RecordTransaction(receipt.sku, receipt.token, receipt.token);
		OnPurchaseVerification(new AcePurchaseVerifier.VerificationResults(true, "", new AceStoreProduct(receipt.sku)));
	}

	/*
    private void OnProductPurchased(CEvent e)
    {
        BillingResult result = e.data as BillingResult;

        if (result.isSuccess)
        {
            //Debug.Log("result.purchase.developerPayload: " + result.purchase.developerPayload);
            //Debug.Log("result.purchase.orderId: " + result.purchase.orderId);
            //Debug.Log("result.purchase.signature: " + result.purchase.signature);
            RecordTransaction(result.purchase.SKU, result.purchase.signature, result.purchase.orderId);

            AcePurchaseVerifier.Get().VerifyGooglePurchase(result.purchase.originalJson, result.purchase.signature, m_Shopper.GetStoreKey(),
                new AceStoreProduct(result.purchase), OnPurchaseVerification);
        }
        else
        {
            DisplayMessage("Product Purchase Failed", result.response.ToString() + " " + result.message);
        }

        Debug.Log("Purchased Response: " + result.response.ToString() + " " + result.message);
    }


    private void OnBillingConnected(CEvent e)
    {
        BillingResult result = e.data as BillingResult;
        AndroidInAppPurchaseManager.instance.removeEventListener(AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);

        if (result.isSuccess)
        {
            //Store connection is Successful. Next we loading product and customer purchasing details
            AndroidInAppPurchaseManager.instance.addEventListener(AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
            AndroidInAppPurchaseManager.instance.retrieveProducDetails();
        }
        else
        {
            DisplayMessage("Failed to Connect", result.response.ToString() + " " + result.message);
        }

        //AndroidMessage.Create("Connection Response", result.response.ToString() + " " + result.message);
        Debug.Log("Connection Response: " + result.response.ToString() + " " + result.message);
    }

    private void OnRetriveProductsFinised(CEvent e)
    {
        BillingResult result = e.data as BillingResult;
        AndroidInAppPurchaseManager.instance.removeEventListener(AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
        if (result.isSuccess)
        {
            m_IsInited = true;
            //UpdateStoreData();
            //_isInited = true;
            m_Shopper.StoreInitialized();
        }
        else
        {
            AndroidMessage.Create("Retrieve Products Failed", result.response.ToString() + " " + result.message);
        }
    }

    public override bool IsProductPurchased(string sku)
    {
        return AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(sku);
    }*/

    protected override void DisplayMessage(string title, string message)
    {
		AmazonIAP.Log(message);
    }
    #endregion
}
#endif
#endif//#if ALLOW_ACE_DEPENDENCIES