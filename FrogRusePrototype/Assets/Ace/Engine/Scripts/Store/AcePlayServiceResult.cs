﻿using UnityEngine;
using System.Collections;

public class AcePlayServiceResult
{
    #region Constants
    public enum PlayServiceResponseCode
    {
        STATUS_OK,
        STATUS_INTERNAL_ERROR,
        STATUS_NETWORK_ERROR_OPERATION_DEFERRED,
        STATUS_NETWORK_ERROR_OPERATION_FAILED,
        STATUS_NETWORK_ERROR_NO_DATA,
        STATUS_CLIENT_RECONNECT_REQUIRED,
        STATUS_LICENSE_CHECK_FAILED,
        STATUS_NETWORK_ERROR_STALE_DATA,
        UNKNOWN_ERROR,


        STATUS_ACHIEVEMENT_UNLOCKED,
        STATUS_ACHIEVEMENT_UNKNOWN,
        STATUS_ACHIEVEMENT_NOT_INCREMENTAL,
        STATUS_ACHIEVEMENT_UNLOCK_FAILURE,

        STATUS_STATE_KEY_NOT_FOUND,
        STATUS_STATE_KEY_LIMIT_EXCEEDED
    }
    #endregion

    #region Variables
    public readonly PlayServiceResponseCode response;
    public readonly string message;

    public readonly string leaderboardId = "";
    public readonly string achievementId = "";
    #endregion

    #region Functions
    public AcePlayServiceResult() { } 

#if ALLOW_ACE_DEPENDENCIES
    public AcePlayServiceResult(GooglePlayResult result)
    {
        message = result.message;
        // TODO: CHECK THIS
        //leaderboardId = result.leaderboardId;
        //achievementId = result.achievementId;
        response = (PlayServiceResponseCode)result.response;
    }
#endif
    #endregion
}
