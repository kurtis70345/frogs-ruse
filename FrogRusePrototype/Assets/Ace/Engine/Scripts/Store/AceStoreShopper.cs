﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceStoreShopper : MonoBehaviour
{
    #region Constants
    public delegate void OnNotEnoughMoney(AceStoreProduct product);
    #endregion

    #region Variables
    public AceBankAccount m_InGameCurrencyAccount;
	public AcePlayServiceGame m_PlayService;
    public string m_TransactionRecordUrl = "";
    //public List<string> m_ProductSkus = new List<string>();
    protected AceStoreProductStock m_AvailableProducts = new AceStoreProductStock();
    protected OnNotEnoughMoney m_NotEnoughMoneyCB;
    protected string m_LastVerifiedRealMoneySku = "";
    protected AceStore.OnProcessProduct m_LastPurchaseCompleteCB;
    AceStore m_Store;
    #endregion

    #region Functions
    virtual public void Start()
    {
        Init(GetStoreKey(), OnProcessPurchasedProduct);
    }

    public void Init(string storeKey, AceStore.OnProcessProduct onProcessPurchasedProduct)
    {  

#if UNITY_ANDROID && !UNITY_EDITOR
		if (m_PlayService.m_UseGameCircle)
		{
			m_Store = new AceStore_Amazon(m_AvailableProducts, this);
		}
		else
		{
			m_Store = new AceStore_Android(m_AvailableProducts, this, storeKey);
		}
#elif UNITY_IPHONE && !UNITY_EDITOR
        m_Store = new AceStore_iOS(m_AvailableProducts, this);
#else
        m_Store = new AceStore_Unity(m_AvailableProducts, this);
#endif
        //for (int i = 0; i < m_ProductSkus.Count; i++)
        //{
        //    //Filling product list
        //    m_Store.AddProduct(m_ProductSkus[i]);
        //}

        LoadStore(storeKey);
        AddOnProcessPurchasedProduct(onProcessPurchasedProduct);
    }

    void LoadStore(string storeKey)
    {
        m_Store.LoadStore(storeKey);
    }

    virtual public void StoreInitialized() { }

	public bool IsProductPurchased(string sku)
	{
		return m_Store.IsProductPurchased(sku);
	}

    public void AddOnProcessPurchasedProduct(AceStore.OnProcessProduct cb)
    {
        m_Store.AddOnProcessPurchasedProduct(cb);
    }

    public void AddOnProcessConsumedProduct(AceStore.OnProcessProduct cb)
    {
        m_Store.AddOnProcessConsumedProduct(cb);
    }

    public string GetProductDisplayName(string sku)
    {
        return m_Store.GetProductDisplayName(sku);
    }

    public float GetProductPrice(string sku)
    {
        return m_Store.GetProductPrice(sku);
    }

    public string GetProductPriceAsString(string sku)
    {
        return m_Store.GetProductPriceAsString(sku);
    }

    public int GetAmountGivenFromProduct(string sku)
    {
        return m_AvailableProducts.GetAmountGiven(sku);
    }

    protected bool HasEnoughMoney(string sku)
    {
        return (ulong)GetProductPrice(sku) <= m_InGameCurrencyAccount.CurrentAmount;
    }

    protected void MakeRealMoneyPurchase(string sku)
    {
        m_Store.PurchaseProduct(sku);
    }

    protected void ConsumeProduct(string sku)
    {
        m_Store.ConsumeProduct(sku);
    }

    void MakeGameCurrencyPurchase(string sku, AceStore.OnProcessProduct purchaseCompleteCB)
    {
        if (HasEnoughMoney(sku))
        {
            AceStoreProduct purchasedProduct = m_AvailableProducts.GetProduct(sku);
            OnProcessPurchasedProduct(purchasedProduct);
            if (purchaseCompleteCB != null)
            {
                purchaseCompleteCB(purchasedProduct);
            }
        }
        else
        {
            NotEnoughMoney(m_AvailableProducts.GetProduct(sku));
        }

        // clear it out
        m_LastPurchaseCompleteCB = null;
    }

    public void PurchaseProduct(string sku)
    {
        OnPrePurchaseVerification(sku, null);
    }

    public void PurchaseProduct(string sku, AceStore.OnProcessProduct purchaseCompleteCB)
    {
        OnPrePurchaseVerification(sku, purchaseCompleteCB);
    }

    protected void PurchaseProduct__Internal(string sku, AceStore.OnProcessProduct purchaseCompleteCB)
    {
        if (m_AvailableProducts.DoesProductExist(sku))
        {
            if (m_AvailableProducts.RequiresRealMoney(sku))
            {
                MakeRealMoneyPurchase(sku);
            }
            else
            {
                MakeGameCurrencyPurchase(sku, purchaseCompleteCB);
            }
        }
    }

    public virtual void OnProcessPurchasedProduct(AceStoreProduct product)
    {
        Debug.Log("OnProcessPurchasedProduct callback called! " + product.packageName);
    }

    protected virtual void NotEnoughMoney(AceStoreProduct product)
    {

    }

    protected virtual void OnPrePurchaseVerification(string sku, AceStore.OnProcessProduct purchaseCompleteCB)
    {
        m_LastVerifiedRealMoneySku = sku;
        m_LastPurchaseCompleteCB = purchaseCompleteCB;
        PurchaseProduct__Internal(sku, purchaseCompleteCB);
    }

    public string GetStoreKey()
    {
        string key =  GetKey0() + GetKey1() + GetKey2() + GetKey3();
        //Debug.Log(key);
        return key;
    } 

    protected virtual string GetKey0() { return "NOT_"; }
    protected virtual string GetKey1() { return "VALID_"; }
    protected virtual string GetKey2() { return "KEY_"; }
    protected virtual string GetKey3() { return ""; }

    //private static void OnProcessingPurchasedProduct(GooglePurchaseTemplate purchase)
    //{
    //    //some stuff for processing product purchse. Add coins, unlock track, etc
    //    switch (purchase.SKU)
    //    {
    //        case COINS_ITEM:
    //            consume(COINS_ITEM);
    //            break;
    //        case COINS_BOOST:
    //            GameDataExample.EnableCoinsBoost();
    //            break;
    //    }
    //}

    //private static void OnProcessingConsumeProduct(GooglePurchaseTemplate purchase)
    //{
    //    switch (purchase.SKU)
    //    {
    //        case COINS_ITEM:
    //            GameDataExample.AddCoins(100);
    //            break;
    //    }
    //}

    #endregion
}
