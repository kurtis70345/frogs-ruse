﻿using UnityEngine;
using System.Collections;

public class AchievementUnlockTest : MonoBehaviour 
{
	#region Variables
	[SerializeField] AceAchievementUnlockedMenu m_Unlock;
	#endregion

	#region Functions
	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			m_Unlock.Display("Test Title", "Some Awesome Description", 100);
		}
	}
	#endregion
}
