﻿using UnityEngine;
using System.Collections;

public class AceScreenShotTaker_Texture : AceScreenShotTaker
{
    #region Variables
    public Rect m_ViewPortRect = new Rect(0, 0, 1, 1);
    #endregion

    #region Functions
    public override IEnumerator TakeScreenShotDelayed()
    {
        bool wasCameraActive = m_Camera.gameObject.activeSelf;
        m_Camera.gameObject.SetActive(true);

        // We should only read the screen after all rendering is complete
        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        float width = Screen.width;
        float height = Screen.height;
        m_Sceenshot = new Texture2D((int)(width * m_ViewPortRect.width), (int)(height * m_ViewPortRect.height), TextureFormat.RGB24, false);

        // Read screen contents into the texture
        m_Sceenshot.ReadPixels(new Rect(width * m_ViewPortRect.x, height * m_ViewPortRect.y, width * m_ViewPortRect.width, height * m_ViewPortRect.height), 0, 0);
        m_Sceenshot.Apply();

        if (!wasCameraActive)
        {
            m_Camera.gameObject.SetActive(false); 
        }    
    }
    #endregion
}
