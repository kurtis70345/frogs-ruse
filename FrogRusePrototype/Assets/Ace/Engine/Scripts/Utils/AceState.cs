using UnityEngine;
using System.Collections;

public class AceState
{
	public virtual void Update() {}
	public virtual void TransitionOn() {}
	public virtual void TransitionOff() {}
}
