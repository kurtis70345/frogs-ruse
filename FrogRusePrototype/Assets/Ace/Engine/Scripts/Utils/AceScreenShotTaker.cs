﻿using UnityEngine;
using System.Collections;

abstract public class AceScreenShotTaker : MonoBehaviour
{
    #region Variables
    public Camera m_Camera;
    protected Texture2D m_Sceenshot;
    #endregion

    #region Properties
    public Texture2D Screenshot
    {
        get { return m_Sceenshot; }
    }
    #endregion

    #region Functions
    abstract public IEnumerator TakeScreenShotDelayed();
    #endregion
}
