﻿using UnityEngine;
using System.Collections;

public class AceRatingPopUpCounter : MonoBehaviour
{
    #region Variables
    public int m_RatingPopUpInterval = 3;
    public string m_RateTitleKey = "Rate_Title";
    public string m_RateTextKey = "Rate_Text";
    public string m_AppUrl = "";
    #endregion

    #region Functions
    // Use this for initialization
	void Start()
    {
        if (PlayerPrefs.GetInt(AcePlayService.HasBeenRatedKey, 0) == 0)
        {
            int ratingCounter = PlayerPrefs.GetInt(AcePlayService.RatingCounterKey, 0) + 1;
            PlayerPrefs.SetInt(AcePlayService.RatingCounterKey, ratingCounter);
            if (ratingCounter % m_RatingPopUpInterval == 0)
            {
                // reset it
                PlayerPrefs.SetInt(AcePlayService.RatingCounterKey, 0);
				FindObjectOfType<AcePlayServiceGame>().ShowRatingPopUp(AceTranslator.Translate(m_RateTitleKey),
                    AceTranslator.Translate(m_RateTextKey), m_AppUrl);
            }
        }
	}
    #endregion
}
