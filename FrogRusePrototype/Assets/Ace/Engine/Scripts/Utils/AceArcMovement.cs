﻿using UnityEngine;
using System.Collections;

public class AceArcMovement : MonoBehaviour
{
    #region Constants
    public delegate void OnReachedArcEnd(AceArcMovement mover);
    #endregion

    #region Variables
    public float m_Speed;
    public float m_ArcHeight = 5;
    public Vector3 m_ABC;
    //Vector3 m_Direction = Vector3.zero;
    OnReachedArcEnd m_ReachedArcEndCBs;
    #endregion

    #region Functions
    // Use this for initialization
    void Start()
    {

    }

    //public void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Alpha1))
    //    {
    //        Move(Vector3.zero, new Vector3(10, 0, 0), Vector3.zero);
    //    }
    //}

    public void AddOnReachedArcEndCB(OnReachedArcEnd cb)
    {
        m_ReachedArcEndCBs += cb;
    }

    public void Move(Vector3 startPos, Vector3 targetPos, Vector3 targetVelocity)
    {
        Vector3 interceptionPos = Utils.Intercept(startPos, m_Speed, targetPos, targetVelocity);
        float timeToReachTarget = (interceptionPos - startPos).magnitude / m_Speed;
        //timeToReachTarget = Mathf.Max(timeToReachTarget, 1);
        //m_Direction = Utils.ShootAt(transform.position, interceptionPos, m_Speed);

        StartCoroutine(MoveTowardsTarget(startPos, targetPos, timeToReachTarget));
    }

    IEnumerator MoveTowardsTarget(Vector3 startPos, Vector3 endPos, float timeToReachTarget)
    {
        float timePassed = 0;
        //float yPos = startPos.y;
        Vector3 posHolder = startPos;
        Vector3 midPos = startPos;
        midPos.y = m_ArcHeight;//Mathf.Abs(Mathf.Max(startPos.y, endPos.y)) * 2;
        while (timePassed < timeToReachTarget)
        {
            //posHolder.x = startPos.x + 
            //
            //posHolder = startPos + m_Direction * (m_ABC.x * Mathf.Pow(timePassed, 2) + m_ABC.y * timePassed + m_ABC.z);
            //posHolder.y = startPos.y + (m_ABC.x * Mathf.Pow(timePassed, 2) + m_ABC.y * timePassed + m_ABC.z);
            posHolder = Utils.FollowCurve(startPos, midPos, endPos, timePassed / timeToReachTarget);

            //Debug.Log(posHolder.y);
            transform.position = posHolder;
            timePassed += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        if (m_ReachedArcEndCBs != null)
        {
            m_ReachedArcEndCBs(this);
        }
    }
    #endregion
   
}
