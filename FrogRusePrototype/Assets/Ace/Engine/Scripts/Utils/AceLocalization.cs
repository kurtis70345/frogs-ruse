﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AceLocalization : MonoBehaviour 
{
	#region Variables
	[SerializeField] TextAsset[] m_Languages;
	[SerializeField] static string m_CurrentLanguage = "en";
	public static AceLocalization m_instance;
	Dictionary<string, string> m_Dictionary = new Dictionary<string, string>();
	#endregion

	#region Properties
	public static AceLocalization instance 
	{
		get 
		{
			if (m_instance == null)
			{
				m_instance = Resources.Load<AceLocalization>("Localization/Localization");
				m_instance.LoadLanguage(AceLocalization.CurrentLanguage);
			}
			return m_instance;
		}
	}

	static public string CurrentLanguage { get { return m_CurrentLanguage; } }
	#endregion

	#region Functions
	void Awake()
	{
		if (m_instance == null)
		{
			m_instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
		LoadLanguage(m_CurrentLanguage);
	}

	public bool LoadLanguage(string langName)
	{
		TextAsset lang = FindLanguage(langName);
		bool success = false;
		if (lang != null)
		{
			m_CurrentLanguage = langName;
			success = LoadLanguage(lang);
		}
		return success;
	}

	public string[] GetLanguageNames()
	{
		string[] languages = new string[m_Languages.Length];
		for (int i = 0; i < m_Languages.Length; i++)
		{
			languages[i] = m_Languages[i].name;
		}
		return languages;
	}

	public string GetWord(string key)
	{
		if (key == null)
		{
			return string.Empty;
		}

		string word = key;
		if (m_Dictionary.ContainsKey(key))
		{
			word = m_Dictionary[key];
		}
		else
		{
			Debug.LogWarning(string.Format("Key {0} doesn't exist in language {1}", key, m_CurrentLanguage));
		}
		return word;
	}

	protected TextAsset FindLanguage(string langName)
	{
		TextAsset lang = Array.Find(m_Languages, l => l.name == langName);
		if (lang == null)
		{
			Debug.LogError("Failed to find language " + langName);
		}
		return lang;
	}

	protected bool LoadLanguage(TextAsset lang)
	{
		if (lang == null)
		{
			return false;
		}

		m_Dictionary.Clear();

		string[] lines = lang.text.Split('\n');
		foreach (string line in lines)
		{
			if (string.IsNullOrEmpty(line) || line[0] == '#' || line == "\r" || line == "\r\n")
			{
				continue;
			}

			//line = line.Trim();
			string[] kvp = line.Split('=');
			if (kvp == null || kvp.Length != 2)
			{
				Debug.LogWarning("Failed to load line " + line);
				continue;
			}

			string key = kvp[0].Trim();
			if (m_Dictionary.ContainsKey(key))
			{
				Debug.LogError("Duplicate key in language: " + lang.name + " Key: " + key);
				continue;
			}

			m_Dictionary.Add(key, kvp[1].Trim());
		}

		//PrintDictionary();

		return true;
	}

	void PrintDictionary()
	{
		foreach (KeyValuePair<string, string> kvp in m_Dictionary)
		{
			Debug.Log(kvp.Key + " " + kvp.Value);
		}
	}
	#endregion
}
