﻿using UnityEngine;
using System.Collections;

public class AceTriggerHandler : MonoBehaviour 
{
	#region Variables
	public GameObject m_Receiver;
	public string m_OnTriggerEnterCB = "";
	public string m_OnTriggerStayCB = "";
	public string m_OnTriggerExitCB = "";
	#endregion
	
	#region Functions
	void Start()
	{
		if (m_Receiver == null)
		{
			Debug.LogError(string.Format("AceTriggerHandler on gameobject {0} doesn't have a receiver", name));	
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (m_Receiver != null && !string.IsNullOrEmpty(m_OnTriggerEnterCB))
		{
			m_Receiver.SendMessage(m_OnTriggerEnterCB, other.gameObject);
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		if (m_Receiver != null && !string.IsNullOrEmpty(m_OnTriggerStayCB))
		{
			m_Receiver.SendMessage(m_OnTriggerStayCB, other.gameObject);
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		if (m_Receiver != null && !string.IsNullOrEmpty(m_OnTriggerExitCB))
		{
			m_Receiver.SendMessage(m_OnTriggerExitCB, other.gameObject);
		}
	}
	#endregion
}
