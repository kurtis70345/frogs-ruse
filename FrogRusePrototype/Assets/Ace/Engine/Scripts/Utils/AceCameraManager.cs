﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceCameraManager : MonoBehaviour 
{
	#region Variables
	public Camera m_DefaultCamera;
	public List<Camera> m_Cameras = new List<Camera>();
	#endregion

	#region Functions
	// Use this for initialization
	void Start()
	{
		ActivateCamera(m_DefaultCamera);
	}

	bool IsCameraManaged(Camera camera)
	{
		return m_Cameras.Find(cam => cam == camera) != null;
	}

	public void ActivateCamera(Camera camera)
	{
		if (!IsCameraManaged(camera))
		{
			m_Cameras.Add(camera);
		}

		for (int i = 0; i < m_Cameras.Count; i++)
		{
			if (m_Cameras[i] != camera)
			{
				m_Cameras[i].gameObject.SetActive(false);
			}
		}

		camera.gameObject.SetActive(true);
	}
	#endregion
}
