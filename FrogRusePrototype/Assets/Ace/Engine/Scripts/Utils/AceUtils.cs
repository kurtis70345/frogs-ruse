using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System.IO;
using System.Text;

//#define ALLOW_ACE_DEPENDENCIES

static public class Utils 
{
    public const int MouseLeft = 0;
    public const int MouseRight = 1;
    public const int MouseMiddle = 2;
    

    public const float GridWidth = 1;
    public const float GridHeight = 1;
	
	public const float DefaultScreenWidth = 480;
	public const float DefaultScreenHeight = 320;
	
	public const string ShaderMainColorName = "_Color";
	public static Vector3 RootScale = new Vector3(0.0025f, 0.0025f, 0.0025f);
    
	
	public enum InterpolationStyle
	{
		Lerp,
		Smooth_Step,
		Ease_In,
		Ease_Out,
	}

    public enum LevelNames
    {
        MainMenu,
		Main,
		LevelEditor,
		Tutorial,
    };
	
	public enum Zones
	{
		Sacred_Highlands = 1,
        Molten_Gateway = 5, // represnts the offset into LevelNames where the specific zone levels being
		Frozen_Fortress = 9,
		Ancient_Swamps = 13,
        Challenges = 17,

		NUM_ZONES = 4
	}

    public enum Axis
    {
        X_Axis,
        Y_Axis,
        Z_Axis,
    };

	public static void LoadLevel(int levelId)
	{
		Debug.Log("load level id: " + levelId);
		Application.LoadLevel(levelId);	
	}
	
	/*public static void LoadLevel(LevelNames level)
	{
		//Debug.Log("LoadLevel: " + level);
        string levelName = "MainMenu";
        switch (level)
        {
            case LevelNames.Main:
                levelName = "Main";
                break;

            case LevelNames.LevelEditor:
                levelName = "LevelEditor";
                break;

			case LevelNames.Tutorial:
				levelName = "Tutorial";
				break;

            case LevelNames.MainMenu:
            default:
                levelName = "MainMenu";
                break;   
        }
        
		// TODO 
        //PersistentData.Get().StartCoroutine(ResetLevelSettings(levelName));
		ResetLevelSettings(levelName);
		//Application.LoadLevel(levelName);	
	}*/
	
	public static void ReLoadLevel()
	{
        //PersistentData.Get().StartCoroutine(ResetLevelSettings(Application.loadedLevelName));
		LoadLevel(Application.loadedLevel);
	}

	static IEnumerator ResetLevelSettings(string levelName)
	{
		Time.timeScale = 1.0f;
        AceLoadingMenu.Get().Show();
		// we need all 3 of these waits otherwise the loading screen sometimes won't show
        yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
        if (Application.loadedLevelName == levelName)
        {
            Application.LoadLevel(Application.loadedLevel);
        }
        else
        {
            Application.LoadLevel(levelName);
        }
	}
	
	public static bool UsingTouchInput
	{
		get 
		{ 
			//Debug.Log("Application.platform: " + Application.platform);
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
            //return Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android;
            return true;
#else
            return false;
#endif
		}
	}

    public static float ConvertToGridUnits(float fNum)
    {
        return fNum * (GridWidth * GridHeight);
    }

    public static Vector3 RotateVector(Axis axis, float fAmount, Vector3 vec, bool bUseDegrees)
    {
        Vector3 retVal = new Vector3();

        if (bUseDegrees)
        {
            fAmount *= Mathf.Deg2Rad;
        }       

        // one of these might be wrong
        switch (axis)
        {
            case Axis.X_Axis:
                retVal.x = vec.x;
                retVal.y = vec.y * Mathf.Cos(fAmount) - vec.z * Mathf.Sin(fAmount);
                retVal.z = vec.y * Mathf.Sin(fAmount) + vec.z * Mathf.Cos(fAmount);
                break;

            case Axis.Y_Axis:
                retVal.x = vec.x * Mathf.Cos(fAmount) - vec.z * Mathf.Sin(fAmount);
                retVal.y = vec.y;
                retVal.z = vec.x * Mathf.Sin(fAmount) + vec.z * Mathf.Cos(fAmount);
                break;

            case Axis.Z_Axis:
                retVal.x = vec.x * Mathf.Cos(fAmount) - vec.y * Mathf.Sin(fAmount);
                retVal.y = vec.y * Mathf.Cos(fAmount) + vec.x * Mathf.Sin(fAmount);
                retVal.z = vec.z;
                break;
        }

        return retVal;
    }

    //A quadratic B�zier curve is the path traced by the function B(t), given points P0, P1, and P2,
    //mathbf{B}(t) = (1 - t)^{2}\mathbf{P}_0 + 2(1 - t)t\mathbf{P}_1 + t^{2}\mathbf{P}_2 \mbox{ , } t \in [0,1]. 
    public static Vector3 FollowCurve(Vector3 startPos, Vector3 middlePos, Vector3 endPos, float fDistanceAlongCurve)
    {     
        Vector3 retVal = Vector3.zero;
        fDistanceAlongCurve = Mathf.Clamp01(fDistanceAlongCurve);
        retVal = (float)Mathf.Pow((1 - fDistanceAlongCurve), 2) * startPos + 2 * (1 - fDistanceAlongCurve) * fDistanceAlongCurve
            * middlePos + (float)Mathf.Pow(fDistanceAlongCurve, 2) * endPos;
        return retVal;
    }

    public static float LargestQuadraticEquationRoot(float A, float B, float C)
    {
        return (B + Mathf.Sqrt(/*Mathf.Abs*/(B * B - 4 * A * C))) / (2 * A);
    }

    // finds the intercepting point 
    public static Vector3 Intercept(Vector3 shooter, float bullet_speed, Vector3 targetPos, Vector3 target_velocity)
    {
        float a = bullet_speed * bullet_speed - Vector3.Dot(target_velocity, target_velocity);
        float b = -2 * Vector3.Dot(target_velocity, targetPos - shooter);
        float c = -Vector3.Dot(targetPos - shooter, targetPos - shooter);

        return targetPos + LargestQuadraticEquationRoot(a, b, c) * target_velocity;
    }

    public static Vector3 ShootAt(Vector3 shooterPos, Vector3 interceptionPos, float bullet_speed)
    {
        Vector3 v = interceptionPos - shooterPos;
        return bullet_speed / Mathf.Sqrt(Vector3.Dot(v, v)) * v;
    }

    public static float AngleBetween(Vector3 vec1, Vector3 vec2)
    {
        if (vec1.sqrMagnitude != 1)
        {
            vec1.Normalize();
        }
        if (vec2.sqrMagnitude != 1)
        {
            vec2.Normalize();
        }

        float dotProduct = Vector3.Dot(vec1, vec2);
        dotProduct = Mathf.Clamp(dotProduct, -1, 1);

        //return Mathf.Abs(Mathf.Acos(dotProduct)) * Mathf.Rad2Deg;
		return Mathf.Acos(dotProduct) * Mathf.Rad2Deg;
    }

    public static void TurnFlagOn(ref int flagholder, int flag)
    {
        flagholder |= flag;
    }

    public static void TurnFlagOff(ref int flagholder, int flag)
    {
        flagholder &= ~flag;
    }

    public static bool IsFlagOn(int flagholder, int flag)
    {
        return (flagholder & flag) == flag;
    }

    public static void ToggleFlag(ref int flagholder, int flag)
    {
        flagholder ^= flag;
    }

    public static void TurnFlagOn(ref uint flagholder, uint flag)
    {
        flagholder |= flag;
    }

    public static void TurnFlagOff(ref uint flagholder, uint flag)
    {
        flagholder &= ~flag;
    }

    public static bool IsFlagOn(uint flagholder, uint flag)
    {
        return (flagholder & flag) == flag;
    }

    public static void ToggleFlag(ref uint flagholder, uint flag)
    {
        flagholder ^= flag;
    }

    public static int FindFirstOpenIndex(ArrayList a)
    {
        int retVal = a.Count;
        for (int i = 0; i < a.Count; i++)
        {
            if (a[i] == null)
            {
                retVal = i;
                break;
            }
        }

        return retVal;
    }

    public static Vector2 LerpVector2(Vector2 val1, Vector2 val2, float t)
    {
        t = Mathf.Clamp01(t);
        return val1 + (val2 - val1) * t;
    }

    public static Vector3 LerpVector(Vector3 vec1, Vector3 vec2, float t)
    {
		t = Mathf.Clamp01(t);
        return vec1 + (vec2 - vec1) * t;
    }
	
	public static Vector3 SmoothStep(Vector3 a, Vector3 b, float t)
	{
		return Vector3.Lerp(a, b, t * t * (3.0f - (2.0f * t)));
	}
	
	public static float SmoothStep(float a, float b, float t)
	{
		return Mathf.Lerp(a, b, t * t * (3.0f - (2.0f * t)));
	}

    public static float SecondsToMinutes(float fNumSeconds)
    {
        return fNumSeconds / 60.0f;
    }

    public static float MinutesToSeconds(float fNumMinutes)
    {
        return fNumMinutes * 60.0f;
    }

    public static void RenderForwardVector(GameObject obj, float fMagnitudeModifier)
    {
        Gizmos.color = new Color(1, 0, 0);
        Gizmos.DrawRay(obj.transform.position, obj.transform.forward * fMagnitudeModifier);
        Gizmos.color = new Color(1, 1, 1);
    }

    public static Vector3 Convert3DPosTo2D(Vector3 pos3D, bool bInvertY)
    {
		return Convert3DPosTo2D(pos3D, bInvertY, Camera.main);
    }

	public static Vector3 Convert3DPosTo2D(Vector3 pos3D, bool bInvertY, Camera camera)
	{
		Vector3 retVal = camera.WorldToScreenPoint(pos3D);
		if (bInvertY)
		{
			retVal.y = Screen.height - retVal.y;
		}        
		return retVal;
	}

    public static Vector3 Convert3DPosTo2DWithOffset(Vector3 pos3D, float xOffset, float yOffset, bool bInvertY)
    {
        Vector3 retVal = Convert3DPosTo2D(pos3D, bInvertY);
        retVal.x -= xOffset;
        retVal.y -= yOffset;
        return retVal;
    }

    public static bool IsOnScreen(Rect r)
    {
        bool retVal = true;

        if (r.x > Screen.width || r.y > Screen.height || r.x + r.width < 0 || r.y + r.height < 0)
        {
            retVal = false;
        }

        return retVal;
    }

    public static bool IsPowerOf2(uint val)
    {
        return (val & (val - 1)) == 0;
    }

    static public int ConvertFlagToIndex(uint flag)
    {
        if (!IsPowerOf2(flag))
        {
            Debug.LogError("Can't ConvertFlagToIndex because " + flag.ToString() + " isn't a power of 2");
            return 0;
        }
        return (int)Mathf.Log(flag, 2);
    }

    static public int ConvertIndexToFlag(int index)
    {
        return 1 << index;
    }

    static public void Convert2DIndexTo1D(int nRow, int nCol, int nNumCols, ref int nIndex)
    {
        nIndex = nRow * nNumCols + nCol;
    }

    static public void Convert1DIndexTo2D(int nIndex, int nNumCols, ref int nRow, ref int nCol)
    {
        nRow = nIndex / nNumCols;
        nCol = nIndex % nNumCols;
    }

    public static string RemovePath(string str)
    {
        if (str == null || str.Length <= 0)
        {
            return null;
        }

        int index = str.LastIndexOf('/');
        if (index > -1)
        {
            str = str.Remove(0, index + 1);
        }
		else
		{
			index = str.LastIndexOf('\\');
			if (index > -1)
			{
				str = str.Remove(0, index + 1);
			}
		}

        return str;
    }

    public static string RemoveFileExtension(string str)
    {
        if (str == null || str.Length <= 0)
        {
            return null;
        }

        int index = str.LastIndexOf('.');
        if (index > -1)
        {
			//Debug.Log("index: " + index);
            str = str.Remove(index, str.Length - index);
        }
        return str;
    }

    public static int NameToShiftedLayer(string layerName)
    {
        return 1 << LayerMask.NameToLayer(layerName);
    }

    public static string GetResourcePathTo(string pathToThis)
    {
        return string.Format("{0}/{1}", Application.dataPath, pathToThis);
    }

    public static string GetApplicationPathTo(string pathToThis)
    {
        return Application.dataPath + "\\" + pathToThis;
    }
	
	public static string GetSaveLoadPath()
	{
#if UNITY_EDITOR
		return string.Format("{0}/{1}", Application.dataPath, "Resources/Levels");
#else
		return Application.persistentDataPath;
#endif
	}

    public static bool CloseEnoughToPos(Vector3 myPos, Vector3 targetPos, float fCloseEnoughDistanceSqr, bool bIncludeHeight)
    {
        bool retVal = false;

        if (!bIncludeHeight)
        {
            targetPos.y = 0;
            myPos.y = 0;
        }

        float distanceSqrd = (targetPos - myPos).sqrMagnitude;

        if (distanceSqrd <= fCloseEnoughDistanceSqr)
        {
            retVal = true;
        }

        return retVal;
    }

    public static bool PointInCircle(Vector2 center, float fRadius, Vector2 point)
    {
        bool retVal = false;
        if ((center - point).sqrMagnitude <= fRadius * fRadius)
        {
            retVal = true;
        }

        return retVal;
    }

    public static bool PointInSphere(Vector3 center, float fRadius, Vector3 point)
    {
        bool retVal = false;

        if ((center - point).sqrMagnitude <= fRadius * fRadius)
        {
            retVal = true;
        }

        return retVal;
    }
	
	public static bool PointInSphereIgnoreHeight(Vector3 center, float fRadius, Vector3 point)
    {
        bool retVal = false;
		center.y = point.y = 0;

        if ((center - point).sqrMagnitude <= fRadius * fRadius)
        {
            retVal = true;
        }

        return retVal;
    }

    public static float CalcTimeToReachPos(Vector3 startPos, Vector3 endPos, float fSpeed)
    {
		return fSpeed != 0 ? ((endPos - startPos).magnitude) / fSpeed : 0;
    }

    public static Vector3 CalcVelocity(Vector3 startPos, Vector3 endPos, float fSpeed)
    {
        Vector3 retVal = endPos - startPos;
        retVal.Normalize();
        retVal *= fSpeed;
        return retVal;
    }

    public static GameObject FindChild(GameObject pRoot, string pName)
    {
		Transform childTransform = pRoot.transform.Find(pName);
		return childTransform != null ? childTransform.gameObject : null;
        //return pRoot.transform.Find(pName).gameObject;
    }

    public static GameObject FindChildDeep(GameObject pRoot, string pName)
    {
        Stack<Transform> children = new Stack<Transform>();
        children.Push(pRoot.transform);
        while (children.Count > 0)
        {
            Transform child = children.Pop();

            if (child.name == pName)
            {
                return child.gameObject;
            }

            for (int i = 0; i < child.childCount; i++)
            {
                children.Push(child.GetChild(i));
            }
        }

        Debug.LogError(string.Format("Wasn't able to find gameobject named {0} as a child of {1}", pName, pRoot.name));
        return null;
    }

    public static void ForceTransformToDefaults(Transform t)
    {
        t.localPosition = Vector3.zero;
        t.localRotation = Quaternion.identity;
        t.localScale = Vector3.one;
    }

    public static Vector3 GetRandomPointInEllipsoid(Vector3 center, Vector3 ranges)
    {
        center.x += Mathf.Lerp(-ranges.x, ranges.x, Random.value);
        center.y += Mathf.Lerp(-ranges.y, ranges.y, Random.value);
        center.z += Mathf.Lerp(-ranges.z, ranges.z, Random.value);
        return center;
    }
	
	public static IEnumerator LerpColorOverTime(GameObject obj, Color targetColor, float numSeconds, string shaderColorName)
	{
		float deltaTime = 0;
		Renderer objRenderer = obj.GetComponent<Renderer>();
		Color originalColor = objRenderer.material.GetColor(shaderColorName);
		Color newColor = originalColor;
	 	
		while (true)
		{
			deltaTime = Mathf.Clamp(deltaTime, 0, numSeconds);
			newColor = Color.Lerp(originalColor, targetColor, deltaTime / numSeconds);
			objRenderer.material.SetColor(shaderColorName, newColor);
			//objRenderer.material.color = newColor;
			                                 
			deltaTime += Time.deltaTime;	
			
			if (deltaTime >= numSeconds)
			{
				break;	
			}
			yield return new WaitForEndOfFrame();
		}

	}

    public static bool IsValidRowCol(int row, int col, int numRows, int numCols)
    {
        bool retVal = false;

        if (row >= 0 && row < numRows && col >= 0 && col < numCols)
        {
            retVal = true;
        }

        return retVal;
    }
	
	public static bool IsNullOrEmpty(string s)
	{
		return s == null || s.Length == 0;	
	}
	
	public static void Swap<T>(ref T a, ref T b)
	{
		T temp = a;
		a = b;
		b = temp;
	}
	
	public static Vector2 ConvertToScaledPos(float x, float y)
	{
		return new Vector2(x / DefaultScreenWidth * Screen.width, y / DefaultScreenHeight * Screen.height);	
	}


	public static string ConvertListToString(List<int> vals, char seperator)
	{
        if (vals == null || vals.Count == 0)
        {
            return string.Empty;
        }

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < vals.Count - 1; i++)
		{
			builder.Append(vals[i]);
			builder.Append(seperator);
		}

		builder.Append(vals[vals.Count - 1]);
		return builder.ToString();
	}
	
	public static Rect ConvertToScaledPos(float x, float y, float width, float height)
	{
		Rect retVal = new Rect(x, y, width, height);
		retVal.x = x / DefaultScreenWidth * Screen.width;
		retVal.y = y / DefaultScreenHeight * Screen.height;
		retVal.width = width / DefaultScreenWidth * Screen.width;
		retVal.height = height / DefaultScreenHeight * Screen.height;
		return retVal;
	}
	
	public static Rect ConvertToScaledPos(Rect rect)
	{
		return ConvertToScaledPos(rect.x, rect.y, rect.width, rect.height);
	}
	
	public static float Interpolate(InterpolationStyle style, float a, float b, float t)
	{
		switch (style)
		{
		case InterpolationStyle.Lerp:
			return Mathf.Lerp(a, b, t);
			
		case Utils.InterpolationStyle.Smooth_Step:
			return Utils.SmoothStep(a, b, t);
			
		case Utils.InterpolationStyle.Ease_In:
		{
			float smoothStep = Utils.SmoothStep(a, b, t);
			float lerp = Mathf.Lerp(a, b, t);
			return Mathf.Lerp(smoothStep, lerp, t);
		}
			
		case Utils.InterpolationStyle.Ease_Out:
		{
			float smoothStep = Utils.SmoothStep(a, b, t);
			float lerp = Mathf.Lerp(a, b, t);
			return Mathf.Lerp(lerp, smoothStep, t);
		}
			
		default:
			return Mathf.Lerp(a, b, t);
		}
	}
	
	public static Vector3 Interpolate(InterpolationStyle style, Vector3 a, Vector3 b, float t)
	{
		switch (style)
		{
		case Utils.InterpolationStyle.Lerp:
			return Vector3.Lerp(a, b, t);
			
		case Utils.InterpolationStyle.Smooth_Step:
			return Utils.SmoothStep(a, b, t);
			
		case Utils.InterpolationStyle.Ease_In:
		{
			Vector3 smoothStep = Utils.SmoothStep(a, b, t);
			Vector3 lerp = Vector3.Lerp(a, b, t);
			return Vector3.Lerp(smoothStep, lerp, t);
		}
			
		case Utils.InterpolationStyle.Ease_Out:
		{
			Vector3 smoothStep = Utils.SmoothStep(a, b, t);
			Vector3 lerp = Vector3.Lerp(a, b, t);
			return Vector3.Lerp(lerp, smoothStep, t);
		}
			
		default:
			return Vector3.Lerp(a, b, t);
		}
	}
	
	public static string GetAppDataPath()
	{
		string dataPath = string.Empty;
//#if UNITY_IPHONE
//        {
//            //dataPath = Application.dataPath.Replace("/Data", "/Documents/");
			
//            // Your game has read+write access to /var/mobile/Applications/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/Documents 
//            // Application.dataPath returns              
//            // /var/mobile/Applications/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/myappname.app/Data 
//            // Strip "/Data" from path 
//            dataPath = Application.dataPath.Substring (0, Application.dataPath.Length - 5); 
//            // Strip application name 
//            dataPath = dataPath.Substring(0, dataPath.LastIndexOf('/'));  
//            dataPath += "/Documents/"; 
//        }
//#elif UNITY_ANDROID
        //Debug.Log("Application.persistentDataPath: " + Application.persistentDataPath);
        dataPath = Application.persistentDataPath + '/';
//#else 
//        //else
//        {
//            dataPath = Application.persistentDataPath + '/';	
//        }
//#endif


        return dataPath;
	}
	
	// this function won't work if the value being wrapped is too large or too small
	// this can be fixed with % operator, but that's too expensive if this function is being called
	// every frame
	public static float Wrap01(float val)
	{
		if (val > 1.0f)
		{
			val -= 1.0f;	
		}
		else if (val < 0)
		{
			val = 1.0f + val;	
		}
		return val;
	}
	
	public static Vector2 Wrap01(Vector2 val)
	{
		Wrap01(val.x);
		Wrap01(val.y);
		return val;
	}
	
	public static IEnumerator WaitForSeconds(float seconds)
	{
		yield return new WaitForSeconds(Mathf.Abs(seconds));	
	}
	
	public static Vector3 ConvertWorldPositionToOrthoPosition(Vector3 pos, Camera perspectiveCam, Camera orthoCam)
	{
		Vector3 retVal = perspectiveCam.WorldToScreenPoint(pos);
		retVal.x -= Screen.width / 2;
		retVal.y += (orthoCam.transform.position.y - orthoCam.orthographicSize);
		retVal.z = orthoCam.transform.position.z + 50;
		return retVal;
	}
	
	public static Vector3 ConvertOrthoPositionToWorldPosition(Vector3 pos, Camera perspectiveCam, Camera orthoCam)
	{
		Vector3 retVal = orthoCam.ScreenToWorldPoint(pos);
		
		return retVal;
	}
	
	public static T[] DeepCopyArray<T>(T[] source)
	{
		T[] dest = null;
		if (source != null && source.Length > 0)
		{
			dest = new T[source.Length];
			for (int i = 0; i < dest.Length; i++)
			{
				dest[i] = source[i];
			}
		}
		else
		{
			Debug.LogError("CopyArray failed, the source array is null or empty");
		}
		
		return dest;
	}

	public static void DeleteChildren(Transform t)
	{
		for (int i = 0; i < t.childCount; i++)
		{
			Transform child = t.GetChild(i);
			GameObject.Destroy(child.gameObject);
		}
	}

	public static bool IsIntersecting(Collider c1, Collider c2)
	{
		return IsIntersecting(c1.bounds, c2.bounds);
	}

	public static bool IsIntersecting(Bounds b1, Bounds b2)
	{
		return b1.Intersects(b2);
	}

	public static bool Intersect(Rect a, Rect b)
	{
		/*return ( Mathf.Abs(rectA.x - rectB.x) < (Mathf.Abs(rectA.width + rectB.width) / 2)) &&
			(Mathf.Abs(rectA.y - rectB.y) < (Mathf.Abs(rectA.height + rectB.height) / 2));*/
		bool c1 = a.x < b.xMax;
		bool c2 = a.xMax > b.x;
		bool c3 = a.y < b.yMax;
		bool c4 = a.yMax > b.y;
		return c1 && c2 && c3 && c4;
	}

	public static void Shuffle<T>(this IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = Random.Range(0, n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}
}
