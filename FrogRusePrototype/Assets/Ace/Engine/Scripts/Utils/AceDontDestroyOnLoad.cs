﻿using UnityEngine;
using System.Collections;

public class AceDontDestroyOnLoad : MonoBehaviour 
{
	#region Variables
	Transform m_Transform;
	static string m_UniqueName = "";
	#endregion

	#region Function
	void Awake()
	{
		if (string.IsNullOrEmpty(m_UniqueName))
		{
			m_UniqueName = name;
			m_Transform = transform;
			DontDestroyOnLoad(m_Transform.gameObject);
		}
		else if (name == m_UniqueName)
		{
			Destroy(gameObject);
		}
	}

	#endregion
}
