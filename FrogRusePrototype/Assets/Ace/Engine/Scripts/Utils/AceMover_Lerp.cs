﻿using UnityEngine;
using System.Collections;

public class AceMover_Lerp : MonoBehaviour
{
    #region Constants
    public delegate void OnMoveCompleted();
    #endregion

    #region Variables
	//public Transform m_Target;
    #endregion

    #region Functions
    public void Move(GameObject go, float completionTime, OnMoveCompleted moveCompletedCB)
    {
        Move(go.transform.position, completionTime, moveCompletedCB);
    }

    public void Move(Transform trans, float completionTime, OnMoveCompleted moveCompletedCB)
    {
        Move(trans.position, completionTime, moveCompletedCB);
    }

    public void Move(Vector3 targetPos, float completionTime, OnMoveCompleted moveCompletedCB)
    {
        StartCoroutine(MoveObject(targetPos, completionTime, moveCompletedCB));
    }

    IEnumerator MoveObject(Vector3 targetPos, float completionTime, OnMoveCompleted moveCompletedCB)
    {
        float timePassed = 0;
        Vector3 startingPos = transform.position;
        while (timePassed < completionTime)
        {
            transform.position = Vector3.Lerp(startingPos, targetPos, timePassed / completionTime);
            timePassed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.position = targetPos;

        if (moveCompletedCB != null)
        {
            moveCompletedCB();
        }
    }
    #endregion
}
