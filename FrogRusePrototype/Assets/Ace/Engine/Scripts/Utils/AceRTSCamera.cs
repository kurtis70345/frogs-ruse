using UnityEngine;
using System.Collections;

public class AceRTSCamera : MonoBehaviour
{
    // ------>  Constants
    public enum ScrollDirectionFlags
    {
        Not_Scrolling = 0,
        Left = 1,
        Right = 1 << 1,
        Up = 1 << 2,
        Down = 1 << 3,
    }
	
	static Vector2 HorizontalVec = new Vector2(1, 0);

    // ------>  Variables
    public float m_fScrollSpeed = 10.0f;
	public float m_fIPhoneScrollSpeed = 5.0f;
    public float m_fZoomSpeed = 10.0f;
    public float m_fScrollArea = 0.05f;
    public float m_fMinZoomOffset = 3;
    public float m_fMaxZoomOffset = 5.0f;
    public float m_fZoomIncrement = 0.2f;
    public float m_fZoom = 0;
    public bool m_bCanZoom = false;
	public bool m_bScrollingEnabled = true;
	public Rect m_LevelBoundariesOffset = new Rect();

    Rect m_LevelBoundaries = new Rect();
    Vector3 m_ForwardDirection;
    Vector3 m_SideDirection;
    //Vector3 m_PreviousPos;
    //Vector3 m_AmountMoved;
	Vector3 m_PosClamper = new Vector3();
	Vector2 m_ZoomClamper;
	Transform m_Transform;
	Rigidbody m_RigidBody;
	//Camera m_Camera;
	
	//Vector3 m_Acceleration = new Vector3();
	//Vector3 m_Velocity = new Vector3();
	Vector3 m_NetForce = new Vector3();
	
	
	public bool ScrollingEnabled
	{
		get { return m_bScrollingEnabled;}
		set { m_bScrollingEnabled = value;}
	}


    public Vector3 Position
    {
        get { return m_Transform.localPosition; }
    }
	
	public void AllowMovement(bool bAllow)
	{
		ScrollingEnabled = bAllow;
		m_bCanZoom = bAllow;
		
		if (!bAllow)
		{
			GetComponent<Rigidbody>().velocity = Vector3.zero;
		}
	}


    void Start()
    {
		m_Transform = transform;
		m_RigidBody = GetComponent<Rigidbody>();
			
        m_fZoomIncrement = Mathf.Clamp01(m_fZoomIncrement);

        m_ZoomClamper.x = m_Transform.position.y - m_fMinZoomOffset;
        m_ZoomClamper.y = m_Transform.position.y + m_fMaxZoomOffset;

        m_ForwardDirection = m_Transform.forward;
        m_ForwardDirection.y = 0;
        m_ForwardDirection.Normalize();

        m_SideDirection = m_Transform.right;
        m_SideDirection.y = 0;
        m_SideDirection.Normalize();
		
		//m_Camera = (Camera)GetComponentInChildren(typeof(Camera));

        // get the terrain info
        GameObject terrainObj = GameObject.FindGameObjectWithTag("Terrain");
        if (terrainObj)
        {
            Collider terrainSize = (Collider)terrainObj.GetComponent(typeof(Collider));
            if (terrainSize)
            {
                float fZOffset = 0;
                m_LevelBoundaries.xMin = terrainSize.transform.localPosition.x;
                m_LevelBoundaries.xMax = m_LevelBoundaries.xMin + terrainSize.bounds.size.x;
                m_LevelBoundaries.yMin = terrainSize.transform.localPosition.z - fZOffset;
                m_LevelBoundaries.yMax = m_LevelBoundaries.yMin + terrainSize.bounds.size.z + 5;			
				
				m_LevelBoundaries.x += m_LevelBoundariesOffset.x;
				m_LevelBoundaries.y += m_LevelBoundariesOffset.y;
				m_LevelBoundaries.xMax += m_LevelBoundariesOffset.width;
				m_LevelBoundaries.yMax += m_LevelBoundariesOffset.height;
                
            }
            else
            {
                Debug.Log("The terrain doesn't have a collider component");
            }
        } 
        else
        {
			m_LevelBoundaries.xMin = 0;
			m_LevelBoundaries.yMin = -10;
            m_LevelBoundaries.xMax = 22;
            m_LevelBoundaries.yMax = 22;
            //Debug.LogError("RTS Camera failed to find terrain");
        }


        //m_PreviousPos = Position;
    }

    void FixedUpdate()
	//void Update()
    {
        //m_PreviousPos = m_Transform.localPosition;

        //KeepMouseInGameWindow();

        bool bUsingKeysToScroll = false;

		if (m_bScrollingEnabled && Input.touchCount < 2)
		{
			// which way are we scrolling?
	        uint scrollFlags = GetScrollDirection(ref bUsingKeysToScroll);
	
	        // scroll it!
	        OnScroll(scrollFlags, CalculateScrollAmount(scrollFlags, bUsingKeysToScroll));
		}
        

        if (m_bCanZoom)
        {
			if (!Utils.UsingTouchInput)
			{
				if (Input.GetAxis("Mouse ScrollWheel") < 0)
	            {
	                OnScrollZoom(-m_fZoomSpeed);
	            }
	            else if (Input.GetAxis("Mouse ScrollWheel") > 0)
	            {
	                OnScrollZoom(m_fZoomSpeed);
	            }
			}
			else
			{
				if (AceiPhoneInput.IsTouchingScreen(0) && AceiPhoneInput.IsTouchingScreen(1))
				{
					OnTouchZoom();
				}
			}
        }
		
		/*m_Acceleration = m_NetForce; // mass is 1, so we don't need to divide
		m_Velocity += (m_Acceleration * 0.1f * Time.deltaTime);
		m_Transform.position += m_Velocity * Time.deltaTime;
		m_NetForce = Vector3.zero;
		if (m_Acceleration == Vector3.zero && m_Velocity.magnitude < 2.0f)
		{
			m_Velocity = Vector3.zero;
		}
		
		Debug.Log("m_Velocity: " + m_Velocity + " m_Velocity.magnitude: " + m_Velocity.magnitude
		          + " m_Acceleration: " + m_Acceleration);*/

        //m_AmountMoved = m_Transform.localPosition - m_PreviousPos;
    }
	
	void AddForce(Vector3 force)
	{
		m_NetForce += force;
	}

    void KeepMouseInGameWindow()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.x = Mathf.Clamp(0, Screen.width, mousePos.x);
        mousePos.y = Mathf.Clamp(0, Screen.height, mousePos.y);
    }

    void CheckCameraBounds()
    {
		m_PosClamper.x = Mathf.Clamp(m_Transform.position.x, m_LevelBoundaries.xMin, m_LevelBoundaries.xMax);
		m_PosClamper.y = Mathf.Clamp(m_Transform.position.y, m_ZoomClamper.x, m_ZoomClamper.y);
		m_PosClamper.z = Mathf.Clamp(m_Transform.position.z, m_LevelBoundaries.yMin, m_LevelBoundaries.yMax);
		m_Transform.position = m_PosClamper;
    }

    uint GetScrollDirection(ref bool bUsingKeysToScroll)
    {
        Vector3 mousePos = Input.mousePosition;
        uint retVal = (uint)ScrollDirectionFlags.Not_Scrolling;
        bUsingKeysToScroll = false;

		if (Utils.UsingTouchInput)
		{
			if (Input.touchCount > 1 || Input.touchCount < 1)
			{
				// they are trying to zoom, not scroll
				return (uint)ScrollDirectionFlags.Not_Scrolling;
			}
			bUsingKeysToScroll = true;
			
			Vector2 amountMoved = AceiPhoneInput.GetAmountDragged();
			Vector2 absAmountMoved = amountMoved;
			absAmountMoved.x = Mathf.Abs(absAmountMoved.x); 
			absAmountMoved.y = Mathf.Abs(absAmountMoved.y);
			absAmountMoved = Vector3.Normalize(absAmountMoved);
			float angleBetween = Vector2.Dot(absAmountMoved, HorizontalVec);

			if (angleBetween >= 0.866f) // <=30 degrees
			{
				// swiping horizontally	
				retVal |= amountMoved.x > 0 ? (uint)ScrollDirectionFlags.Left : (uint)ScrollDirectionFlags.Right;
			}
			else if (angleBetween <= 0.5f) // >=60 degress
			{
				// swiping vertically
				retVal |= amountMoved.y > 0 ? (uint)ScrollDirectionFlags.Down : (uint)ScrollDirectionFlags.Up;
			}
			else
			{
				// swiping diaganolly
				retVal |= amountMoved.x > 0 ? (uint)ScrollDirectionFlags.Left : (uint)ScrollDirectionFlags.Right;
				retVal |= amountMoved.y > 0 ? (uint)ScrollDirectionFlags.Down : (uint)ScrollDirectionFlags.Up;
			}
		}
		else
		{
			if (mousePos.x <= Screen.width * m_fScrollArea || IsScrollingKeyDown(ScrollDirectionFlags.Left))
	        {
	            Utils.TurnFlagOn(ref retVal, (uint)ScrollDirectionFlags.Left);
	
	            if (IsScrollingKeyDown(ScrollDirectionFlags.Left))
	            {
	                bUsingKeysToScroll = true;
	            }
	        }
	        else if (mousePos.x >= Screen.width * (1.0f - m_fScrollArea) || IsScrollingKeyDown(ScrollDirectionFlags.Right))
	        {
	            Utils.TurnFlagOn(ref retVal, (uint)ScrollDirectionFlags.Right);
	
	            if (IsScrollingKeyDown(ScrollDirectionFlags.Right))
	            {
	                bUsingKeysToScroll = true;
	            }
	        }
	
	        if (mousePos.y >= Screen.height * (1.0f - m_fScrollArea) || IsScrollingKeyDown(ScrollDirectionFlags.Up))
	        {
	            Utils.TurnFlagOn(ref retVal, (uint)ScrollDirectionFlags.Up);
	
	            if (IsScrollingKeyDown(ScrollDirectionFlags.Up))
	            {
	                bUsingKeysToScroll = true;
	            }
	        }
	        else if (mousePos.y <= Screen.height * m_fScrollArea || IsScrollingKeyDown(ScrollDirectionFlags.Down))
	        {
	            Utils.TurnFlagOn(ref retVal, (uint)ScrollDirectionFlags.Down);
	
	            if (IsScrollingKeyDown(ScrollDirectionFlags.Down))
	            {
	                bUsingKeysToScroll = true;
	            }
	        }
		}
        

        return retVal;
    }

    public void Scroll(ScrollDirectionFlags direction)
    {
        OnScroll((uint)direction, CalculateScrollAmount((uint)direction, true));
    }

    void OnScroll(uint scrollFlags, Vector3 fScrollAmount)
    { 
        if (Utils.IsFlagOn(scrollFlags, (uint)ScrollDirectionFlags.Left))
        {
            //m_Transform.position += (-m_SideDirection * fScrollAmount.x);
			m_RigidBody.AddForce(-m_SideDirection * fScrollAmount.x);
			//AddForce(-m_SideDirection * fScrollAmount.x);
        }
        else if (Utils.IsFlagOn(scrollFlags, (uint)ScrollDirectionFlags.Right))
        {
            //m_Transform.position += (m_SideDirection * fScrollAmount.x);
			m_RigidBody.AddForce(m_SideDirection * fScrollAmount.x);
			//AddForce(m_SideDirection * fScrollAmount.x);
        }

        if (Utils.IsFlagOn(scrollFlags, (uint)ScrollDirectionFlags.Up))
        {
            //m_Transform.position += (m_ForwardDirection * fScrollAmount.z);
			m_RigidBody.AddForce(m_ForwardDirection * fScrollAmount.z);
			//AddForce(m_ForwardDirection * fScrollAmount.z);
        }
        else if (Utils.IsFlagOn(scrollFlags, (uint)ScrollDirectionFlags.Down))
        {
            //m_Transform.position += (-m_ForwardDirection * fScrollAmount.z);
			m_RigidBody.AddForce(-m_ForwardDirection * fScrollAmount.z);
			//AddForce(-m_ForwardDirection * fScrollAmount.z);
        }
		
		//Debug.Log();
        CheckCameraBounds();
    }

    Vector3 CalculateScrollAmount(uint scrollFlags, bool bScrollingWithKeys)
    {
        Vector3 retVal = Vector3.zero;
        
        if (bScrollingWithKeys)
        {
			if (Utils.UsingTouchInput)
			{
				Vector2 amountDragged = AceiPhoneInput.GetAmountDragged() * m_fIPhoneScrollSpeed;
				retVal.x = Mathf.Abs(amountDragged.x);
				retVal.z = Mathf.Abs(amountDragged.y);
			}
			else
			{
				retVal.x = 1;
				retVal.z = 1;
			}
			
            retVal *= m_fScrollSpeed;
        }
        else
        {
			Vector3 mousePos = new Vector3(Mathf.Clamp(Input.mousePosition.x, 0, Screen.width),
            	Mathf.Clamp(Input.mousePosition.y, 0, Screen.height), 0);

            if (Utils.IsFlagOn((uint)scrollFlags, (uint)ScrollDirectionFlags.Left))
            {
                retVal.x = m_fScrollSpeed * (1.0f - (mousePos.x / (m_fScrollArea * Screen.width)));
            }
            else if (Utils.IsFlagOn((uint)scrollFlags, (uint)ScrollDirectionFlags.Right))
            {
                retVal.x = m_fScrollSpeed * (1.0f - (Screen.width - mousePos.x) / (m_fScrollArea * Screen.width));
            }

            if (Utils.IsFlagOn((uint)scrollFlags, (uint)ScrollDirectionFlags.Up))
            {
                retVal.z = m_fScrollSpeed * (1.0f - (Screen.height - mousePos.y) / (m_fScrollArea * Screen.height));
            }
            else if (Utils.IsFlagOn((uint)scrollFlags, (uint)ScrollDirectionFlags.Down))
            {
                retVal.z = m_fScrollSpeed * (1.0f - (mousePos.y / (m_fScrollArea * Screen.height)));
            }

            // take the larger of the 2
            //retVal = Mathf.Max(fLeftRightVal, fUpDownVal);
        }
	
        retVal *= Time.deltaTime;
        return retVal;
    }

    bool IsScrollingKeyDown(ScrollDirectionFlags direction)
    {
        bool retVal = false;
        switch (direction)
        {
            case ScrollDirectionFlags.Left:
                retVal = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
                break;

            case ScrollDirectionFlags.Right:
                retVal = Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
                break;

            case ScrollDirectionFlags.Up:
                retVal = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
                break;

            case ScrollDirectionFlags.Down:
                retVal = Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
                break;
        }

        return retVal;
    }

    void OnScrollZoom(float fZoomSpeed)
    {
		if ((fZoomSpeed > 0 && m_Transform.position.y <= m_ZoomClamper.x)
		    || (fZoomSpeed < 0 && m_Transform.position.y >= m_ZoomClamper.y))
		{		
			return;
		}
		m_Transform.Translate(0, 0, Time.deltaTime * fZoomSpeed);
		CheckCameraBounds();
    }
	
	void OnTouchZoom()
	{
		Touch touch = AceiPhoneInput.GetTouch(0);
		Touch touch2 = AceiPhoneInput.GetTouch(1); 
		
		// Find out how the touches have moved relative to eachother: 
		Vector2 curDist = touch.position - touch2.position; 
		Vector2 prevDist = (touch.position - touch.deltaPosition) - (touch2.position - touch2.deltaPosition);
		
		float delta = curDist.magnitude - prevDist.magnitude; 
		
		if ((delta > 0 && m_Transform.position.y <= m_ZoomClamper.x)
		    || (delta < 0 && m_Transform.position.y >= m_ZoomClamper.y))
		{		
			return;
		} 
		m_Transform.Translate(0, 0, delta * Time.deltaTime * 3); 
		CheckCameraBounds();
	}
	
	/// <summary>
	/// moves the camera to center a 3d position on the screen 
	/// </summary>
	public void CenterPointOnScreen(Vector3 worldPosition)
	{	
		worldPosition.z -= 1.0f;
		Vector3 targetCameraPosition = worldPosition + -transform.forward * transform.position.y;
		targetCameraPosition.y = transform.position.y;
		StartCoroutine(MoveToPosition(targetCameraPosition, 0.5f, true));
	}
	
	public void MoveToPos(Vector3 targetPos, float timeToGetThere, bool disableMovement)
	{
		StartCoroutine(MoveToPosition(targetPos, timeToGetThere, disableMovement));
	}
	
	IEnumerator MoveToPosition(Vector3 targetPos, float timeToGetThere, bool disableMovement)
	{
		targetPos.x = Mathf.Clamp(targetPos.x, m_LevelBoundaries.xMin, m_LevelBoundaries.xMax);
		targetPos.y = Mathf.Clamp(targetPos.y, m_ZoomClamper.x, m_ZoomClamper.y);
		targetPos.z = Mathf.Clamp(targetPos.z, m_LevelBoundaries.yMin, m_LevelBoundaries.yMax);
		
		if (disableMovement)
		{
			AllowMovement(false);
		}
		
		Vector3 startingPos = m_Transform.position;
		float t = 0;
		
		while (t <= timeToGetThere)
		{
			t += Time.deltaTime;
			transform.position = Utils.Interpolate(Utils.InterpolationStyle.Smooth_Step, startingPos,
			                                         targetPos, t /timeToGetThere);
			yield return new WaitForEndOfFrame();
		}
		
		//m_Transform.position = targetPos;
		if (disableMovement)
		{
			AllowMovement(true);
		}
	}
}


