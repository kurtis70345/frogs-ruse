﻿using UnityEngine;
using System;
using System.Collections;

public static class AceExtensions
{
    #region Functions
    public static T[] SubArray<T>(this T[] data, int index, int length)
    {
        if (index < 0 || index >= data.Length || index + length >= data.Length)
        {
            Debug.LogError("SubArray failed because of bad indexing or length");
            return data;
        }

        T[] result = new T[length];
        Array.Copy(data, index, result, 0, length);
        return result;
    }
    #endregion
}
