﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AceServerInfo : MonoBehaviour
{
    #region Constants
    public delegate void ServerStatusReceived(ServerInfo serverInfo);

    public class ServerInfo
    {
        public string Message;
    }
    #endregion

    #region Variables
    public string m_ServerUrl = "";
    #endregion

    #region Functions
    public void GetServerInfo(ServerStatusReceived cb)
    {
        StartCoroutine(GetServerInfoCoroutine(cb, null));   
    }

    public void GetServerInfo(ServerStatusReceived cb, Dictionary<string, string> postArgs)
    {
        StartCoroutine(GetServerInfoCoroutine(cb, postArgs));
    }

    IEnumerator GetServerInfoCoroutine(ServerStatusReceived cb, Dictionary<string, string> postArgs)
    {
        WWW www = null;
        if (postArgs != null)
        {
            WWWForm form = new WWWForm();
            foreach (KeyValuePair<string, string> arg in postArgs)
            {
                form.AddField(arg.Key, arg.Value);
            }
            www = new WWW(m_ServerUrl, form);
        }
        else
        {
            www = new WWW(m_ServerUrl);
        }
       
        yield return www;

        //Debug.Log(www.text);

        if (!string.IsNullOrEmpty(www.text))
        {
#if ALLOW_ACE_DEPENDENCIES
            IDictionary results = (IDictionary)ANMiniJSON.Json.Deserialize(www.text);
            if (cb != null)
            {
                cb(ParseServerInfo(results));
            }
#endif
        }
        else
        {
            Debug.LogError("failed to get server info");
        }
    }

    protected virtual ServerInfo ParseServerInfo(IDictionary results)
    {
        ServerInfo serverInfo = new ServerInfo();
        serverInfo.Message = results["message"].ToString();
        return serverInfo;
    }
    #endregion
    
}
