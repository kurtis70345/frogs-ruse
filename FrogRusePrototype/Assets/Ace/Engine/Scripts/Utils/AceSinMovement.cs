﻿using UnityEngine;
using System.Collections;

public class SinMovement : MonoBehaviour {

	#region Variables
	[SerializeField] float amplitudeX = 10.0f;
	[SerializeField] float amplitudeY = 5.0f;
	[SerializeField] float omegaX = 1.0f;
	[SerializeField] float omegaY = 5.0f;
	float index;
	Vector3 m_OriginalPos;
	#endregion


	#region Functions
	void Start()
	{
		m_OriginalPos = transform.localPosition;
	}

	public void Update()
	{
		index += Time.deltaTime;
		float x = amplitudeX*Mathf.Cos (omegaX*index);
		float y = Mathf.Abs (amplitudeY*Mathf.Sin (omegaY*index));
		transform.localPosition = m_OriginalPos + new Vector3(x,y,0);
	}
	#endregion
}
