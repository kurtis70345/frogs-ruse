﻿using UnityEngine;
using System.Collections;

public class AceTimerTrigger : MonoBehaviour 
{
	#region Variables
	public float m_TimeInterval = 5;
	public GameObject m_Receiver;
	public string m_OnTimerHit = "";
	#endregion
	
	#region Functions
	// Use this for initialization
	void Start()
	{
		StartCoroutine(DoCountDown());
	}
	
	IEnumerator DoCountDown()
	{
		while (true)
		{
            yield return new WaitForSeconds(m_TimeInterval);

            if (m_Receiver != null && !string.IsNullOrEmpty(m_OnTimerHit))
            {
                m_Receiver.SendMessage(m_OnTimerHit);
            }
		}
	}
	#endregion
}
