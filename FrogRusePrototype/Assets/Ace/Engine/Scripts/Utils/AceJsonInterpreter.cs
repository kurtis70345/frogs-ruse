﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

static public class AceJsonInterpreter
{
	#region Functions
	public static string GetString(string json, string key)
	{
		JSONNode j = JSON.Parse(json);
		return j[key];
	}

	public static AcePlayerAccount ParsePlayerAccount(string json)
	{
		JSONNode node = JSONNode.Parse(json);
		AcePlayerAccount playerAccount = new AcePlayerAccount(node["username"], node["email"], node["password"]);
		
		Debug.Log(playerAccount.Username);
		return playerAccount;
	}

	public static AcePlayerAccount ParsePlayerAccountWithLevel(string json)
	{
		JSONNode node = JSONNode.Parse(json);
		AcePlayerAccount playerAccount = new AcePlayerAccount(node["username"], node["email"],
		                                                      node["password"], (uint)node["xp"].AsInt,
		                                                      (uint)node["currLevel"].AsInt,
		                                                      (float)node["normalizedProgressToNextLevel"].AsFloat);
		
		Debug.Log(playerAccount.Username);
		return playerAccount;
	}

	public static void ParseErrors(string json, ref string publicError, ref string internalError)
	{
		JSONNode node = JSON.Parse(json);
		publicError = node["error"];
		internalError = node["internalError"];
	}

	public static AceAchievement[] ParseAchievements(string json)
	{
		JSONNode node = JSON.Parse(json);
		JSONNode achievementsNode = node["achievements"];
		AceAchievement[] achievements = new AceAchievement[achievementsNode.Count];
		for (int i = 0; i < achievementsNode.Count; i++)
		{
			achievements[i] = ParseAchievement(achievementsNode[i]);
		}
		return achievements;
	}

	public static AceAchievement ParseAchievement(string json)
	{
		JSONNode node = JSON.Parse(json);
		return ParseAchievement(node["achievement"]);
	}

	public static AceAchievement ParseAchievement(JSONNode achievementNode)
	{
		return new AceAchievement(AceTranslator.Translate(achievementNode["name"]), achievementNode["internalName"],
		                          AceTranslator.Translate(achievementNode["description"]), (uint)achievementNode["points"].AsInt,
		                          (uint)achievementNode["stepsCompleted"].AsInt, (uint)achievementNode["steps"].AsInt);
	}

	public static AceLeaderboard ParseLeaderboard(string json)
	{
		JSONNode node = JSON.Parse(json);
		return ParseLeaderboard(node["leaderboard"]);
	}

	public static AceLeaderboard ParseLeaderboard(JSONNode leaderboardNode)
	{
		return new AceLeaderboard(AceTranslator.Translate(leaderboardNode["name"]), leaderboardNode["internalName"],
		                          leaderboardNode["bestPersonalScore"].AsFloat, leaderboardNode["bestWorldScore"].AsFloat);
	}

	public static AceXPResponse ParseXPResponse(string json)
	{
		JSONNode node = JSON.Parse(json);

		return ParseXPResponse(node["responseXP"]);
	}

	public static AceXPResponse ParseXPResponse(JSONNode node)
	{
		return new AceXPResponse((uint)node["prevXP"].AsInt, (uint)node["currXP"].AsInt, 
		                         (uint)node["addedXP"].AsInt, (uint)node["currLevel"].AsInt, 
		                         node["normalizedProgressToNextLevel"].AsFloat);
	}

	public static AceGameLevelResponse ParseGameLevelResponse(string json)
	{
		JSONNode node = JSON.Parse(json);
		return ParseGameLevelResponse(node["responseLevel"]);
	}

	public static AceGameLevelResponse ParseGameLevelResponse(JSONNode node)
	{
		return new AceGameLevelResponse(node["gameName"], node["internalGameName"],
		                                (uint)node["level"].AsInt, (uint)node["numLevels"].AsInt);
	}
	#endregion
}
