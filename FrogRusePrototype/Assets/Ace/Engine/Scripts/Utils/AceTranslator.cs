﻿using UnityEngine;
using System.Collections;

public class AceTranslator
{
    #region Functions
    public static string Translate(string key)
    {
        return AceLocalization.instance.GetWord(key);
    }

    public static string Translate(string key, params object[] values)
    {
		return string.Format(AceLocalization.instance.GetWord(key), values);
    }
    #endregion
}
