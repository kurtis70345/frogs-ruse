using UnityEngine;
using System.Collections;

public class AceRotator : MonoBehaviour
{
    #region Variables
    public Vector3 m_Rotation;
    Transform m_Transform;
    #endregion

    #region Functions
    // Use this for initialization
	void Start () 
    {
        m_Transform = transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_Transform.Rotate(m_Rotation * Time.deltaTime);
    }
    #endregion
}
