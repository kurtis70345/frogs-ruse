﻿using UnityEngine;
using System.Collections;

public class AceDragItem : MonoBehaviour
{
    #region Constants
    enum State
    {
        Disabled,
        Waiting_For_Drag,
        Dragging,
    }
    #endregion

    #region Variables
    public Camera m_DragCam;
    public LayerMask m_DragLayer;
    public LayerMask m_DropLayer;
    public int m_MouseDragButton;
    public string m_DropTargetFunction = "OnDrop";
    public bool m_RequireDropLocation = true;
    State m_DragState = State.Waiting_For_Drag;
    Vector3 m_OriginalDragPos;
	Vector3 m_InitalPos;
    #endregion

	#region Properties
	bool IsDraggingEnabled
	{
		get { return m_DragState != State.Disabled; }
	}
	#endregion

    #region Functions
    // Use this for initialization
	void Awake () 
    {
        if (m_DragCam == null)
        {
            m_DragCam = Camera.main;
        }
        
		m_InitalPos = transform.position;
	}
	
	// Update is called once per frame
	void Update()
    {
        switch (m_DragState)
        {
            case State.Waiting_For_Drag:
                WaitingForDragUpdate();
                break;

            case State.Dragging:
                DragUpdate();
                break;
        }
    }

    void WaitingForDragUpdate()
    {
        if (Input.GetMouseButtonDown(m_MouseDragButton))
        {
            RaycastHit hit;
            //Vector3 hitPos = Vector3.zero;
            //Debug.Log(gameObject.layer);
            if (GetDragPosition(1 << gameObject.layer, out hit) && IsThisObjectHitByRay())
            {
				//Debug.Log(name);
                m_OriginalDragPos = transform.position;
                transform.position = hit.point;
                SetDragState(State.Dragging);
            }
        }
    }

    void DragUpdate()
    {
        RaycastHit hit;
        if (GetDragPosition(m_DragLayer, out hit))
        {
            transform.position = hit.point;
        }

        // check if they stopped dragging
        if (Input.GetMouseButtonUp(m_MouseDragButton))
        {
			SetDragState(State.Waiting_For_Drag);

            Ray r = m_DragCam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(r, out hit, Mathf.Infinity, m_DropLayer.value))
            {
                transform.position = hit.transform.position;
                if (!string.IsNullOrEmpty(m_DropTargetFunction))
                {
                    hit.transform.SendMessage(m_DropTargetFunction, gameObject);
                }
            }
            else if (m_RequireDropLocation)
            {
                transform.position = m_OriginalDragPos;
            }

            
        }
    }

    bool GetDragPosition(LayerMask targetLayer, out RaycastHit hit)
    {
        Ray r = m_DragCam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(r, out hit, Mathf.Infinity, 1 << targetLayer.value))
        {
            //hitPos = hit.point;
            return true;
        }
        else
        {
            //hitPos = Vector3.zero;
            return false;
        }
    }

    bool IsThisObjectHitByRay()
    {
        Ray r = m_DragCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(r, Mathf.Infinity, 1 << gameObject.layer);
        for (int i = 0; i < hits.Length; i++)
        {
            //Debug.Log(hits[i].transform.name);
			if (hits[i].transform.gameObject == gameObject && IsDraggingEnabled)
            {
                return true;
            }
        }

        return false;
    }

    void SetDragState(State state)
    {
        m_DragState = state;
    }

    public void EnableDragging(bool enable)
    {
        SetDragState(enable ? State.Waiting_For_Drag : State.Disabled);
    }

	public void ResetPosition()
	{
		transform.position = m_InitalPos;
	}
    #endregion
}
