using UnityEngine;
using System.Collections;

public class AceMessageQueue
{
    // ------> Constants
    const int MaxMessagesDisplayed = 1;

    public struct Message
    {
        public AceLabel m_Text;
        public float m_fDisplayTimer;

        public bool InUse
        {
            get { return m_Text.gameObject.activeSelf; }
            set { m_Text.gameObject.SetActive(value); }
        }

        public Message(AceLabel text, float timer)
        {
            m_Text = text;
            m_fDisplayTimer = timer;
            InUse = false;
        }
    }

    // ------> Variables    
    float m_fDisplayTimeLength = 1;
    AceLabel m_TextStylePrefab;
    Message[] m_CurrentMessages = new Message[MaxMessagesDisplayed];
	

    public AceMessageQueue(float fDisplayTimeLength, GameObject textStylePrefab)
    {
        m_fDisplayTimeLength = fDisplayTimeLength;
        m_TextStylePrefab = (AceLabel)textStylePrefab.GetComponent(typeof(AceLabel));
        if (m_TextStylePrefab == null)
        {
            Debug.LogError("textStylePrefab in AceMessageQueue doesn't have a SpriteText component");
        }

        // the position where the text will first show up
        //m_StartingPositionRect = new Rect((float)Screen.width * 0.5f, (float)Screen.height * 0.5f, 400, 40);

        for (int i = 0; i < MaxMessagesDisplayed; i++)
		{
            AceLabel t = (AceLabel)GameObject.Instantiate(m_TextStylePrefab);
			t.transform.parent = m_TextStylePrefab.transform.parent;
			t.transform.localPosition = m_TextStylePrefab.transform.localPosition;
			t.transform.localScale = m_TextStylePrefab.transform.localScale;
            m_CurrentMessages[i] = new Message(t, 0);
        }
    }

    public void AddMessage(string text, bool clearQueue)
    {
		if (clearQueue)
		{
			ClearQueue();	
		}
		
        int i = 0;
        for (i = 0; i < MaxMessagesDisplayed; i++)
        {
            if (!m_CurrentMessages[i].InUse)
            {
				ActivateText(i, text, Color.white, m_fDisplayTimeLength);
                break;
            }
        }
		
		if (MaxMessagesDisplayed == i)
		{
			// there were no open spots	
		}
    
    }
	
	public void ClearQueue()
	{
		for (int i = 0; i < MaxMessagesDisplayed; i++)
		{
			m_CurrentMessages[i].InUse = false;
			m_CurrentMessages[i].m_Text.Hide();
		}
	}
	
	public void ActivateText(int index, string text, Color col, float fadeTime)
	{
		if (index < 0 || index >= MaxMessagesDisplayed)
		{
			Debug.LogError("ActivateText fail, bad index: " + index);
			return;
		}
		/*
		m_CurrentMessages[index].m_Text.RemoveAllEffects();
		m_CurrentMessages[index].m_Text.enabled = true;
		m_CurrentMessages[index].m_Text.Show();	
		m_CurrentMessages[index].m_Text.SetColor(col);
		m_CurrentMessages[index].m_Text.Text = text;
        m_CurrentMessages[index].m_Text.Alpha = 1;
        m_CurrentMessages[index].InUse = true;
        m_CurrentMessages[index].m_Text.AddEffect(new MenuItemEffect_Alpha(m_CurrentMessages[index].m_Text,
                   fadeTime, MessageExpiredCallBack, 0));
                   */
	}

    /*void MessageExpiredCallBack(AceMenuItem effectItem, AceMenuItemEffect effect)
    {
        effectItem.enabled = false;
		
		for (int i = 0; i < MaxMessagesDisplayed; i++)
		{
			if (m_CurrentMessages[i].m_Text == effectItem)
			{
				m_CurrentMessages[i].InUse = false;
				m_CurrentMessages[i].m_Text.Hide();
			}
		}	
    }*/
	
	public void HideText(bool hide)
	{
		for (int i = 0; i < MaxMessagesDisplayed; i++)
		{
			if (m_CurrentMessages[i].InUse)
			{
				m_CurrentMessages[i].m_Text.Hide(hide);
			}
		}
	}
}
