using UnityEngine;
using System.Collections;

static public class AceSound
{
    public const string MusicKey = "MusicVolume";
    public const string SfxKey = "SfxVolume";

    static float m_SfxVolume = 1.0f;
    static float m_MusicVolume = 1.0f;

    static public float SfxVolume
    {
        get { return m_SfxVolume; }
        set { m_SfxVolume = value; } 
    }

    static public float MusicVolume
    {
        get { return m_MusicVolume; }
        set { m_MusicVolume = value; } 
    }
	
	static public void InitVolumes()
	{
		MusicVolume = PlayerPrefs.GetFloat(MusicKey, 1.0f);
		SfxVolume = PlayerPrefs.GetFloat(SfxKey, 1.0f);
	}
	
	static public void SetMusicOn(bool on, AudioSource musicSource)
	{
		PlayerPrefs.SetInt(MusicKey, on ? 1 : 0);
		m_MusicVolume = on ? 1 : 0;
		if (on)
		{
			musicSource.Play();
		}
		else
		{
			musicSource.Pause();
		}
	}
	
	static public void SetSfxOn(bool on)
	{
		PlayerPrefs.SetInt(SfxKey, on ? 1 : 0);
		m_SfxVolume = on ? 1 : 0;
	}

    static float GetProperVolume(bool bIsSfx)
    {
        return bIsSfx ? m_SfxVolume : m_MusicVolume;
    }

    static public bool IsVolumeOff(bool bIsSfx)
    {
        if (bIsSfx)
        {
            return m_SfxVolume == 0;
        }
        else
        {
            return m_MusicVolume == 0;
        }
    }

    static public void Play(AudioSource source, AudioClip clip, bool bIsSfx)
    {
        if (IsVolumeOff(bIsSfx))
        {
            return;
        }

        if (source != null && clip != null)
        {
            source.volume = GetProperVolume(bIsSfx);
            source.Play();
        }
    }

    static public void Pause(AudioSource source)
    {
        if (source != null)
        {
            source.Pause();
        }      
    }

    static public void PlayOneShot(AudioSource source, AudioClip clip, bool bIsSfx)
    {
        if (IsVolumeOff(bIsSfx))
        {
            return;
        }

        if (source != null && clip != null)
        {	
            source.PlayOneShot(clip, source.volume);
        }
    }

    static public void ToggleMusic(AudioSource musicSource)
    {
        float newValue = PlayerPrefs.GetFloat(MusicKey, 1.0f) == 1.0f ? 0 : 1.0f;
        PlayerPrefs.SetFloat(MusicKey, newValue);
        MusicVolume = newValue;

        if (newValue == 0)
        {
            musicSource.Pause();
        }
        else
        {
            musicSource.Play();
        }
    }

    static public void ToggleSfx()
    {
        float newValue = PlayerPrefs.GetFloat(SfxKey, 1.0f) == 1.0f ? 0 : 1.0f;
        PlayerPrefs.SetFloat(SfxKey, newValue);
        SfxVolume = newValue;
    }
}
