using UnityEngine;
using System.Collections;

public class AceWayPointPair// : GameEntity 
{
    // ------> Variables
    public int ID = -1;
	public Vector3 StartPoint;
	public Vector3 EndPoint;
	
	public AceWayPointPair(int id, Vector3 startPoint, Vector3 endPoint) 
	{
		ID = id;
		StartPoint = startPoint;
		EndPoint = endPoint;
	}
}