﻿using UnityEngine;
using System.Collections;

public class AceRotator_Lerp : MonoBehaviour 
{
	#region Constants
	public delegate void OnCompleted();
	#endregion

	#region Functions
	public void Rotate(Vector3 eulerAngles, float completionTime, OnCompleted onTurnCompletedCB)
	{
		StartCoroutine(RotateObject(eulerAngles, completionTime, onTurnCompletedCB));
	}

	IEnumerator RotateObject(Vector3 target, float completionTime, OnCompleted onTurnCompletedCB)
	{
		float timePassed = 0;
		Vector3 startingRot = transform.localRotation.eulerAngles;
		while (timePassed < completionTime)
		{
			transform.localRotation = Quaternion.Euler(Vector3.Lerp(startingRot, target, timePassed / completionTime));
			timePassed += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		
		transform.localRotation = Quaternion.Euler(target);
		
		if (onTurnCompletedCB != null)
		{
			onTurnCompletedCB();
		}
	}
	#endregion
}
