﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public class AceProfanityFilter : MonoBehaviour
{
    #region Variables
    public TextAsset m_ProfanityList;
    string m_ProfaneWords;
    #endregion

    #region Functions
    // Use this for initialization
	void Start()
    {
        string[] words = m_ProfanityList.text.Split('\n');
        StringBuilder builder = new StringBuilder(@"^.*\b(");
        for (int i = 0; i < words.Length - 1; i++)
        {
            if (!string.IsNullOrEmpty(words[i]))
            {
                builder.Append(words[i] + "|");
            }
        }

        builder.Append(words[words.Length - 1]);
        builder.Append(@")\b.*$");

        m_ProfaneWords = builder.ToString();

        // release memory
        m_ProfanityList = null;
	}

    public string FilterWords(string inputWords, string replacment)
    {
        Regex wordFilter = new Regex(m_ProfaneWords);
        return wordFilter.Replace(inputWords, replacment);
    }

    public bool HasBadWords(string inputWords)
    {
        //Debug.Log(m_ProfaneWords);
        //Debug.Log(inputWords);
        Regex wordFilter = new Regex(m_ProfaneWords, RegexOptions.IgnoreCase);
        return wordFilter.IsMatch(inputWords);
    }
    #endregion
}
