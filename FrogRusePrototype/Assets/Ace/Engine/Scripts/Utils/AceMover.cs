﻿using UnityEngine;
using System.Collections;

public class AceMover : MonoBehaviour
{
    #region Variables
    public Vector3 m_Force;
    #endregion

    #region Functions
	// Update is called once per frame
	void Update()
    {
        transform.localPosition += (m_Force * Time.deltaTime);
    }
    #endregion
}
