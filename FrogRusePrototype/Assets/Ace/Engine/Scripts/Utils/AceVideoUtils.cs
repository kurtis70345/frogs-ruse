using UnityEngine;
using System.Collections;

public class AceVideoUtils : MonoBehaviour
{
    #region Variables
    public AceToggle m_RecordStart;
    public AceToggle m_RecordPause;
    public AceButton m_ShowUIButton;
    AceVideoSharer m_VideoSharer;
	bool m_IsSupported;
	bool m_IsRecordingAvailable;
    #endregion

    #region Properties
    public bool IsVideoSharingSupported
    {
		get { return m_IsSupported; }
    }

	public bool IsRecordingAvailable
	{
		get { return m_IsRecordingAvailable; }
	}
    #endregion

    #region Functions
    void Start()
    {
        //if (m_VideoSharer == null)
        {
            m_VideoSharer = FindObjectOfType<AceVideoSharer>();
        }

        if (m_VideoSharer == null || !m_VideoSharer.IsRecordingSupported())
        {
            m_IsSupported = false;
            m_RecordStart.Hide();
            m_RecordPause.Hide();

            if (m_ShowUIButton != null)
            {
                m_ShowUIButton.Hide();
            }
        }
        else
        {
            m_IsSupported = true;
        }
    }

    public void OnRecordStart(bool start)
    {
        //Debug.Log("OnRecordStart");
        m_RecordPause.Hide(!start);
        if (IsVideoSharingSupported)
        {
            if (start)
            {
				m_IsRecordingAvailable = true;
                m_VideoSharer.StartRecording();
            }
            else
            {
                m_VideoSharer.StopRecording();
            }
        }
    }

    public void OnRecordPause(bool pause)
    {
        //Debug.Log("OnRecordPause");
        if (IsVideoSharingSupported)
        {
            if (pause)
            {
				m_VideoSharer.ResumeRecording();
            }
            else
            {
				m_VideoSharer.PauseRecording();
            }
        }
    }

    //void OnLevelWasLoaded(int level)
    //{
    //    m_VideoSharer = FindObjectOfType<AceVideoSharer>();
    //    if (m_VideoSharer != null)
    //    {
    //        m_VideoSharer.StopRecording();
    //    }
    //}

	public void SubmitVideo()
	{
		m_VideoSharer.ShowSharingUI();
        //Everyplay.SharedInstance.ShowSharingModal();
	}

    public void PlayLastRecording()
    {
        m_VideoSharer.PlayLastRecording();
    }
    #endregion
}
