using UnityEngine;
using System.Collections;

public class Pair<S, T> 
{
	S m_First;
	T m_Second;
	
	public S First
	{
		get { return m_First; }	
	}
	
	public T Second
	{
		get { return m_Second; } 	
	}
	
	public Pair(S s, T t)
	{
		m_First = s;
		m_Second = t;
	}
	
}
