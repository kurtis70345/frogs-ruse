﻿using UnityEngine;
using System.Collections;

public class AceDoubleClick : MonoBehaviour 
{
	#region Variables
	//start doubleClickStart as a float so no ambiguity to the compiler
	float doubleClickStart = -1.0f;
	bool disableClicks = false;
	#endregion
	
	#region Functions
	void Update()
	{
	    if (disableClicks)
	        return;		
		
		if (Input.GetMouseButtonDown(0))
		{
			//make sure doubleClickStart isn't negative, that'll break things
		    if (doubleClickStart > 0 && (Time.time - doubleClickStart) < 0.4)
		    {
		        DoDoubleClick();
		        doubleClickStart = -1;
		            lockClicks();
		    }
		    else
		    {
		        doubleClickStart = Time.time;
		    }
		} 
	}
	 
	IEnumerator lockClicks()
	{
	    disableClicks = true;
	    yield return new WaitForSeconds(0.4f);
	    disableClicks = false;
	}
	 
	void DoDoubleClick()
	{   
   		SendMessage("OnDoubleClick");   
	}
	#endregion
}
