using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AceLabel))]
public class AceLocalize : MonoBehaviour 
{
	#region Variables
	[SerializeField] string m_TextKey = "";
	#endregion

	#region Functions
	void Start()
	{
		Localize();
	}

	public void Localize()
	{
		if (!string.IsNullOrEmpty(m_TextKey))
		{
			GetComponent<AceLabel>().Text = AceTranslator.Translate(m_TextKey);
		}
	}
	#endregion
}
