﻿using UnityEngine;
using System.Collections;

public class AceDropArea : MonoBehaviour
{
    #region Variables
    public GameObject m_DropReceiver;
    public string m_DropReceiverFunction = "OnDrop";
    #endregion

    #region Functions
    public void OnDrop(GameObject droppedObj)
    {
        if (m_DropReceiver != null && !string.IsNullOrEmpty(m_DropReceiverFunction))
        {
            m_DropReceiver.SendMessage(m_DropReceiverFunction, droppedObj);
        }
    }
    #endregion
}
