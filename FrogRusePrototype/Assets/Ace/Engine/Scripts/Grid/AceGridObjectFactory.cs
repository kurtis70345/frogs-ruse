using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AceGridObjectFactory : MonoBehaviour 
{
	#region Variables
	[SerializeField] AceGrid m_Grid;
	[SerializeField] AceGridObject[] m_GridObjects;
	//[SerializeField] GameObject remove;
	#endregion

	#region Functions
	// Use this for initialization
	void Awake () 
	{
		for (int i = 0; i < m_GridObjects.Length; i++)
		{
			m_GridObjects[i].SetPrefabId(i.ToString());
		}
	}

	AceGridObject FindGridObject(string id)
	{
		AceGridObject go = Array.Find<AceGridObject>(m_GridObjects, g => g.Id == id);
		if (go == null)
		{
			Debug.LogError(name + " failed to find Gridobject with id: " + id);
		}
		return go;
	}

	public AceGridObject CreateGridObject(string id)
	{
		return CreateGridObject(id, Vector3.zero, Quaternion.identity);
	}

	public AceGridObject CreateGridObject(string id, Vector3 position, Quaternion rotation)
	{
		AceGridObject newGO = Instantiate(FindGridObject(id), position, rotation) as AceGridObject;
		newGO.SetPrefabId(id);
		return newGO;
	}
	
	public AceGridObject CreateGridObject(AceGridObjectData data)
	{
		//Vector3 pos = m_Grid.GetPosFromRowCol(data.Row, data.Col);
		Vector3 pos = m_Grid.GetSnappedPos(data.Row, data.Col, data.Width, data.Depth);

		//Debug.Log("data.Row: " + data.Row + " data.Col: " + data.Col + " pos: " + pos);
		AceGridObject gridObject = CreateGridObject(data.GridObjectId);
		gridObject.SetGridObjectData(data);
		gridObject.transform.position = pos;

		// TODO: need to do this before we snap to the grid
		gridObject.transform.Rotate(0, data.RotationY, 0);
		return gridObject;
	}

	public IEnumerable<AceGridObject> GetGridObjects() 
	{
		return m_GridObjects;
	}
	#endregion
}
