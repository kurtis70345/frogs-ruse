﻿using UnityEngine;
using System.Collections;

[System.Serializable()]
public class AceGridObjectData
{
	#region Variables
	// id used for linking with a prefab
	[SerializeField]
	string m_GridObjectId = "";

	// position of the pivot on the grid
	[SerializeField]
	int m_Row;

	[SerializeField]
	int m_Col;
	
	// number of grid cells occupied
	[SerializeField]
	int m_Width;

	[SerializeField]
	int m_Depth;

	[SerializeField]
	float m_RotationY;
	#endregion
	
	#region Properties
	public string GridObjectId { get { return m_GridObjectId; } }
	public int Row { get { return m_Row; } }
	public int Col { get { return m_Col; } }
	public int Width { get { return m_Width; } }
	public int Depth { get { return m_Depth; } }
	public float RotationY { get { return m_RotationY ; } }
	#endregion

	#region Functions
	public AceGridObjectData() { }

	public AceGridObjectData(string gridObjectId, int row, int col, int width, int depth, float rotY)
	{
		m_GridObjectId = gridObjectId;
		m_Row = row;
		m_Col = col;
		m_Width = width;
		m_Depth = depth;
		m_RotationY = rotY;

	}
	#endregion
}