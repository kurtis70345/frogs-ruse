﻿using UnityEngine;
using System.Collections;

public class AceGrid : MonoBehaviour 
{
	#region Variables
	[SerializeField]
	BoxCollider m_GridBoundary;

	[SerializeField]
	float m_GridCellWidth = 1;
	float m_GridCellHeight = 1;

	int m_NumRows;
	int m_NumCols;
	#endregion

	#region Properties
	float GridCellArea { get { return m_GridCellWidth * m_GridCellHeight; } }

	Vector3 TerrainSize { get { return m_GridBoundary.bounds.size; } }
	Vector3 TerrainMinPos { get { return m_GridBoundary.bounds.min; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Start ()
	{
		if (m_GridBoundary == null)
		{
			Debug.LogError("m_GridBoundary is null");
			m_GridBoundary = GetComponent<BoxCollider>();
		}

		m_NumCols = (int)(m_GridBoundary.bounds.size.x / m_GridCellWidth);
		m_NumRows = (int)(m_GridBoundary.bounds.size.z / m_GridCellHeight);

		Debug.Log("Num Rows: " + m_NumRows + " m_NumCols: " + m_NumCols);
	}
	
	public void GetRowColFromPos(Vector3 pos, ref int nRow, ref int nCol)
	{
		Vector3 transformedPos = pos - TerrainMinPos;
		nRow = (int)(transformedPos.z / m_GridCellHeight);
		nCol = (int)(transformedPos.x / m_GridCellWidth);
	}

	public Vector3 GetPosFromRowCol(int nRow, int nCol)
	{
		Vector3 retVal = TerrainMinPos;
		retVal.x += nCol * m_GridCellWidth;
		retVal.z += nRow * m_GridCellHeight;
		return retVal;
	}

	public Vector3 GetSnappedPos(Vector3 pos)
	{
		return GetSnappedPos(pos, 1, 1);
	}

	public Vector3 GetSnappedPos(Vector3 pos, int width, int depth)
	{
		int row = 0, col = 0;
		GetRowColFromPos(pos, ref row, ref col);
		return GetSnappedPos(row, col, width, depth);
	}

	public Vector3 GetSnappedPos(int row, int col, int width, int depth)
	{
		Vector3 snappedPos = GetPosFromRowCol(row, col);
		snappedPos.x += (float)width * m_GridCellWidth * 0.5f;
		snappedPos.z += (float)depth * m_GridCellHeight * 0.5f;
		return snappedPos;
	}

	public void GetWidthDepthFromSize(Collider collider, ref int width, ref int depth)
	{
		width = (int)(collider.bounds.size.x / m_GridCellWidth);
		depth = (int)(collider.bounds.size.z / m_GridCellHeight);
	}

	#endregion
}
