using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceGridObjectPlacer : MonoBehaviour 
{
	#region Variables
	[SerializeField] AceGrid m_Grid;
	[SerializeField] Camera m_RayCastCam;
	[SerializeField] AceGridObjectDataBuilder m_GridDataBuilder;
	[SerializeField] AceGridObjectFactory m_GridObjectFactory;
	[SerializeField] AceGridObject m_TrackedObject;

	List<AceGridObject> m_PlacedGridObjects = new List<AceGridObject>();

	bool m_TrackMousePos = true;
	int m_Width = 0;
	int m_Depth = 0;
	#endregion

	#region Properties
	public bool TrackMousePos
	{
		get { return m_TrackMousePos; }
		set 
		{ 
			m_TrackMousePos = value;
			if (m_TrackedObject != null)
			{
				m_TrackedObject.gameObject.SetActive(value);
			}
		}
	}

	public IEnumerable<AceGridObject> PlacedGridObjects { get { return m_PlacedGridObjects; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		SetTrackedObject(m_TrackedObject);
	}

	public void SetTrackedObject(AceGridObject gridObject)
	{
		if (m_TrackedObject != null)
		{
			// delete the previous one
			Destroy(m_TrackedObject.gameObject);
		}

		m_TrackedObject = gridObject;
		if (m_TrackedObject != null)
		{
			m_Grid.GetWidthDepthFromSize(m_TrackedObject.CollisionVolume, ref m_Width, ref m_Depth);
		}
	}

	public AceGridObject PlaceGridObject()
	{
		Vector3 mousePos = Vector3.zero;
		AceGridObject placedObject = null;
		if (GetGridMousePosition(out mousePos))
		{
			if (m_TrackedObject == null)
			{
				Debug.LogError("no grid object is being tracked");
				return null;
			}

			placedObject = m_GridObjectFactory.CreateGridObject(m_TrackedObject.Id,
			                                                    m_TrackedObject.transform.position,
			                                                    m_TrackedObject.transform.rotation);

			placedObject.name = placedObject.DisplayName;

			// TODO: remove any other grid object that is currently placed here
			placedObject.SetGridObjectData(m_GridDataBuilder.CreateGridObjectData(placedObject));
			RemoveIntersectingGridObjects(placedObject);
			m_PlacedGridObjects.Add(placedObject);
		}
		else
		{
			// TODO: play error sound
		}

		return placedObject;
	}

	void RemoveIntersectingGridObjects(AceGridObject testObject)
	{
		for (int i = 0; i < m_PlacedGridObjects.Count; i++)
		{
			//if (Utils.IsIntersecting(testObject.CollisionVolume, m_PlacedGridObjects[i].CollisionVolume))
			if (AceGridObject.IsIntersecting(testObject, m_PlacedGridObjects[i]))
			{
				Destroy(m_PlacedGridObjects[i].gameObject);
				m_PlacedGridObjects.RemoveAt(i--);
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (TrackMousePos && m_TrackedObject != null)
		{
			DoMouseTracking();
		}
	}

	void DoMouseTracking()
	{
		Vector3 mouseGridPos;
		if (GetGridMousePosition(out mouseGridPos))
		{
			SnapGridObject(mouseGridPos);
		}
		else
		{

		}
	}

	void SnapGridObject(Vector3 mouseGridPos)
	{
		m_TrackedObject.transform.position = m_Grid.GetSnappedPos(mouseGridPos, m_Width, m_Depth);
	}

	public void SnapGridObject(AceGridObjectData go)
	{
		m_Grid.GetSnappedPos(go.Row, go.Col, go.Width, go.Depth);
	}

	/// <summary>
	/// Returns false if the mouse position isn't on the grid. Returns true and sets the position if the mouse hovers over the grid
	/// </summary>
	/// <returns><c>true</c>, if grid mouse position was gotten, <c>false</c> otherwise.</returns>
	/// <param name="mousePos">Mouse position.</param>
	public bool GetGridMousePosition(out Vector3 mousePos)
	{
		Ray ray = m_RayCastCam.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		bool mouseIsOverGrid = false;
		if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << m_Grid.gameObject.layer))
		{
			// ray hit the grid
			mousePos = hitInfo.point;
			mouseIsOverGrid = true;
		}
		else
		{
			// the ray isn't hitting the grid
			mousePos = Vector3.zero;	
		}

		return mouseIsOverGrid;
	}

	public AceGridObject SelectGridObject()
	{
		Ray ray = m_RayCastCam.ScreenPointToRay(Input.mousePosition);
		return SelectGridObject(ray);
	}

	/// <summary>
	/// Returns the grid object intersected by the ray. Null is no grid object is intersected
	/// </summary>
	/// <returns>The grid object.</returns>
	/// <param name="ray">Ray.</param>
	public AceGridObject SelectGridObject(Ray ray)
	{
		AceGridObject selectedGridObject = null;
		RaycastHit hitInfo;
		if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
		{
			selectedGridObject = hitInfo.collider.GetComponent<AceGridObject>();
		}

		return selectedGridObject;
	}

	public void DeletePlacedObjects()
	{
		for (int i = 0; i < m_PlacedGridObjects.Count; i++)
		{
			Destroy(m_PlacedGridObjects[i].gameObject);
		}
		m_PlacedGridObjects.Clear();
	}
	#endregion
}
