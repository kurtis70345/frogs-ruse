using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AceGridObjectDataBuilder : MonoBehaviour {

	#region Variables
	[SerializeField] 
	AceGrid m_Grid;
	#endregion

	#region Functions
	// Use this for initialization
	void Start () {
	
	}

	public AceGridObjectData[] CreateGridObjectDataFromScene()
	{
		AceGridObject[] gridObjects = FindObjectsOfType<AceGridObject>();
		if (gridObjects == null || gridObjects.Length == 0)
		{
			Debug.Log("there are no grid objects in the scene");
			return null;
		}

		AceGridObjectData[] gridObjectData = new AceGridObjectData[gridObjects.Length];
		for (int i = 0; i < gridObjects.Length; i++)
		{
			gridObjectData[i] = CreateGridObjectData(gridObjects[i]);
		}

		return gridObjectData;
	}

	public AceGridObjectData[] CreateGridObjectData(IEnumerable<AceGridObject> gridObjects)
	{
		int numGridObjects = gridObjects.Count();

		/*foreach (AceGridObject go in gridObjects)
		{
			numGridObjects += 1;
		}*/

		AceGridObjectData[] gridObjectData = new AceGridObjectData[numGridObjects];
		int counter = 0;
		foreach (AceGridObject gridObject in gridObjects)
		{
			gridObjectData[counter] = CreateGridObjectData(gridObject);
			counter += 1;
		}

		return gridObjectData;
	}

	public AceGridObjectData CreateGridObjectData(AceGridObject gridObject)
	{
		int row = 0, col = 0;
		m_Grid.GetRowColFromPos(gridObject.transform.position, ref row, ref col);

		int width = 1, depth = 1;
		m_Grid.GetWidthDepthFromSize(gridObject.CollisionVolume, ref width, ref depth);
		AceGridObjectData gridObjectData = new AceGridObjectData(gridObject.Id, row, col,
		                                                   width, depth, gridObject.transform.eulerAngles.y);
		return gridObjectData;
	}
	#endregion
}
