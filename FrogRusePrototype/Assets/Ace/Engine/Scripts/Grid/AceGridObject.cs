﻿using UnityEngine;
using System.Collections;

public class AceGridObject : MonoBehaviour 
{
	#region Constants

	#endregion

	#region Variables
	[SerializeField] string m_DisplayName = "unnamed grid object";
	AceGridObjectData m_GridObjectData;
	string m_GridObjectId = "";
	#endregion

	#region Properties
	public BoxCollider CollisionVolume { get { return GetComponent<BoxCollider>(); } }
	public int Row { get { return m_GridObjectData.Row; } }
	public int Col { get { return m_GridObjectData.Col; } }
	public int Width { get { return m_GridObjectData.Width; } }
	public int Depth { get { return m_GridObjectData.Depth; } } 
	public string Id { get { return m_GridObjectId; } }
	public string DisplayName { get { return m_DisplayName; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Start() 
	{
	
	}

	public void SetGridObjectData(AceGridObjectData gridObjectData)
	{
		m_GridObjectData = gridObjectData;
	}

	public void SetPrefabId(string prefabId)
	{
		m_GridObjectId = prefabId;
	}

	public static bool IsIntersecting(AceGridObject go1, AceGridObject go2)
	{
		Rect r1 = new Rect(go1.Col , go1.Row, go1.Width, go1.Depth);
		Rect r2 = new Rect(go2.Col, go2.Row, go2.Width, go2.Depth);
		Debug.Log("r1: " + r1 + " r2: " + r2);

		return Utils.Intersect(r1, r2);
		//return r1.Contains(r2.min) || r1.Contains(r2.max)
		//	|| r2.Contains(r1.min) || r2.Contains(r1.max);
	}
	#endregion
}
