﻿using UnityEngine;
using System.Collections;

public class AceXPResponse
{
	#region Variables
	readonly uint m_PrevXP;
	readonly uint m_CurrXP;
	readonly uint m_AddedXP;
	readonly uint m_CurrLevel;
	readonly float m_NormalizedProgressToNextLevel;
	#endregion
	
	#region Properties
	public uint PrevXP { get { return m_PrevXP; } }
	public uint CurrXP { get { return m_CurrXP; } }
	public uint AddedXP { get { return m_AddedXP; } }
	public uint CurrLevel { get { return m_CurrLevel; } }
	public float NormalizedProgressToNextLevel { get { return m_NormalizedProgressToNextLevel; } }
	#endregion
	
	#region Functions
	public AceXPResponse(uint prevXP, uint currXP, uint addedXP, uint currLevel, float normlizedProgressToNextLevel)
	{
		m_PrevXP = prevXP;
		m_CurrXP = currXP;
		m_AddedXP = addedXP;
		m_CurrLevel = currLevel;
		m_NormalizedProgressToNextLevel = normlizedProgressToNextLevel;
	}
	#endregion
}