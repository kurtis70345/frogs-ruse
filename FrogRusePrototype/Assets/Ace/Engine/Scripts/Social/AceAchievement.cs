﻿using UnityEngine;
using System.Collections;

public class AceAchievement
{
	#region Variables
	readonly string m_AchievementName = "";
	readonly string m_InternalName = "";
	readonly string m_Description = "";
	readonly uint m_Points;
	readonly uint m_Steps = 1;
	uint m_StepsCompleted;
	#endregion

	#region Properties
	public string AchievementName { get { return m_AchievementName; } }
	public string InternalName { get { return m_InternalName; } }
	public string Description { get { return m_Description; } }
	public uint Points { get { return m_Points; } }
	public uint Steps { get { return m_Steps; } }
	public uint StepsCompleted 
	{ 
		get { return m_StepsCompleted; }
		set { m_StepsCompleted = value; }
	}
	public float PercentageCompleted { get { return (float)StepsCompleted / (float)Steps; } }
	public bool IsCompleted { get { return StepsCompleted >= Steps; } }
	#endregion

	#region Functions
	public AceAchievement(string achievementName, string internalName,
	                      string description, uint points, uint stepsCompleted, uint steps) 
	{
		m_AchievementName = achievementName;
		m_InternalName = internalName;
		m_Description = description;
		m_Points = points;
		m_StepsCompleted = stepsCompleted;
		m_Steps = steps;
	}

	public void IncrementStepsCompleted(uint steps)
	{
		m_StepsCompleted += steps;
	}
	#endregion
}
