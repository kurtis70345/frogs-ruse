﻿using UnityEngine;
using System.Collections;


#if ALLOW_ACE_GOOGLEPLAY
using UnionAssets.FLE;
public class AcePlayService_GooglePlay : AcePlayService
{
    public AcePlayService_GooglePlay(AcePlayServiceGame game)
        : base(game)
    {
        GooglePlayConnection.instance.connect(/*GooglePlayConnection.CLIENT_GAMES | GooglePlayConnection.CLIENT_APPSTATE*/);
    }

	protected override void SetupCallbacks()
    {
        GooglePlayConnection.instance.addEventListener(GooglePlayConnection.PLAYER_CONNECTED, OnPlayerConnected);
        GooglePlayConnection.instance.addEventListener(GooglePlayConnection.PLAYER_DISCONNECTED, OnPlayerDisconnected);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.FRIENDS_LOADED, OnPlayerInfoLoaded);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.ACHIEVEMENT_UPDATED, OnAchievmentUpdated);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.SCORE_SUBMITED, OnScoreSubmited);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.LEADERBOARDS_LOADED, OnLeaderBoardsLoaded);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.ACHIEVEMENTS_LOADED, OnAchievmentsLoaded);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.SCORE_REQUEST_RECEIVED, OnScoreListLoaded);
    }

    public override bool IsLoggedIn()
    {
        return GooglePlayConnection.instance.isInitialized;
    }

    public override string ServiceName()
    {
        return "GooglePlay";
    }

    public override void ShowRatingPopup(string title, string text, string url)
	{
        AndroidRateUsPopUp rate = AndroidRateUsPopUp.Create(title, text, url, AceTranslator.Translate(RateNowTextKey),
            AceTranslator.Translate(RateLaterTextKey), AceTranslator.Translate(RateNeverTextKey));
         rate.addEventListener(BaseEvent.COMPLETE, OnRatePopUpClose);
	}

    private void OnRatePopUpClose(CEvent e)
    {
        (e.dispatcher as AndroidRateUsPopUp).removeEventListener(BaseEvent.COMPLETE, OnRatePopUpClose);
        string result = e.data.ToString();
        PlayerPrefs.SetInt(HasBeenRatedKey, result == AceTranslator.Translate(RateLaterTextKey) ? 0 : 1);
    }

    #region UserInfo
    public override string GetUserName()
    {
        string userName = "";
        if (GooglePlayManager.instance != null && GooglePlayManager.instance.player != null)
        {
            userName = GooglePlayManager.instance.player.name;
        }
        else
        {
            userName = "";
        }
        return userName;
    }
    #endregion

    #region Leaderboards
    public override void ShowLeaderboardsUI() 
    {
        GooglePlayManager.instance.ShowLeaderBoardsUI();
    }

    public override void ShowLeaderboard(string leaderboardName) 
    {
        GooglePlayManager.instance.ShowLeaderBoard(leaderboardName);
    }

    public override void SubmitLeaderboardScore(string leaderboardName, int score) 
    {
        GooglePlayManager.instance.SubmitScore(leaderboardName, score);
    }

    public override void LoadLeaderboards()
    {
        GooglePlayManager.instance.LoadLeaderBoards();
    }

	public override void GetLeaderboardScore (string leaderboardId)
	{
		GPLeaderBoard lb = GooglePlayManager.instance.GetLeaderBoard(leaderboardId);
		if (lb != null)
		{
			m_Game.OnReceivedLeaderboard(new AceLeaderboard(lb));
		}
	}
    #endregion

    #region Achievements
    public override void ShowAchievementsUI()
    {
        GooglePlayManager.instance.ShowAchievementsUI();
    }

    public override void ReportAchievement(string achievementName) 
    {
        GooglePlayManager.instance.UnlockAchievementById(achievementName);
    }

    public override void IncrementAchievement(string achievementName, int numSteps)
    {
        GooglePlayManager.instance.IncrementAchievement(achievementName, numSteps);
    }

    public override void RevealAchievement(string achievementName)
    {
        GooglePlayManager.instance.RevealAchievement(achievementName);
    }

    public override void LoadAchievements()
    {
        GooglePlayManager.instance.LoadAchievements();
    }
    #endregion

	#region Callbacks
	protected override void OnPlayerConnected()
	{
		//GooglePlayManager.instance.loadPlayer();
		InvokeOnPlayerConnected(new AcePlayServiceResult());
        GooglePlayManager.instance.LoadLeaderBoards();
        GooglePlayManager.instance.LoadAchievements();
	}
	
	protected override void OnPlayerDisconnected()
	{
		InvokeOnPlayerDisconnected(new AcePlayServiceResult());
	}
	
	protected override void OnPlayerInfoLoaded(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnPlayerLoaded(new AcePlayServiceResult(result));
		}
		else
		{
			DisplayMessage("Error", "Failed to load player info");
		}
	}
	
	protected override void OnAchievmentsLoaded(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnAchievementsLoaded(new AcePlayServiceResult(result));
		}
		else
		{
			DisplayMessage("OnAchievmentsLoaded error: ", result.message);
		}
	}
	
	protected override void OnAchievmentUpdated(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnAchievementsUpdated(new AcePlayServiceResult(result));
		}
	}
	
	protected override void OnLeaderBoardsLoaded(CEvent e)
	{
		GooglePlayManager.instance.removeEventListener(GooglePlayManager.LEADERBOARDS_LOADED, OnLeaderBoardsLoaded);
		
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnLeaderboardsLoaded(new AcePlayServiceResult(result));
		}
		else
		{
			DisplayMessage("OnLeaderBoardsLoaded error: ", result.message);
		}
	}
	
	protected override void OnScoreSubmited(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnScoreSubmitted(new AcePlayServiceResult(result));
		}
	}

    private void OnScoreListLoaded()
    {
        Debug.Log("OnScoreListLoaded!!");
        //SA_StatusBar.text = "Scores Load Finished";
        //loadedLeaderBoard = GooglePlayManager.instance.GetLeaderBoard(LEADERBOARD_ID);
    }

	public override void DisplayMessage(string title, string message)
	{
		AndroidMessage.Create(title, message);
	}
	#endregion
}
#endif
