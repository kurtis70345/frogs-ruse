﻿using UnityEngine;
using System.Collections;

public class AceVideoSharer_Kamcord : AceVideoSharer
{
#if FALSE
    #region Variables
    static AceVideoSharer_Kamcord m_Instance;
    #endregion

    #region Functions
    void Awake()
    {
        if (m_Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public static AceVideoSharer_Kamcord Get()
    {
        if (m_Instance == null)
        {
            m_Instance = FindObjectOfType(typeof(AceVideoSharer_Kamcord)) as AceVideoSharer_Kamcord;
            if (m_Instance == null)
            {
                m_Instance = (AceVideoSharer_Kamcord)Resources.Load("AceVideoSharer_Kamcord", typeof(AceVideoSharer_Kamcord));
                DontDestroyOnLoad(m_Instance.gameObject);
            }
        }
        return m_Instance;
    }


    public override void ShowUI()
    {
        Debug.Log("ShowUI");
        Kamcord.ShowWatchView();
    }

    public override void StopRecording()
    {
        Debug.Log("StopRecording");
        Kamcord.StopRecording();
    }

    public override void StartRecording()
    {
        Debug.Log("StartRecording");
        Kamcord.StartRecording();
    }

    public override void PauseRecording()
    {
        Debug.Log("PauseRecording");
        Kamcord.Pause();
        m_IsPaused = true;
    }

    public override void ResumeRecording()
    {
        Debug.Log("ResumeRecording");
        Kamcord.Resume();
        m_IsPaused = false;
    }

    public override void PlayLastRecording()
    {
        Kamcord.ShowView();
    }

    public override void ShowSharingUI()
    {
        Debug.Log("ShowSharingUI");
        Kamcord.ShowWatchView();
    }

    public override bool IsRecordingSupported()
    {
        return Kamcord.IsEnabled();
    }

    //void OnGUI()
    //{
    //    GUILayout.Label(Kamcord.GetDisabledReason());
    //}
    #endregion
#endif
}
