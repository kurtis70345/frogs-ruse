﻿using UnityEngine;
using System.Collections;

public class AcePlayServiceGame : MonoBehaviour
{
    #region Variables
	public bool m_UseGameCircle = false;
    protected AcePlayService m_PlayService;
    #endregion

    #region Properties
    public string UserName
    {
        get { return m_PlayService.GetUserName(); }
    }

    public bool IsLoggedIn
    {
        get { return m_PlayService.IsLoggedIn(); }
    }

    public string PlayServiceName
    {
        get { return m_PlayService.ServiceName(); }
    }
    #endregion

    #region Functions
    public virtual void Awake()
    {

#if UNITY_ANDROID && !UNITY_EDITOR
		if (m_UseGameCircle)
		{
			m_PlayService = new AcePlayService_GameCircle(this);
		}
		else
		{
			m_PlayService = new AcePlayService_GooglePlay(this);
		} 
#elif UNITY_IPHONE && !UNITY_EDITOR
        m_PlayService = new AcePlayService_GameCenter(this);
#else
        m_PlayService = new AcePlayService_Unity(this);
#endif

		Connect();
    }

    public void Connect()
    {
        m_PlayService.Connect();
    }

    public void ReportAchievement(string achievementName)
    {
		Debug.Log("ReportAchievement: " + achievementName);
        m_PlayService.ReportAchievement(achievementName);
    }

    public void IncrementAchievement(string achievementName, int numSteps)
    {
        m_PlayService.IncrementAchievement(achievementName, numSteps);
    }

	public void IncrementAchievement(string achievementName, int numSteps, AcePlayService.OnIncrementedAchievement cb)
	{
		m_PlayService.IncrementAchievement(achievementName, numSteps, cb);
	}
	
	public void GetAllAchievements()
	{
		m_PlayService.GetAllAchievements(null);
	}

    public void SubmitLeaderboardScore(string leaderboardName, int score)
    {
        m_PlayService.SubmitLeaderboardScore(leaderboardName, score);
    }

    public void ShowAchievementsUI()
    {
        m_PlayService.ShowAchievementsUI();
    }

    public void ShowLeaderboardsUI()
    {
        m_PlayService.ShowLeaderboardsUI();
    }

    public void ShowRatingPopUp(string title, string text, string url)
    {
        m_PlayService.ShowRatingPopup(title, text, url);
    }

	/*public void AddListener(AcePlayService.PlayServiceEventType eventType, )
	{
		m_PlayService.AddListener(AcePlayService.PlayServiceEventType.
	}*/

	public void GetLeaderboardData(string leaderboardId, AcePlayService.OnLeaderboardDataReceived cb)
	{
		//Debug.LogWarning("instance id: " + gameObject.GetInstanceID());
		m_PlayService.GetLeaderboardData(leaderboardId, cb);
	}

    public virtual void OnReceivedLeaderboard(AceLeaderboard lb)
    {

    }
    #endregion
}
