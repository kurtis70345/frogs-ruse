﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if ALLOW_ACE_DEPENDENCIES
using UnionAssets.FLE;

public class AceSocialNetwork : MonoBehaviour
{
    #region Constants
    public enum SocialEvent
    {
        Initialized,
        Authentication_Succeeded,
        User_Data_Load_Succeeded,
        User_Data_Load_Failed,
        Post_Succeeded,
        Post_Failed,
        Friends_Data_Load_Succeeded,
        Friends_Data_Load_Failed,
        Game_Focus_Changed,

		NUM_EVENTS
    }

	public delegate void OnSocialNetworkEvent(AceSocialNetworkEventResult result);
    #endregion

    #region Variables
	protected AceSocialNetworkUserInfo m_UserInfo;
    protected bool m_IsUserInfoLoaded = false;
    protected bool m_IsFriendsInfoLoaded = false;
    protected bool m_IsAuthenticated = false;
	Dictionary<SocialEvent, OnSocialNetworkEvent> m_SocialEventCBs 
		= new Dictionary<SocialEvent, OnSocialNetworkEvent>();
    #endregion

    #region Functions
	public virtual void Start()
	{
		for (int i = 0; i < (int)SocialEvent.NUM_EVENTS; i++)
		{
			m_SocialEventCBs.Add((SocialEvent)i, null);
		}
		Init();
		SetupListeners();
	}

	protected virtual void SetupListeners() { }
    public virtual void Post(string status) { }
    public virtual void Post(string status, Texture2D image) { }
    public virtual void AuthenticateUser() { }
    public virtual void Init() { }
    protected virtual void LoadUserData() { }
    public virtual void Login() { }
    public virtual Texture2D GetProfileImage() { return null;  }
	public virtual AceSocialNetworkUserInfo GetUserInfo() { return m_UserInfo; }
	public virtual bool IsLoggedIn() { return false; }

	public virtual void Logout() 
	{
		m_IsUserInfoLoaded = false;
		m_IsAuthenticated = false;
	}

	public virtual void AddEventListener(SocialEvent socialEvent, OnSocialNetworkEvent cb) 
	{
		m_SocialEventCBs[socialEvent] = cb;
	}

	protected void OnFocusChanged(CEvent e)
	{
		//bool focus = (bool)e.data;
		if (m_SocialEventCBs[SocialEvent.Game_Focus_Changed] != null)
		{
			m_SocialEventCBs[SocialEvent.Game_Focus_Changed](new AceSocialNetworkEventResult());
		}
	}

	protected void OnUserDataLoadFailed() 
	{
		DisplayMessage("Load Error", "User data failed to load");
		if (m_SocialEventCBs[SocialEvent.User_Data_Load_Failed] != null)
		{
			m_SocialEventCBs[SocialEvent.User_Data_Load_Failed](new AceSocialNetworkEventResult());
		}
	}

	protected virtual void OnUserDataLoaded()
	{
		m_IsUserInfoLoaded = true;
		if (m_SocialEventCBs[SocialEvent.User_Data_Load_Succeeded] != null)
		{
			m_SocialEventCBs[SocialEvent.User_Data_Load_Succeeded](new AceSocialNetworkEventResult());
		}
	}
	
	protected void OnFriendDataLoadFailed()
	{
		DisplayMessage("Load Error", "Friends data failed to load");
		if (m_SocialEventCBs[SocialEvent.Friends_Data_Load_Failed] != null)
		{
			m_SocialEventCBs[SocialEvent.Friends_Data_Load_Failed](new AceSocialNetworkEventResult());
		}
	}
	
	protected void OnFriendsDataLoaded() 
	{
		//foreach(FacebookUserInfo friend in SPFacebook.instance.firendsList)
		//{
		//    friend.LoadProfileImage(FacebookProfileImageSize.square);
		//}

		if (m_SocialEventCBs[SocialEvent.Friends_Data_Load_Succeeded] != null)
		{
			m_SocialEventCBs[SocialEvent.Friends_Data_Load_Succeeded](new AceSocialNetworkEventResult());
		}
		
		m_IsFriendsInfoLoaded = true;
	}
	
	protected void OnInit()
	{
		if(IsLoggedIn()) 
		{
			OnAuth();
		} 
		else
		{
			//statusMessage = "user Login -> fale";
			//Login();
		}
	}
	
	protected void OnAuth()
	{
		m_IsAuthenticated = true;
		if (m_SocialEventCBs[SocialEvent.Authentication_Succeeded] != null)
		{
			m_SocialEventCBs[SocialEvent.Authentication_Succeeded](new AceSocialNetworkEventResult());
		}
	}
	
	protected void OnPost()
	{
		if (m_SocialEventCBs[SocialEvent.Post_Succeeded] != null)
		{
			m_SocialEventCBs[SocialEvent.Post_Succeeded](new AceSocialNetworkEventResult());
		}
	}
	
	protected void OnPostFailed()
	{
		DisplayMessage("Network Error", "Post failed");
		if (m_SocialEventCBs[SocialEvent.Post_Failed] != null)
		{
			m_SocialEventCBs[SocialEvent.Post_Failed](new AceSocialNetworkEventResult());
		}
	}

    public void DisplayMessage(string title, string message)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidMessage.Create(title, message);
#endif
    }
    #endregion
}

#endif
