﻿using UnityEngine;
using System;
using System.Collections;


public class AcePlayService_Ace : AcePlayService
{
	#region Variables
	string m_AchievementsUrl = "";
	string m_GetAchievementsUrl = "";
	string m_LeaderboardsUrl = "";
	string m_GetLeaderboardUrl = "";
	AceAchievement[] m_Achievements;
	#endregion

	#region Functions
	public AcePlayService_Ace(AcePlayServiceGame game, string achievementsUrl, string getAchievementUrl, string leaderboardsUrl,
	                          string getLeaderboardUrl)
        : base(game) 
	{
		m_AchievementsUrl = achievementsUrl;
		m_GetAchievementsUrl = getAchievementUrl;
		m_LeaderboardsUrl = leaderboardsUrl;
		m_GetLeaderboardUrl = getLeaderboardUrl;
	}

    public override bool IsLoggedIn()
    {
		return !string.IsNullOrEmpty(AceAccountController.Account.Username);
    }

    public override string GetUserName()
    {
		return AceAccountController.Account != null ? AceAccountController.Account.Username : "";
    }

	public override bool IsAchievementUnlocked (string achievementName)
	{
		AceAchievement achievement = GetAchievement(achievementName);
		return achievement != null ? achievement.IsCompleted : false;
	}

	AceAchievement GetAchievement(string internalName)
	{
		AceAchievement achievement = Array.Find<AceAchievement>(m_Achievements, a => a.InternalName == internalName);
		if (achievement == null)
		{
			Debug.LogError("Could not find achievement with name: " + achievement.InternalName);
		}
		return achievement;
	}

	public override void ReportAchievement (string achievementName)
	{
		IncrementAchievement(achievementName, 1);
	}

	public override void IncrementAchievement (string achievementName, int numSteps)
	{
		IncrementAchievement(achievementName, numSteps, null);
	}

	public override void IncrementAchievement (string achievementName, int numSteps, OnIncrementedAchievement cb)
	{
#if !UNITY_EDITOR
		if (!IsAchievementUnlocked(achievementName))
		{
			m_Game.StartCoroutine(IncrementAchievement(achievementName, GetUserName(), numSteps, cb));
		}
#else
		m_Game.StartCoroutine(IncrementAchievement(achievementName, GetUserName(), numSteps, cb));
#endif
	}

	IEnumerator IncrementAchievement(string internalAchievementName, string username, int numSteps, OnIncrementedAchievement cb)
	{
		if (string.IsNullOrEmpty(username))
		{
			yield break;
		}

		WWWForm form = new WWWForm();
		form.AddField("username", username);
		form.AddField("internalAchievementName", internalAchievementName);
		form.AddField("stepsIncremented", numSteps);
		WWW www = new WWW(m_AchievementsUrl, form);

		Debug.Log(m_AchievementsUrl + " " + internalAchievementName + " " + username + " " + numSteps);

		yield return www;

		if (!www.isDone)
		{
			yield return new WaitForEndOfFrame();
		}

		Debug.Log(www.text);

		AceAchievement achievement = AceJsonInterpreter.ParseAchievement(www.text);

		AceAchievement cachedAchievement = GetAchievement(achievement.InternalName);
		if (cachedAchievement != null)
		{
			cachedAchievement.StepsCompleted = achievement.StepsCompleted; 
		}

		if (achievement.IsCompleted)
		{
			AceAchievementUnlockedMenu.instance.Display(achievement.AchievementName,
			                                            achievement.Description, achievement.Points);
		}

		if (cb != null)
		{
			cb(achievement);
		}
	}

	public override void SubmitLeaderboardScore (string leaderboardName, int score)
	{
		m_Game.StartCoroutine(SubmitScore(leaderboardName, GetUserName(), score));
	}

	IEnumerator SubmitScore(string internalLeaderboardName, string username, float score)
	{
		if (string.IsNullOrEmpty(username))
		{
			yield break;
		}

		WWWForm form = new WWWForm();
		form.AddField("username", username);
		form.AddField("internalLeaderboardName", internalLeaderboardName);
		form.AddField("score", score.ToString());
		WWW www = new WWW(m_LeaderboardsUrl, form);
		
		yield return www;

		//Debug.Log(www.text);
		
		if (!www.isDone)
		{
			yield return new WaitForEndOfFrame();
		}
	}

	public override void GetAllAchievements (OnReceivedAchievements cb)
	{
		m_Game.StartCoroutine(GetAllAchievementsCR(GetUserName(), cb));
	}

	IEnumerator GetAllAchievementsCR(string username, OnReceivedAchievements cb)
	{
		if (string.IsNullOrEmpty(username))
		{
			yield break;
		}

		WWWForm form = new WWWForm();
		form.AddField("username", username);
		WWW www = new WWW(m_GetAchievementsUrl, form);
		
		yield return www;
		
		if (!www.isDone)
		{
			yield return new WaitForEndOfFrame();
		}

		m_Achievements = AceJsonInterpreter.ParseAchievements(www.text);

		if (cb != null)
		{
			cb(m_Achievements);
		}
	}

	public override void GetLeaderboardData (string leaderboardId, OnLeaderboardDataReceived cb)
	{
		m_Game.StartCoroutine(GetLeaderboardData(leaderboardId, GetUserName(), cb));
	}

	IEnumerator GetLeaderboardData(string internalLeaderboardName, string username, OnLeaderboardDataReceived cb)
	{
		/*if (string.IsNullOrEmpty(username))
		{
			yield break;
		}*/

		WWWForm form = new WWWForm();
		form.AddField("username", username);
		form.AddField("internalLeaderboardName", internalLeaderboardName);
		WWW www = new WWW(m_GetLeaderboardUrl, form);
		
		yield return www;
		
		Debug.Log(www.text);

		AceLeaderboard leaderboard = AceJsonInterpreter.ParseLeaderboard(www.text);
		
		if (!www.isDone)
		{
			yield return new WaitForEndOfFrame();
		}

		if (cb != null)
		{
			cb(leaderboard);
		}
	}
	#endregion
}
