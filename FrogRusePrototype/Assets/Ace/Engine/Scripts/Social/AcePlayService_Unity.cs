﻿using UnityEngine;
using System.Collections;

public class AcePlayService_Unity : AcePlayService
{
    public AcePlayService_Unity(AcePlayServiceGame game)
        : base(game) { }

    public override bool IsLoggedIn()
    {
        return true;
    }

    public override string GetUserName()
    {
        return "UnityUser";
    }
}
