﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceVideoSharer : MonoBehaviour
{
    #region Constants
    public class VideoData
    {
        public int videoId;
        public float uploadProgress;

        public VideoData() { }
        public VideoData(int _videoId)
        {
            videoId = _videoId;
            uploadProgress = 0;
        }

        public VideoData(int _videoId, float _uploadProgress)
        {
            videoId = _videoId;
            uploadProgress = _uploadProgress;
        }
    }

    public enum VideoEvents
    {
        OnUploadStarted,
        OnUploadProgress,
        OnUploadFinished,
        OnStartedRecording,
        OnStoppedRecording,

        NUM_EVENTS
    }

    public delegate void OnVideoEvent(VideoData videoData);
    #endregion

    #region Variables
    protected bool m_IsRecording = true;
    protected bool m_IsPaused = false;
    protected bool m_IsRecordingFinished = false;
    Dictionary<VideoEvents, OnVideoEvent> m_Events = new Dictionary<VideoEvents, OnVideoEvent>();
    #endregion

    #region Functions
    public virtual void Start()
    {
        for (int i = 0; i < (int)VideoEvents.NUM_EVENTS; i++)
        {
            m_Events[(VideoEvents)i] = null;
        }
    }

    public virtual void ShowUI() { }
    public virtual void StopRecording() { }
    public virtual void StartRecording() { }
    public virtual void PauseRecording() { }
    public virtual void ResumeRecording() { }
    public virtual void PlayLastRecording() { }
    public virtual void TakeThumbnail() { }
    public virtual void ShowSharingUI() { }
    public virtual bool IsRecordingSupported() { return false; }

    public void AddEvent(VideoEvents videoEvent, OnVideoEvent cb)
    {
        m_Events[videoEvent] = cb;
    }

    public void RemoveEvent(VideoEvents videoEvent)
    {
        m_Events[videoEvent] = null;
    }

    protected void InvokeEvent(VideoEvents videoEvent, VideoData data)
    {
        if (m_Events[videoEvent] != null)
        {
            m_Events[videoEvent](data);
        }
    }

	void OnLevelWasLoaded(int level)
	{
		//Debug.Log("OnLevelWasLoaded");
		if (m_IsRecording)
		{
			StopRecording();
		}
	}
    #endregion
}
