﻿using UnityEngine;
using System.Collections;

public class AceSocialNetworkUserInfo
{
    #region Variables
    public readonly string id = string.Empty;
    public readonly string name = string.Empty;
    public readonly string first_name = string.Empty;
    public readonly string last_name = string.Empty;
    public readonly string username = string.Empty;

    public readonly string profile_url = string.Empty;
    public readonly string email = string.Empty;

	public readonly string location = string.Empty;
    public readonly string locale = string.Empty;
    #endregion

    #region Functions
    public AceSocialNetworkUserInfo(string _id, string _name, string _first_name, string _last_name, string _username,
        string _profile_url, string _email, string _location, string _locale)
    {
        id = _id;
        name = _name;
        first_name = _first_name;
        last_name = _last_name;
        username = _username;
        profile_url = _profile_url;
        email = _email;
		location = _location;
        locale = _locale;
    }

    public virtual int GetNumFriends() { return 0; }
    #endregion
}
