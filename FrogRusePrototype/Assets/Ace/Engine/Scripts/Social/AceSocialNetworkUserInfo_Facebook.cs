﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES
public class AceSocialNetworkUserInfo_Facebook : AceSocialNetworkUserInfo
{
    #region Constants
    public enum Gender
    {
        Male,
        Female,
        Unknown
    }

    public enum ProfileImageSize
    {
        Large,
        Normal,
        Small,
        Square
    }
    #endregion

    #region Variables
    FacebookUserInfo info;
    public readonly Gender gender;
    #endregion

    #region Functions
    public AceSocialNetworkUserInfo_Facebook(FacebookUserInfo _info)
        : base(_info.id, _info.name, _info.first_name, _info.last_name, _info.username, _info.profile_url, _info.email, _info.location, _info.locale)
    {
        info = _info;
        gender = SwapGenderEnum(_info.gender);
    }

    public override int GetNumFriends()
    {
        return SPFacebook.instance.friendsList.Count;
    }

    public string GetProfileUrl(ProfileImageSize size)
    {
        return info.GetProfileUrl(SwapProfileSizeEnum(size));
    }

    public Texture2D GetProfileImage(ProfileImageSize size)
    {
        return info.GetProfileImage(SwapProfileSizeEnum(size));
    }

    public void LoadProfileImage(ProfileImageSize size)
    {
        info.LoadProfileImage(SwapProfileSizeEnum(size));
    }

    Gender SwapGenderEnum(GoogleGender gender)
    {
        Gender retval = Gender.Male;
        switch (gender)
        {
		case GoogleGender.Male:
                retval = Gender.Male;
                break;

		case GoogleGender.Female:
                retval = Gender.Female;
                break;

		case GoogleGender.Unknown:
                retval = Gender.Unknown;
                break;

            default:
                Debug.LogError("Couldn't convert gender enum");
                break;
        }

        return retval;
    }

    FacebookProfileImageSize SwapProfileSizeEnum(ProfileImageSize size)
    {
        FacebookProfileImageSize retval = FacebookProfileImageSize.normal;
        switch (size)
        {
            case ProfileImageSize.Large:
                retval = FacebookProfileImageSize.large;
                break;

            case ProfileImageSize.Normal:
                retval = FacebookProfileImageSize.normal;
                break;

            case ProfileImageSize.Small:
                retval = FacebookProfileImageSize.small;
                break;

            case ProfileImageSize.Square:
                retval = FacebookProfileImageSize.square;
                break;

            default:
                Debug.LogError("Couldn't convert FacebookProfileImageSize enum");
                break;
        }

        return retval;
    }
    #endregion
}
#endif