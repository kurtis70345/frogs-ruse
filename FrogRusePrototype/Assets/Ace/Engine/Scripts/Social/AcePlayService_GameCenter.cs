using UnityEngine;
using System.Collections;


#if ALLOW_ACE_GAMECENTER
using UnionAssets.FLE;
public class AcePlayService_GameCenter : AcePlayService
{
    #region Variables
    bool m_IsInitialized = false;
	OnLeaderboardDataReceived m_LeaderboardDataReceivedCB;
    #endregion

    #region Functions
    public AcePlayService_GameCenter(AcePlayServiceGame game)
        : base(game) { }

    protected override void StartService ()
	{
		//IOSNative.instance.Init();
		GameCenterManager.Init();
	}

	protected override void SetupCallbacks ()
	{
		//Achievement registration. If you will skipt this step GameCenterManager.achievements array will contain only achievements with reported progress 
		//GameCenterManager.registerAchievement (TEST_ACHIEVEMENT_1_ID);
		//GameCenterManager.registerAchievement (TEST_ACHIEVEMENT_2_ID);
		
		/*
		//Listen for the Game Center events
		GameCenterManager.dispatcher.addEventListener (GameCenterManager.GAME_CENTER_ACHIEVEMENTS_LOADED, OnAchievmentsLoaded);
		GameCenterManager.dispatcher.addEventListener (GameCenterManager.GAME_CENTER_ACHIEVEMENT_PROGRESS, OnAchievementProgress);
		GameCenterManager.dispatcher.addEventListener (GameCenterManager.GAME_CENTER_ACHIEVEMENTS_RESET, OnAchievementsReset);
		GameCenterManager.dispatcher.addEventListener (GameCenterManager.GAME_CENTER_LEADER_BOARD_SCORE_LOADED, OnLeaderBoarScoreLoaded);
		GameCenterManager.dispatcher.addEventListener (GameCenterManager.GAME_CENTER_PLAYER_AUTHENTICATED, OnAuth);
		*/
		//GameCenterManager.OnAchievementsLoaded += OnAchievmentsLoaded;
		//GameCenterManager.OnAchievementsProgress += OnAchievementProgress;
		//GameCenterManager.OnAchievementsReset += OnAchievementsReset;
		GameCenterManager.OnPlayerScoreLoaded += OnLeaderBoarScoreLoaded;
		GameCenterManager.OnAuthFinished += OnAuth;
	}

    public override bool IsLoggedIn()
    {
        return m_IsInitialized;
    }

    public override string ServiceName()
    {
        return "GameCenter";
    }

    public override void ShowRatingPopup(string title, string text, string url)
	{
        IOSRateUsPopUp rate = IOSRateUsPopUp.Create(title, text, AceTranslator.Translate(RateNowTextKey),
            AceTranslator.Translate(RateLaterTextKey), AceTranslator.Translate(RateNeverTextKey));

        rate.OnComplete += onRatePopUpClose;
	}

	private void onRatePopUpClose(IOSDialogResult result)
    {
        //(e.dispatcher as IOSRateUsPopUp).removeEventListener(BaseEvent.COMPLETE, onRatePopUpClose);
        //string result = e.data.ToString();

		PlayerPrefs.SetInt(HasBeenRatedKey, result == IOSDialogResult.REMIND ? 0 : 1);
    }

    #region UserInfo
    public override string GetUserName()
    {
        string username = "";
        if (GameCenterManager.Player != null)
        {
			username = GameCenterManager.Player.Alias;
        }
        return username;
    }
    #endregion

    #region Leaderboards
    public override void ShowLeaderboardsUI()
	{
		GameCenterManager.ShowLeaderboards();
	}

	public override void ShowLeaderboard (string leaderboardName)
	{
		GameCenterManager.ShowLeaderboard(leaderboardName);
	}

	public override void SubmitLeaderboardScore (string leaderboardName, int score)
	{
		if (IsLoggedIn())
		{
			GameCenterManager.ReportScore(score, leaderboardName);
		}
	}

	public override void GetLeaderboardScore (string leaderboardId)
	{
		if (IsLoggedIn())
		{
			Debug.Log("getting player score because I'm logged in");
			GameCenterManager.LoadCurrentPlayerScore(leaderboardId);
		}

	}
	#endregion

	#region Achievements
	public override void ShowAchievementsUI()
	{
		GameCenterManager.ShowAchievements();
	}

	public override void ReportAchievement(string achievementName)
	{
		if (IsLoggedIn())
		{
			GameCenterManager.SubmitAchievement(100, achievementName, true);
		}
	}
	
	public override void IncrementAchievement(string achievementName, int numSteps)
	{
		if (IsLoggedIn())
		{
			GameCenterManager.SubmitAchievement(GameCenterManager.GetAchievementProgress(achievementName) + numSteps,
			                                    achievementName, true);
		}
	}
	#endregion

	#region Callbacks
	/*protected override void OnAchievmentsLoaded(CEvent e) 
	{
		Debug.Log ("Achievemnts was loaded from IOS Game Center");
		
		foreach(AchievementTemplate tpl in GameCenterManager.achievements) 
		{
			Debug.Log (tpl.id + ":  " + tpl.progress);
		}
	}*/
	
	/*private void OnAchievementsReset() 
	{
		Debug.Log ("All Achievemnts were reseted");
	}*/
	
	/*private void OnAchievementProgress(GK_AchievementProgressResult result) 
	{
		Debug.Log ("OnAchievementProgress");

		ISN_AcheivmentProgressResult result = e.data as ISN_AcheivmentProgressResult;
		
		if(result.IsSucceeded) 
		{
			AchievementTemplate tpl = result.info;
			Debug.Log (tpl.id + ":  " + tpl.progress.ToString());
		}
	}*/
	
	private void OnLeaderBoarScoreLoaded(GK_PlayerScoreLoadedResult result) 
	{
		//LeaderBoardScoreData data = e.data as LeaderBoardScoreData;
		//ISN_PlayerScoreLoadedResult data = e.data as ISN_PlayerScoreLoadedResult;
		if (result.IsSucceeded)
        {
			m_Game.OnReceivedLeaderboard(new AceLeaderboard(result));
        }
        else
        {
            Debug.LogError("OnLeaderBoarScoreLoaded failed. Null leaderboard data returned");
        }
        //if (m_LeaderboardDataReceivedCB != null)
        //{
        //    m_LeaderboardDataReceivedCB(new AceLeaderboard(data));
        //}
		//IOSNative.showMessage("Leader Board " + data.leaderBoardId, "Score: " + data.leaderBoardScore + "\n" + "Rank:" + data.GetRank());
	}

	private void OnAuth(ISN_Result res) {
		
		
		if (res.IsSucceeded) 
		{
			m_IsInitialized = true;
			Debug.Log("Player Authed");
		} 
		else 
		{
			m_IsInitialized = false;
			Debug.Log("Player auntification failed");
		}
		
		
	}
	
	/*private void OnAuthFailed() 
	{
        m_IsInitialized = false;
		DisplayMessage("Game Center ", "Player authentification failed");
		//if you got this event it means that player canseled auntification flow. With probably mean that playr do not whant to use gamcenter in your game
	}*/

	public override void DisplayMessage(string title, string message)
	{
        IOSNativePopUpManager.showMessage(title, message);
	}
	#endregion
	#endregion
}
#endif
