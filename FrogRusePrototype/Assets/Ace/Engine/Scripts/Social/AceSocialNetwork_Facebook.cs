﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES

public class AceSocialNetwork_Facebook : AceSocialNetwork
{
    #region Variables
   
    #endregion

    #region Functions

    public override void Init()
    {
        SPFacebook.instance.Init();
    }

    public override void Login()
    {
		Debug.Log("facebook login!!");
        SPFacebook.instance.Login("email,publish_actions");
    }

    public override void Logout()
    {
		base.Logout();
        SPFacebook.instance.Logout();
    }

	public override bool IsLoggedIn ()
	{
		return SPFacebook.instance.IsLoggedIn;
	} 

    protected override void LoadUserData()
    {
        SPFacebook.instance.LoadUserData();
    }

    public override void Post(string status)
    {
        /*SPFacebook.instance.Post(
                        link: "https://example.com/myapp/?storyID=thelarch",
                        linkName: "The Larch",
                        linkCaption: "I thought up a witty tagline about larches",
                        linkDescription: "There are a lot of larch trees around here, aren't there?",
                        picture: "https://example.com/myapp/assets/1/larch.jpg"
                        );*/
    }

    public override void Post(string status, Texture2D image)
    {
        SPFacebook.instance.PostImage(status, image);
    }

	protected override void SetupListeners ()
	{
		SPFacebook.instance.addEventListener(FacebookEvents.FACEBOOK_INITED, OnInit);
		SPFacebook.instance.addEventListener(FacebookEvents.AUTHENTICATION_SUCCEEDED, OnAuth);
		SPFacebook.instance.addEventListener(FacebookEvents.USER_DATA_LOADED, OnUserDataLoaded);
		SPFacebook.instance.addEventListener(FacebookEvents.USER_DATA_FAILED_TO_LOAD, OnUserDataLoadFailed);
		SPFacebook.instance.addEventListener(FacebookEvents.POST_SUCCEEDED, OnPost);
		SPFacebook.instance.addEventListener(FacebookEvents.POST_FAILED, OnPostFailed);
		SPFacebook.instance.addEventListener(FacebookEvents.FRIENDS_DATA_LOADED, OnFriendsDataLoaded);
		SPFacebook.instance.addEventListener(FacebookEvents.FRIENDS_FAILED_TO_LOAD, OnFriendDataLoadFailed);
		SPFacebook.instance.addEventListener(FacebookEvents.GAME_FOCUS_CHANGED, OnFocusChanged);
	}

	public override void AuthenticateUser ()
	{
		Debug.Log("AuthenticateUser facebook!");
		m_UserInfo = new AceSocialNetworkUserInfo_Facebook(SPFacebook.instance.userInfo);
		base.AuthenticateUser ();

	}
    #endregion
}
#endif