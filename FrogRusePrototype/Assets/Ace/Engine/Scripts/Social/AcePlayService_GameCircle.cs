﻿using UnityEngine;
using System.Collections;


#if FALSE
#if UNITY_IPHONE
using UnionAssets.FLE;
public class AcePlayService_GameCircle: AcePlayService
{
	public AcePlayService_GameCircle(AcePlayServiceGame game)
		: base(game)
	{

	}
}
#else
public class AcePlayService_GameCircle: AcePlayService
{

	string m_Nickname = "";
	public AcePlayService_GameCircle(AcePlayServiceGame game)
        : base(game)
    {
		Debug.Log("attempting to init AcePlayService_GameCircle");
		AGSClient.Init (true, true, true);
    }

	protected override void SetupCallbacks()
    {
		AGSClient.ServiceReadyEvent += serviceReadyHandler;
		AGSClient.ServiceNotReadyEvent += serviceNotReadyHandler;
		AGSLeaderboardsClient.RequestLocalPlayerScoreCompleted += OnRequestLocalPlayerScoreCompleted;
		AGSPlayerClient.RequestLocalPlayerCompleted += OnRequestLocalPlayerInfo;
        /*GooglePlayConnection.instance.addEventListener(GooglePlayConnection.PLAYER_CONNECTED, OnPlayerConnected);
        GooglePlayConnection.instance.addEventListener(GooglePlayConnection.PLAYER_DISCONNECTED, OnPlayerDisconnected);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.FRIENDS_LOADED, OnPlayerInfoLoaded);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.ACHIEVEMENT_UPDATED, OnAchievmentUpdated);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.SCORE_SUBMITED, OnScoreSubmited);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.LEADERBOARDS_LOEADED, OnLeaderBoardsLoaded);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.ACHIEVEMENTS_LOADED, OnAchievmentsLoaded);
        GooglePlayManager.instance.addEventListener(GooglePlayManager.SCORE_REQUEST_RECEIVED, OnScoreListLoaded);*/
    }

    public override bool IsLoggedIn()
    {
		return AGSClient.IsServiceReady() && AGSPlayerClient.IsSignedIn();
    }

    public override string ServiceName()
    {
		return AGSClient.serviceName;
    }

    public override void ShowRatingPopup(string title, string text, string url)
	{
        /*AndroidRateUsPopUp rate = AndroidRateUsPopUp.Create(title, text, url, AceTranslator.Translate(RateNowTextKey),
            AceTranslator.Translate(RateLaterTextKey), AceTranslator.Translate(RateNeverTextKey));
         rate.addEventListener(BaseEvent.COMPLETE, OnRatePopUpClose);*/
	}

    private void OnRatePopUpClose(CEvent e)
    {
        /*(e.dispatcher as AndroidRateUsPopUp).removeEventListener(BaseEvent.COMPLETE, OnRatePopUpClose);
        string result = e.data.ToString();
        PlayerPrefs.SetInt(HasBeenRatedKey, result == AceTranslator.Translate(RateLaterTextKey) ? 0 : 1);*/
    }

    #region UserInfo
    public override string GetUserName()
    {
		return m_Nickname;
	}
    #endregion

    #region Leaderboards
    public override void ShowLeaderboardsUI() 
    {
		AGSLeaderboardsClient.ShowLeaderboardsOverlay();
    }

    public override void ShowLeaderboard(string leaderboardName) 
    {
		AGSLeaderboardsClient.ShowLeaderboardsOverlay();
    }

    public override void SubmitLeaderboardScore(string leaderboardName, int score) 
    {
		AGSLeaderboardsClient.SubmitScore(leaderboardName, (long)score);
    }

    public override void LoadLeaderboards()
    {
		AGSLeaderboardsClient.RequestLeaderboards();
    }

	public override void GetLeaderboardScore (string leaderboardId)
	{
		AGSLeaderboardsClient.RequestLocalPlayerScore(leaderboardId, LeaderboardScope.GlobalAllTime);
	}
    #endregion

    #region Achievements
    public override void ShowAchievementsUI()
    {
		AGSAchievementsClient.ShowAchievementsOverlay();
    }

    public override void ReportAchievement(string achievementName) 
    {
		//AGSAchievementsClient.rep
		AGSAchievementsClient.UpdateAchievementProgress(achievementName, 100);
    }

    public override void IncrementAchievement(string achievementName, int numSteps)
    {
		AGSAchievementsClient.UpdateAchievementProgress(achievementName, (float)numSteps);
    }

    public override void LoadAchievements()
    {
		AGSAchievementsClient.RequestAchievements();
    }
    #endregion

	#region Callbacks

	private void serviceNotReadyHandler (string error)   
	{
		//DisplayMessage("Service is not ready", error);
		Debug.Log("Service is not ready: " + error);

	}
	
	private void serviceReadyHandler ()  
	{
		Debug.Log("Service is ready");
		if (string.IsNullOrEmpty(GetUserName()))
	    {
			// they haven't signed in yet
			AGSPlayerClient.RequestLocalPlayer();
		}
		//AGSClient.ShowSignInPage();
	}

	protected override void OnPlayerConnected()
	{
		//GooglePlayManager.instance.loadPlayer();
		InvokeOnPlayerConnected(new AcePlayServiceResult());
        //GooglePlayManager.instance.loadLeaderBoards();
        //GooglePlayManager.instance.loadAchievements();
	}
	
	protected override void OnPlayerDisconnected()
	{
		InvokeOnPlayerDisconnected(new AcePlayServiceResult());
	}
	
	/*protected override void OnPlayerInfoLoaded(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnPlayerLoaded(new AcePlayServiceResult(result));
		}
		else
		{
			DisplayMessage("Error", "Failed to load player info");
		}
	}
	
	protected override void OnAchievmentsLoaded(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnAchievementsLoaded(new AcePlayServiceResult(result));
		}
		else
		{
			DisplayMessage("OnAchievmentsLoaded error: ", result.message);
		}
	}
	
	protected override void OnAchievmentUpdated(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnAchievementsUpdated(new AcePlayServiceResult(result));
		}
	}
	
	protected override void OnLeaderBoardsLoaded(CEvent e)
	{
		GooglePlayManager.instance.removeEventListener(GooglePlayManager.LEADERBOARDS_LOEADED, OnLeaderBoardsLoaded);
		
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnLeaderboardsLoaded(new AcePlayServiceResult(result));
		}
		else
		{
			DisplayMessage("OnLeaderBoardsLoaded error: ", result.message);
		}
	}
	
	protected override void OnScoreSubmited(CEvent e)
	{
		GooglePlayResult result = e.data as GooglePlayResult;
		if (result.isSuccess)
		{
			InvokeOnScoreSubmitted(new AcePlayServiceResult(result));
		}
	}*/

	private void OnRequestLocalPlayerInfo(AGSRequestPlayerResponse response)
	{
		if (!response.IsError())
		{
			m_Nickname = response.player.alias;
		}
		else
		{
			Debug.Log(response.error);
		}
	}

	private void OnRequestLocalPlayerScoreCompleted ( AGSRequestScoreResponse response ) 
	{
		if (response.IsError())
		{
			DisplayMessage("Error", "OnRequestLocalPlayerScoreCompleted Error: " + response.error);
		} 
		else 
		{
			m_Game.OnReceivedLeaderboard(new AceLeaderboard(response));
		}
	}

    private void OnScoreListLoaded()
    {
        Debug.Log("OnScoreListLoaded!!");
    }

	public override void DisplayMessage(string title, string message)
	{
		//AGSClient.serviceName
		AGSClient.Log(message);
		Debug.Log(message);
	}
	#endregion

}

#endif
#endif
