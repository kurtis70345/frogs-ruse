﻿using UnityEngine;
using System.Collections;


public class AceLeaderboard 
{
	#region Variables
	//LeaderBoardScoreData m_ScoreData;
	//ISN_PlayerScoreLoadedResult m_ScoreData;
	string m_Name = "";
	string m_LeaderboardId = "";
	float m_Score = 0;
	int m_Rank = 0;
	float m_WorldBest = 0;
	#endregion

	#region Properties
	public float Score { get { return m_Score; } }
	public int ScoreAsInt { get { return (int)m_Score; } }
	public string Name { get { return m_Name; } }
	public string Id { get { return m_LeaderboardId; } }
	public int Rank { get { return m_Rank; } }
	public float WorldBestScore { get { return m_WorldBest; } }
	public float WorldBestScoreAsInt { get { return (int)m_WorldBest; } }
	#endregion

	#region Functions
	public AceLeaderboard(string name, string internalName, float playerScore, float worldBestScore)
	{
		m_Name = name;
		m_LeaderboardId = internalName;
		m_Score = playerScore;
		m_WorldBest = worldBestScore;
	}
#if ALLOW_ACE_DEPENDENCIES
	public AceLeaderboard(GK_PlayerScoreLoadedResult data)
	{
		
	}

	public AceLeaderboard(ISN_PlayerScoreLoadedResult data)
	{
		//m_ScoreData = data;
		m_LeaderboardId = data.loadedScore.leaderboardId;
		m_Score = data.loadedScore.score;
		m_Rank = data.loadedScore.rank;
	} 

	public AceLeaderboard(GPLeaderBoard data)
	{
		//m_ScoreData = new ISN_PlayerScoreLoadedResult();
		m_LeaderboardId = data.id;

        GPScore scoreData = data.GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.GLOBAL);
		m_Score = scoreData.score.ToString();
		m_Rank = scoreData.rank;
	}
#endif
#if false
	public AceLeaderboard(AGSRequestScoreResponse data)
	{
		m_LeaderboardId = data.leaderboardId;
		m_Score = data.score.ToString();
		m_Rank = data.rank;
	}
#endif
	#endregion
}
