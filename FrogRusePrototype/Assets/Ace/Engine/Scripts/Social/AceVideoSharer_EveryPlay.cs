﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_EVERYPLAY

public class AceVideoSharer_EveryPlay : AceVideoSharer
{

    #region Variables
    static AceVideoSharer_EveryPlay m_Instance;
    #endregion

    #region Functions
    void Awake()
    {
        if (m_Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public static AceVideoSharer_EveryPlay Get()
    {
        if (m_Instance == null)
        {
            m_Instance = FindObjectOfType(typeof(AceVideoSharer_EveryPlay)) as AceVideoSharer_EveryPlay;
            if (m_Instance == null)
            {
                m_Instance = (AceVideoSharer_EveryPlay)Resources.Load("VideoSharer_Everyplay", typeof(AceVideoSharer_EveryPlay));
                DontDestroyOnLoad(m_Instance.gameObject);
            }
        }
        return m_Instance;
    }

    public override void Start()
    {
        base.Start();

        //if (Everyplay != null)
        {
            Everyplay.UploadDidStart += UploadDidStart;
            Everyplay.UploadDidProgress += UploadDidProgress;
            Everyplay.UploadDidComplete += UploadDidComplete;
            
            Everyplay.RecordingStarted += RecordingStarted;
            Everyplay.RecordingStopped += RecordingStopped;

			Everyplay.SetMetadata("game_name", "Epic Dragons");
            //Everyplay.ThumbnailReadyAtFilePath += ThumbnailReadyAtFilePath;
        }
    }

    void Destroy()
    {
        //if (Everyplay != null)
        {
            Everyplay.UploadDidStart -= UploadDidStart;
            Everyplay.UploadDidProgress -= UploadDidProgress;
            Everyplay.UploadDidComplete -= UploadDidComplete;
            Everyplay.RecordingStarted -= RecordingStarted;
            Everyplay.RecordingStopped -= RecordingStopped;

            //Everyplay.SharedInstance.ThumbnailReadyAtFilePath -= ThumbnailReadyAtFilePath;
        }
    }

    public override void ShowUI()
    {
        //Debug.Log("ShowUI");
        Everyplay.Show();
    }

    public override void StopRecording()
    {
        //Debug.Log("StopRecording");
        Everyplay.StopRecording();
    }

    public override void StartRecording()
    {
        //Debug.Log("StartRecording");
        Everyplay.StartRecording();	
    }

    public override void PauseRecording() 
    {
        //Debug.Log("PauseRecording");
        Everyplay.PauseRecording();
        m_IsPaused = true;
    }

    public override void ResumeRecording() 
    {
        //Debug.Log("ResumeRecording");
        Everyplay.ResumeRecording();
        m_IsPaused = false;
    }

    public override void PlayLastRecording()
    {
        Everyplay.PlayLastRecording();
    }

    public override void TakeThumbnail()
    {
        Everyplay.TakeThumbnail();
    }

    public override void ShowSharingUI()
    {
        //Debug.Log("ShowSharingUI");
        Everyplay.ShowSharingModal();
    }

	public override bool IsRecordingSupported ()
	{
		return Everyplay.IsRecordingSupported();
	}

    #region Callbacks
    void RecordingStarted()
    {
        m_IsRecording = true;
        m_IsPaused = false;
        m_IsRecordingFinished = false;
        InvokeEvent(VideoEvents.OnStartedRecording, new VideoData());
    }

    void RecordingStopped()
    {
        m_IsRecording = false;
        m_IsRecordingFinished = true;
        InvokeEvent(VideoEvents.OnStoppedRecording, new VideoData());
    }

    private void UploadDidStart(int videoId)
    {
        InvokeEvent(VideoEvents.OnUploadStarted, new VideoData(videoId));
    }

    private void UploadDidProgress(int videoId, float progress)
    {
        InvokeEvent(VideoEvents.OnUploadProgress, new VideoData(videoId, progress));
        //uploadStatusLabel.text = "Upload " + videoId + " is " + Mathf.RoundToInt((float)progress * 100) + "% completed.";
    }

    private void UploadDidComplete(int videoId)
    {
        InvokeEvent(VideoEvents.OnUploadFinished, new VideoData(videoId));
    }
    #endregion
    #endregion

}
#endif
