﻿using UnityEngine;
using System.Collections;

#if ALLOW_ACE_DEPENDENCIES
public class AceSocialNetworkUserInfo_Twitter : AceSocialNetworkUserInfo
{
    #region Variables
    TwitterUserInfo info;
    #endregion

    #region Properties
    public TwitterStatus status
    {
        get { return info.status; }
    }

    public Texture2D profile_image
    {
        get { return info.profile_image; }
    }

    public Texture2D profile_background
    {
        get { return info.profile_background; }
    }

    public Color profile_background_color
    {
        get { return info.profile_background_color; }
    }

    public Color profile_text_color
    {
        get { return info.profile_text_color; }
    }
    #endregion

    #region Functions
    public AceSocialNetworkUserInfo_Twitter(TwitterUserInfo _info)
        : base(_info.id, _info.name, _info.screen_name, "", _info.screen_name, _info.profile_image_url, "", _info.location, _info.lang)
    {
        info = _info;
    }

    public void LoadProfileImage()
    {
        info.LoadProfileImage();
    }

    public void LoadBackgroundImage()
    {
        info.LoadBackgroundImage();
    }

    public override int GetNumFriends()
    {
        return info.friends_count;
    }

    #endregion
}
#endif