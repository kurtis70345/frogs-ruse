﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public interface IAceLevelBuilder : IEventSystemHandler
{
	void SaveLevel(string path);
	void LoadLevel(string path);
	void LoadLevel(TextAsset levelFile);
}