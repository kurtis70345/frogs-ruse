﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public interface IAceGridBuilderView : IEventSystemHandler 
{
	void UpdateSelectedObject(AceGridObject gridObject);
}
