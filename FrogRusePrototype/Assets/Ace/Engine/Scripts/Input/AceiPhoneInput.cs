using UnityEngine;
using System.Collections;

static public class AceiPhoneInput 
{
	static Vector2 NoPosition = new Vector2(-1, -1);
	static Vector2[] AllTouchPositions = new Vector2[5];
	
	static public bool TouchedScreen()
	{
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).phase == TouchPhase.Began)
			{
				return true;	
			}
		}
			    
		return false;
	}
	
	static public Vector2 GetFirstTouchPosition()
	{
		if (Input.touchCount > 0 /*&& iPhoneInput.GetTouch(0).phase == iPhoneTouchPhase.Began*/)
		{
			return 	Input.GetTouch(0).position;
		}
		
		return NoPosition;
	}
	
	static public Vector2[] GetAllTouchPositions()
	{
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).phase == TouchPhase.Began)
			{
				AllTouchPositions[i] = 	Input.GetTouch(i).position;
			}
		}
		
		return null;
	}
	
	static public Vector2 GetAmountDragged()
	{
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			return 	Input.GetTouch(0).deltaPosition;
		}
		
		return Vector2.zero;
	}
	
	static public Vector2 GetTouchPosition(int index)
	{
		if (Input.touchCount > index && index >= 0)
		{
			return Input.GetTouch(index).position;
		}
		
		return NoPosition;
	}
	
	static public bool StoppedTouching(int index)
	{
		if (Input.touchCount > index && index >= 0)
		{
			if (Input.GetTouch(index).phase == TouchPhase.Ended)
			{
				return true;
			}
		}
		
		return false;
	}
	
	static public bool IsTouchingScreen(int index)
	{
		if (Input.touchCount > index && index >= 0)
		{
			return Input.GetTouch(index).phase == TouchPhase.Moved 
				|| Input.GetTouch(index).phase == TouchPhase.Stationary;
		}
		
		return false;
	}
	
	static public Touch GetTouch(int index)
	{
		return Input.GetTouch(index);
	}
}
