using UnityEngine;
using System.Collections;

static public class AceInput
{
    public enum MouseButtons
    {
        Mouse_Left,
        Mouse_Right,
        Mouse_Middle,
    }  
	
	public const string HorizontalAxis = "Mouse X";
	public const string VerticalAxis = "Mouse Y";
	public static float contentScaleFactor = 1.0F;
	static Vector3 m_MouseAxisDeltaStorage = new Vector3();
		
	
	static public void InitInput()
	{
		contentScaleFactor = PlayerPrefs.GetFloat("PR_ScaleFactor", 1.0f);	
	}
	
    static public bool GetMouseButtonDown(MouseButtons button)
    {
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        return AceiPhoneInput.TouchedScreen();
#else
        return Input.GetMouseButtonDown((int)button);
#endif 
    }
	
	
	static public bool GetMouseButtonPressed(MouseButtons button)
    {
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        return AceiPhoneInput.IsTouchingScreen(0);
#else
        return Input.GetMouseButton((int)button);
#endif
	}
	
	static public Vector3 GetAmountDragged()
    {
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        return AceiPhoneInput.GetAmountDragged();
#else
        m_MouseAxisDeltaStorage.x = Input.GetAxis(HorizontalAxis);
        m_MouseAxisDeltaStorage.y = Input.GetAxis(VerticalAxis);
        return m_MouseAxisDeltaStorage * contentScaleFactor;
#endif
	}
	
    static public Vector3 GetMousePosition(bool invertY)
    {
        Vector3 mousePos;
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        mousePos = AceiPhoneInput.GetFirstTouchPosition();
#else
        mousePos = Input.mousePosition;
#endif

        if (invertY)
        {
            mousePos.y = Screen.height - mousePos.y;
        }

        return mousePos * contentScaleFactor;
    }
	
	static public Vector3 GetMousePosition()
	{
		Vector3 mousePos;

#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        mousePos = AceiPhoneInput.GetFirstTouchPosition();
#else
        mousePos = Input.mousePosition;
#endif	
		return mousePos * contentScaleFactor;
	}
	
}
