﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class AceFileHandler_dat : AceFileHandler
{
    #region Functions
    override public bool Serialize<T>(string filePathAndName, T data)
    {
        bool success = true;

        if (!Directory.Exists(Path.GetDirectoryName(filePathAndName)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePathAndName));
        }

        // you must first open a stream for writing.  
        // In this case, use a file stream.
        FileStream fs = new FileStream(filePathAndName, FileMode.Create);

        // Construct a BinaryFormatter and use it to serialize the data to the stream.
        BinaryFormatter formatter = new BinaryFormatter();
        try
        {
            formatter.Serialize(fs, data);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Failed to serialize. Reason: " + e.Message);
            success = false ;
        }
        finally
        {
            fs.Close();
        }

        return success;
    }

    override public T DeserializeFile<T>(string filePathAndName)
    {
        T binaryData = default(T);

        // Open the file containing the data that you want to deserialize.
        FileStream fs = new FileStream(filePathAndName, FileMode.Open);
        try
        {
            BinaryFormatter formatter = new BinaryFormatter();

            // Deserialize the hashtable from the file and  
            // assign the reference to the local variable.
            binaryData = (T)formatter.Deserialize(fs);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Failed to deserialize. Reason: " + e.Message);
        }
        finally
        {
            fs.Close();
        }

        return binaryData;
    }

    override public T DeserializeFile<T>(TextAsset textAsset)
    {
        return DeserializeBytes<T>(textAsset.bytes);
    }

    override public T Deserialize<T>(string fileData)
    {
        byte[] bytes = Convert.FromBase64String(fileData);
        return DeserializeBytes<T>(bytes);
    }

    protected T DeserializeBytes<T>(byte[] bytes)
    {
        T binaryData = default(T);
        MemoryStream stream = new MemoryStream(bytes);
        try
        {
            BinaryFormatter formatter = new BinaryFormatter();

            // Deserialize the hashtable from the file and  
            // assign the reference to the local variable.
            binaryData = (T)formatter.Deserialize(stream);
        }
        catch (SerializationException e)
        {
            binaryData = default(T);
            Debug.LogError("Failed to deserialize. Reason: " + e.Message);
        }
        finally
        {
            stream.Close();
        }

        return binaryData;
    }

    #endregion
}
