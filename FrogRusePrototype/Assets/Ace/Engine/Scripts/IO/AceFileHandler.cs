﻿using UnityEngine;
using System.Collections;

abstract public class AceFileHandler
{
	#region Functions
	public abstract bool Serialize<T>(string filePathAndName, T data);
	public abstract T DeserializeFile<T>(string filePathAndName);
	public abstract T DeserializeFile<T>(TextAsset textAsset);
	public abstract T Deserialize<T>(string fileData);
	#endregion
}
