﻿using UnityEngine;
using System.Collections;

public class AceFileHandler_json : AceFileHandler
{
	#region Functions
	public override bool Serialize<T>(string filePathAndName, T data)
	{
		return false;
	}
	
	public override T DeserializeFile<T>(string filePathAndName)
	{
		return default(T);
	}
	
	public override T DeserializeFile<T>(TextAsset textAsset)
	{
		return default(T);
	}
	
	public override T Deserialize<T>(string fileData)
	{
		return default(T);
	}
	#endregion
}
