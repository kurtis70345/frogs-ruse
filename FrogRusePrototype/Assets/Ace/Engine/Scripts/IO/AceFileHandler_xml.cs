﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System;
using System.IO;

public class AceFileHandler_xml : AceFileHandler
{
	#region Functions
	public override bool Serialize<T>(string filePathAndName, T data)
	{
		bool success = true;
		XmlSerializer serializer = null;
		TextWriter textWriter = null;
		
		try
		{
			serializer = new XmlSerializer(typeof(T));
    		textWriter = new StreamWriter(filePathAndName);
    		serializer.Serialize(textWriter, data);
		}
		catch (Exception e)
		{
			success = false;
			Debug.LogError(string.Format("Error when serializing. Error: {0}", e.Message));
		}
		finally
		{
			if (textWriter != null)
			{
				textWriter.Close();
			}
		}
		
		return success;
	}
	
	public override T DeserializeFile<T>(string filePathAndName)
	{
		string xml = string.Empty;
		try
		{
#if !UNITY_WEBPLAYER
			xml = File.ReadAllText(filePathAndName);
#else
			Debug.LogError("this functions doesn't work on web");
#endif
		}
		catch (Exception e)
		{
			Debug.LogError(string.Format("Error when opening file {0}. Error: {1}", filePathAndName, e.Message));
			return default(T);
		}
		return Deserialize<T>(xml);
	}
	
	public override T DeserializeFile<T>(TextAsset textAsset)
	{
		return Deserialize<T>(textAsset.text);
	}
	
	public override T Deserialize<T>(string fileData)
	{
		XmlSerializer serializer = null;
    	StringReader stringReader = null;
    	XmlTextReader xmlReader = null;
    	T obj = default(T);
		
		try
		{
			serializer = new XmlSerializer(typeof(T));
	    	stringReader = new StringReader(fileData);
	    	xmlReader = new XmlTextReader(stringReader);
	    	obj = (T)serializer.Deserialize(xmlReader);
		}
		catch (Exception e)
		{
			Debug.LogError(string.Format("Error when deserializing. Error: {0}", e.Message));
		}
		finally
		{
			if (xmlReader != null)
			{
				xmlReader.Close();
			}
			if (stringReader != null)
			{
				stringReader.Close();
			}
		}
		
	    return obj;
	}
	#endregion
}
