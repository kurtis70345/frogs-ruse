using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AceBinaryHeap
{
	const int StartingSize = 50;
	//public delegate bool ShouldPercolateUp(T node1, T node2);
	//public delegate bool ShouldPercolateDown(T node1, T node2);
	List<int> m_Heap;
	
	bool m_bIsMaxHeap = false;
	public bool IsMaxHeap
	{
		get { return m_bIsMaxHeap; }
		internal set { m_bIsMaxHeap = value; } 
	}
	
	public AceBinaryHeap(bool isMaxHeap)
	{
		IsMaxHeap = isMaxHeap;
		m_Heap = new List<int>(StartingSize);
	}
	
	public AceBinaryHeap(bool isMaxHeap, int heapSize)
	{
		IsMaxHeap = isMaxHeap;
		m_Heap = new List<int>(heapSize);
	}
	
	// returns the root value, doesn't remove it
	public int GetFront()
	{
		return m_Heap.Count >= 1 ? m_Heap[0] : 0;
	}
	
	// returns the root value and removes it
	public int RemoveFront()
	{
		int retVal = m_Heap[0];
		
		// switch the value of the last node to the root and remove the last node
		m_Heap[0] = m_Heap[m_Heap.Count - 1];
		m_Heap.RemoveAt(m_Heap.Count - 1);
		
		PercolateDown(0);
		
		return retVal;
	}
	
	public void Insert(int data)
	{
		m_Heap.Add(data);
		PercolateUp(m_Heap.Count - 1); // last index
	}
	
	void PercolateUp(int index)
	{
		int parentIndex = (index - 1) / 2;
		if (parentIndex < 0 || index < 0)	
		{
			return;
		}
		
		if ((IsMaxHeap && m_Heap[parentIndex] < m_Heap[index]) 
		    || (IsMaxHeap && m_Heap[parentIndex] > m_Heap[index]))
		{
			// swap the parent with the child
			Swap(parentIndex, index);
			PercolateUp(parentIndex);
		}
	}
	
	void PercolateDown(int index)
	{
		int leftChildIndex = 2 * index + 1;
		int rightChildIndex = leftChildIndex + 1;
		
		if (leftChildIndex < m_Heap.Count)
		{
			if (IsMaxHeap && m_Heap[index] < m_Heap[leftChildIndex])
			{
				Swap(leftChildIndex, index);
				PercolateDown(leftChildIndex);
			}
			else if (IsMaxHeap && m_Heap[index] > m_Heap[leftChildIndex])
			{
				Swap(leftChildIndex, index);
				PercolateDown(leftChildIndex);
			}
		}
		else if (rightChildIndex < m_Heap.Count)
		{
			if (IsMaxHeap && m_Heap[index] < m_Heap[rightChildIndex])
			{
				Swap(rightChildIndex, index);
				PercolateDown(rightChildIndex);
			}
			else if (IsMaxHeap && m_Heap[index] > m_Heap[rightChildIndex])
			{
				Swap(rightChildIndex, index);
				PercolateDown(rightChildIndex);
			}
		}
	}
	
	void Swap(int index1, int index2)
	{
		int temp = m_Heap[index1];
		m_Heap[index1] = m_Heap[index2];
		m_Heap[index2] = temp;
	}
}
