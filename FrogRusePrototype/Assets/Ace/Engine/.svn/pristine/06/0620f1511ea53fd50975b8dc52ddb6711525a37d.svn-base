﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if ALLOW_ACE_DEPENDENCIES
using UnionAssets.FLE;
#endif

public class AcePlayService
{
    #region Constants
    public enum State
    {
        Connected,
        Disconnected
    }

    public enum PlayServiceEventType
    {
        Player_Connected,
        Player_Disconnected,
        Player_Loaded,
        Achievements_Updated,
        Score_Submitted,
        Leaderboards_Loaded,
        Achievements_Loaded,

		NUM_EVENTS
    }

    public const string HasBeenRatedKey = "HasBeenRated";
    public const string RatingCounterKey = "RatingCounter";
    protected const string RateNowTextKey = "Rate_Now";
    protected const string RateLaterTextKey = "Rate_Later";
    protected const string RateNeverTextKey = "Rate_Never";

    public delegate void OnPlayServiceResultReceived(AcePlayServiceResult result);
	public delegate void OnLeaderboardDataReceived(AceLeaderboard leaderBoard);
	public delegate void OnReceivedAchievements(AceAchievement[] achievements);
	public delegate void OnIncrementedAchievement(AceAchievement achievement);
    #endregion

    #region Variables
    protected State m_CurrentState = State.Disconnected;
	Dictionary<PlayServiceEventType, OnPlayServiceResultReceived> m_PlayServiceResultCallbacks
		= new Dictionary<PlayServiceEventType, OnPlayServiceResultReceived>();
    protected AcePlayServiceGame m_Game;
    #endregion

    #region Functions

    public AcePlayService(AcePlayServiceGame game)
    {
        m_Game = game;
		for (int i = 0; i < (int)PlayServiceEventType.NUM_EVENTS; i++)
		{
			m_PlayServiceResultCallbacks.Add((PlayServiceEventType)i, null);
		}

        StartService();
        SetupCallbacks();
    }

    protected virtual void SetupCallbacks() { }

    public virtual void AddListener(PlayServiceEventType serviceEvent, OnPlayServiceResultReceived cb)
    {
		m_PlayServiceResultCallbacks[serviceEvent] = cb;
    }

    protected virtual void StartService() { }
    public virtual void Connect() { }
    public virtual void Disconnect() { }
    public virtual void DisplayMessage(string title, string message) { }
    public virtual bool IsLoggedIn() { return false; }
    public virtual string ServiceName() { return "UnityEditor"; } 
	public virtual void ShowRatingPopup(string title, string text, string url) { }

    #region UserInfo
    public virtual string GetUserName() { return "Developer"; }
    #endregion

    #region Leaderboards
    public virtual void ShowLeaderboardsUI() { }
    public virtual void ShowLeaderboard(string leaderboardName) { }
    public virtual void SubmitLeaderboardScore(string leaderboardName, int score) { }
    public virtual void LoadLeaderboards() { }
	public virtual void GetLeaderboardData(string leaderboardId, OnLeaderboardDataReceived cb) { }
	public virtual void GetLeaderboardScore (string leaderboardId) { }
    #endregion

    #region Achievements
    public virtual void ShowAchievementsUI() { }
    public virtual void ReportAchievement(string achievementName) { }
    public virtual void IncrementAchievement(string achievementName, int numSteps) { }
	public virtual void IncrementAchievement(string achievementName, int numSteps, OnIncrementedAchievement cb) { }
    public virtual void RevealAchievement(string achievementName) { }
    public virtual void LoadAchievements() { }
	public virtual void GetAllAchievements(OnReceivedAchievements cb) { }
	public virtual bool IsAchievementUnlocked(string achievementName) { return false; }
    #endregion

	#region Callbacks
	protected void InvokeOnPlayerConnected(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Player_Connected], result); }
	protected void InvokeOnPlayerDisconnected(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Player_Disconnected], result); }
	protected void InvokeOnPlayerLoaded(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Player_Loaded], result); }
	protected void InvokeOnAchievementsUpdated(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Achievements_Updated], result); }
	protected void InvokeOnScoreSubmitted(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Score_Submitted], result); }
	protected void InvokeOnLeaderboardsLoaded(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Leaderboards_Loaded], result); }
	protected void InvokeOnAchievementsLoaded(AcePlayServiceResult result) { InvokeCallback(m_PlayServiceResultCallbacks[PlayServiceEventType.Achievements_Loaded], result); }
	
	void InvokeCallback(OnPlayServiceResultReceived cb, AcePlayServiceResult result)
	{
		if (cb != null)
		{
			cb(result);
		}
	}
	
	protected virtual void OnPlayerConnected() { }
	protected virtual void OnPlayerDisconnected() { }
#if ALLOW_ACE_DEPENDENCIES
	protected virtual void OnPlayerInfoLoaded(CEvent e) { }
	protected virtual void OnAchievmentsLoaded(CEvent e) { }
	protected virtual void OnAchievmentUpdated(CEvent e) { }
	protected virtual void OnLeaderBoardsLoaded(CEvent e){ }
	protected virtual void OnScoreSubmited(CEvent e) { }
#endif
	#endregion
    #endregion
}