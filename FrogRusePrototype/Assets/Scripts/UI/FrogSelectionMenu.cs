﻿using UnityEngine;
using System;
using System.Collections;

public class FrogSelectionMenu : AceMenu
{
	#region Variables
	[SerializeField] GameBoard m_GameBoard;
	[SerializeField] Transform m_Layout;
	[SerializeField] AceImage m_SelectionImage;
	Frog[] m_Frogs;
	#endregion

	#region Properties
	public Transform Layout { get { return m_Layout; } }
	#endregion

	#region Functions
	void Start()
	{
		//
	}

	public void DisplayFrogs(int playerId)
	{
		Show ();
		if (m_Frogs == null)
		{
			m_Frogs = m_Layout.GetComponentsInChildren<Frog>();
		}

		Array.ForEach<Frog>(m_Frogs, f => f.gameObject.SetActive(f.OwnerId == playerId || f.transform.parent != m_Layout));
		Frog firstActiveFrog = Array.Find<Frog>(m_Frogs, f => f.gameObject.activeSelf && f.transform.parent == m_Layout);
		if (firstActiveFrog != null)
		{
			//SelectFrog(firstActiveFrog);
			StartCoroutine(WaitToSelect(firstActiveFrog));
		}
	}

	IEnumerator WaitToSelect(Frog frog)
	{
		yield return new WaitForEndOfFrame();
		SelectFrog(frog);
	}

	public void SelectFrog(Frog frog)
	{
		m_SelectionImage.transform.position = frog.transform.position;
		m_GameBoard.SelectGamePiece(frog);
	}
	#endregion
}
