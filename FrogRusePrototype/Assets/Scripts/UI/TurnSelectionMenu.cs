﻿using UnityEngine;
using System.Collections;

public class TurnSelectionMenu : AceMenu
{
	#region Constants
	enum Buttons
	{
		Left,
		Right
	}
	#endregion
	
	#region Variables
	[SerializeField] PlayerTurnController m_TurnController;
	int m_LeftPlayerId;
	int m_RightPlayerId;
	#endregion

	#region Properties
	AceButton LeftButton { get { return m_Buttons[(int)Buttons.Left]; } }
	AceButton RightButton { get { return m_Buttons[(int)Buttons.Right]; } }
	#endregion

	#region Functions
	public void Display(string leftPlayerName, int leftPlayerId, string rightPlayerName, int rightPlayerId)
	{
		LeftButton.Text = AceTranslator.Translate(leftPlayerName);
		RightButton.Text = AceTranslator.Translate(rightPlayerName);
		m_LeftPlayerId = leftPlayerId;
		m_RightPlayerId = rightPlayerId;
		Show();
	}

	public void LeftTeamChosen()
	{
		m_TurnController.SetPlayerTurn(m_LeftPlayerId);
		Hide ();
	}

	public void RightTeamChosen()
	{
		m_TurnController.SetPlayerTurn(m_RightPlayerId);
		Hide ();
	}
	#endregion
}
