﻿using UnityEngine;
using System.Collections;

public class PlayerActionMenu : AceMenu 
{
	#region Constants
	enum Buttons
	{
		Push,
		Climb,
		Prison_Break,
		Reroll
	}
	#endregion

	#region Properties
	AceButton PushButton { get { return m_Buttons[(int)Buttons.Push]; } }
	AceButton ClimbButton { get { return m_Buttons[(int)Buttons.Climb]; } }
	AceButton PrisonBreakButton { get { return m_Buttons[(int)Buttons.Prison_Break]; } }
	AceButton RerollButton { get { return m_Buttons[(int)Buttons.Reroll]; } }
	#endregion

	#region Variables
	[SerializeField] FrogsRusePlayerActionHandler m_ActionHandler;
	//GamePiece m_SelectedPiece;
	#endregion

	#region Functions
	// Use this for initialization
	public void SetAvailableActions(GamePiece piece, bool isPushAllowed, bool isClimbAllowed,
	                                bool isPrisonBreakAllowed, bool isRerollAllowed)
	{
		//m_SelectedPiece = piece;
		PushButton.gameObject.SetActive(isPushAllowed);
		ClimbButton.gameObject.SetActive(isClimbAllowed);
		PrisonBreakButton.gameObject.SetActive(isPrisonBreakAllowed);
		RerollButton.gameObject.SetActive(isRerollAllowed);
		Show();
	}

	public void Push()
	{
        m_ActionHandler.SetActionChosen(FrogsRusePlayerActionHandler.ActionType.Push);
	}

	public void Reroll()
	{
        m_ActionHandler.SetActionChosen(FrogsRusePlayerActionHandler.ActionType.Reroll);
    }

	public void Climb()
	{
        m_ActionHandler.SetActionChosen(FrogsRusePlayerActionHandler.ActionType.Climb);
    }

	public void PrisonBreak()
	{
        m_ActionHandler.SetActionChosen(FrogsRusePlayerActionHandler.ActionType.PrisonBreak);
    }
	#endregion
}
