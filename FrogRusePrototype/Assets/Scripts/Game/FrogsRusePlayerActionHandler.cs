using UnityEngine;
using System.Collections;

public class FrogsRusePlayerActionHandler : PlayerActionHandler 
{
    #region Constants
    public enum ActionType
    {
        Push,
        Climb,
        PrisonBreak,
        Reroll
    }
    #endregion

    #region Variables
    [SerializeField] ActionController m_ActionController;
	[SerializeField] PlayerActionMenu m_ActionMenu;
	[SerializeField] GameStateController m_GameStateController;
    ActionType m_LastActionChosen;
    #endregion

    #region Properties
    Pond Pond {  get { return m_GameBoard as Pond; } }
    #endregion

    #region Functions
    // Use this for initialization
    void Start () 
	{
	
	}

	public override void SelectBoardSpace (GameBoardSpace space)
	{
		Pond.SelectGameBoardSpace(space);
		if (m_GameStateController.CurrentGameState == GameStateController.GameState.Selecting_Piece)
		{
			GamePiece occupyingPiece = space.GetOccupyingPiece();
			m_GameBoard.SelectGamePiece(occupyingPiece);
			m_ActionMenu.SetAvailableActions(occupyingPiece,
			                                 m_ActionController.IsPushAllowed(occupyingPiece, space),
			                                 m_ActionController.IsClimbAllowed(occupyingPiece, space),
			                                 m_ActionController.IsPrisonBreakAllowed(occupyingPiece, space),
			                                 m_ActionController.IsRerollAllowed(occupyingPiece, space));
		}
		else if (m_GameStateController.CurrentGameState == GameStateController.GameState.Placing_Pieces)
		{
			//m_GameBoard.MovePiece(m_GameBoard.SelectedPiece, space);
			space.OccupySpace(m_GameBoard.SelectedPiece);
			m_GameStateController.AdvanceToNextTurn();
		}
		else if (m_GameStateController.CurrentGameState == GameStateController.GameState.Selecting_Action_Target)
		{
            switch (m_LastActionChosen)
            {
                case ActionType.Push:
                    Push(Pond.SelectedPiece);
                    break;

                case ActionType.Climb:
                    Climb(Pond.SelectedPiece);
                    break;
            }
		}
	}

    public void SetActionChosen(ActionType action)
    {
        m_LastActionChosen = action;
		if (action == ActionType.Reroll)
		{
			Reroll(Pond.SelectedPiece);
		}
		else if (action == ActionType.PrisonBreak)
		{
			PrisonBreak(Pond.SelectedPiece);
		}
		else
		{
			m_GameStateController.ActionChosen(action);
		}
    }

	public void Push(GamePiece piece)
	{
		GameBoardSpace startingLocation = Pond.GetPieceLocation(piece);
		int neighborIndex = startingLocation.GetNeighborIndex(m_GameBoard.SelectedSpace);
		if (neighborIndex == -1)
		{
			Debug.LogError(string.Format("{0} isn't a neighbor of {1}", m_GameBoard.SelectedSpace.name, startingLocation.name));
		}
		else
		{
			Pond.Push(startingLocation, (Pond.NeighborDirection)neighborIndex);
		}

		m_GameStateController.AdvanceToNextTurn();
	}

	public void Climb(GamePiece piece)
	{
		GameBoardSpace currentSpace = m_GameBoard.GetPieceLocation(piece);
		currentSpace.VacateSpace(piece);
		Pond.SelectedSpace.OccupySpace(piece);
		m_GameStateController.AdvanceToNextTurn();
	}

    public void PrisonBreak(GamePiece piece)
    {
        // gets it out of the list
		Pond.SelectedSpace.VacateSpace(piece);

        // puts it on top
		Pond.SelectedSpace.OccupySpace(piece);

        m_GameStateController.AdvanceToNextTurn();
    }

    public void Reroll(GamePiece piece)
    {
        piece.RerollPieceValue(6);
        m_GameStateController.AdvanceToNextTurn();
    }

	#endregion
}
