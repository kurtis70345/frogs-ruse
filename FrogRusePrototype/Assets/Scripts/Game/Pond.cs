﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Pond : GameBoard
{
    #region Constants
    const int LargestRow = 7;
    const int SmallestRow = 4;
    const int NumRows = 7;

    public enum NeighborDirection
    {
        North_West,
        North_East,
        East,
        South_East,
        South_West,
        West,

        NUM_DIRECTIONS
    }
    #endregion

    #region Variables
    [SerializeField] FrogSelectionMenu m_FrogSelectionMenu;
	[SerializeField] AceLabel m_InfoLabel;
    #endregion

    #region Functions
    // Use this for initialization

    protected override void ConnectGameSpaces()
    {
		Array.Sort<GameBoardSpace>(m_GameBoardSpaces, (a,b) => string.Compare(a.name, b.name));
        int currentRow = 0;
        int numSpacesInCurrentRow = SmallestRow; // we start in bottom left corner
        int currIndexInRow = 0;
        int nwIndex = 0;
        int neIndex = 0;
        int swIndex = 0;
        int seIndex = 0;

        int firstIndexInRow = 0;
        int lastIndexInRow = numSpacesInCurrentRow - 1;

        for (int i = 0; i < m_GameBoardSpaces.Length; i++)
        {
            GameBoardSpace currSpace = m_GameBoardSpaces[i];

            bool atOrPastMidRow = currentRow >= NumRows / 2;
			if (/*currentRow > NumRows / 2*/i > m_GameBoardSpaces.Length / 2)
            {
                nwIndex = i + numSpacesInCurrentRow - 1;
                neIndex = i + numSpacesInCurrentRow;
                swIndex = i - numSpacesInCurrentRow - 1;
                seIndex = i - numSpacesInCurrentRow;
            }
            else
            {
                nwIndex = i + numSpacesInCurrentRow;
                neIndex = i + numSpacesInCurrentRow + 1;
                swIndex = i - numSpacesInCurrentRow;
                seIndex = i - numSpacesInCurrentRow + 1;
            }

			if (nwIndex > lastIndexInRow && nwIndex < m_GameBoardSpaces.Length && m_GameBoardSpaces[nwIndex].SpaceId == currSpace.SpaceId + 1)
            {
                currSpace.SetNeighbor((int)NeighborDirection.North_West, m_GameBoardSpaces[nwIndex]);
            }

			if (neIndex > lastIndexInRow && neIndex < m_GameBoardSpaces.Length && m_GameBoardSpaces[neIndex].SpaceId == currSpace.SpaceId + 1)
            {
                currSpace.SetNeighbor((int)NeighborDirection.North_East, m_GameBoardSpaces[neIndex]);
            }

            //if (currIndexInRow < numSpacesInCurrentRow - 1)
			if (i < lastIndexInRow && m_GameBoardSpaces[i + 1].SpaceId == currSpace.SpaceId)
            {
                currSpace.SetNeighbor((int)NeighborDirection.East, m_GameBoardSpaces[i + 1]);
            }

			if (seIndex < firstIndexInRow && seIndex >= 0 && m_GameBoardSpaces[seIndex].SpaceId == currSpace.SpaceId - 1)
            {
				currSpace.SetNeighbor((int)NeighborDirection.South_East, m_GameBoardSpaces[seIndex]);
            }

			if (swIndex < firstIndexInRow && swIndex >= 0 && m_GameBoardSpaces[swIndex].SpaceId == currSpace.SpaceId - 1)
            {
				currSpace.SetNeighbor((int)NeighborDirection.South_West, m_GameBoardSpaces[swIndex]);
            }
            
            //if (currIndexInRow > 0)
			if (i > firstIndexInRow && m_GameBoardSpaces[i - 1].SpaceId == currSpace.SpaceId)
            {
                currSpace.SetNeighbor((int)NeighborDirection.West, m_GameBoardSpaces[i - 1]);
            }

            
			if (currIndexInRow == numSpacesInCurrentRow - 1)
            {
                currentRow += 1;
				if (atOrPastMidRow)
                {
                    numSpacesInCurrentRow -= 1;
                }
                else
                {
                    numSpacesInCurrentRow += 1;
                }
                currIndexInRow = 0;

                firstIndexInRow = i + 1;
                lastIndexInRow = firstIndexInRow + numSpacesInCurrentRow - 1;
            }
			else
			{
				currIndexInRow += 1;
			}
        }
    }

    /*int GetNumSpacesInRow(int rowNum, int smallestRow, int largestRow, int totalNumRows)
    {

    }*/


    public override void ResetBoard()
    {
        base.ResetBoard();
        foreach (GameBoardSpace space in m_GameBoardSpaces)
        {
            space.VacateSpace();
        }

        foreach (GamePiece piece in m_GamePieces)
        {
            piece.transform.SetParent(m_FrogSelectionMenu.Layout);
        }
    }

    public void EnableEligiblePlacementSpaces(int playerId)
    {
        foreach (GameBoardSpace space in m_GameBoardSpaces)
        {
            GameBoardSpace[] neighbors = space.GetNeighborsOccupiedByPlayer(playerId);
			if (neighbors != null && neighbors.Length > 0)
			{
				int x = 10;
			}
			space.SetActive(!space.IsOccupied && (space.IsEdgeSpace() || neighbors.Length > 0));
        }
    }

	public void EnableEligibleClimbingSpaces(GameBoardSpace space)
	{
		List<GameBoardSpace> neighbors = GetHigherAndEqualValueNeighbors(space);
		foreach (GameBoardSpace neighbor in neighbors)
		{
			if (neighbor != null)
			{
				neighbor.SetActive(true);
			}
		}
	}

	public void EnableEligibleMovingSpaces(GameBoardSpace space)
	{
		//foreach (GameBoardSpace neighbor in space.Neighbors)
		for (int i = 0; i < (int)NeighborDirection.NUM_DIRECTIONS; i++)
		{
			GameBoardSpace neighbor = space.GetNeighbor(i);
			if (neighbor == null)
			{
				continue;
			}

			neighbor.SetActive(!neighbor.IsOccupied || space.SpaceValue >= GetPushValue(neighbor, (NeighborDirection)i));
		}
	}

	int GetPushValue(GameBoardSpace space, NeighborDirection pushDirection)
	{
		int pushVal = 0;

		GameBoardSpace currSpace = space;
		while (currSpace != null)
		{
			if (!currSpace.IsOccupied)
			{
				break;
			}

			pushVal += currSpace.SpaceValue;

			currSpace = currSpace.GetNeighbor((int)pushDirection);
		}

		return pushVal;
	}

	public void Push(GameBoardSpace pushingFrom, NeighborDirection pushDirection)
	{
		if (pushingFrom == null)
		{
			Debug.LogError("Push Error");
			return;
		}
		List<GamePiece> movingPieces = pushingFrom.OccupyingPieces;
		List<GamePiece> nextMovingPieces = pushingFrom.OccupyingPieces;

		GameBoardSpace pushingTo = null;
		do
		{
			// take of the pieces off the from and store them
			//movingPieces = pushingFrom.OccupyingPieces;
			movingPieces = nextMovingPieces;
			pushingFrom.VacateSpace();

			// store the pieces from
			pushingTo = pushingFrom.GetNeighbor((int)pushDirection);
			if (pushingTo == null)
			{
				// pieces have fallen off the board, check who won
				CheckWinner(movingPieces);
				break;
			}
			else
			{
				nextMovingPieces = pushingTo.OccupyingPieces;
				pushingTo.VacateSpace();
				pushingTo.OccupySpace(movingPieces);

				if (nextMovingPieces == null | nextMovingPieces.Count == 0)
				{
					// no more pieces to push
					break;
				}
				else
				{
					pushingFrom = pushingTo;

				}
			}

		}
		while (pushingFrom != null && nextMovingPieces != null && nextMovingPieces.Count > 0);
	}

	void CheckWinner(List<GamePiece> fallenPieces)
	{
		int fallenPiecesTeam1 = 0;
		int fallenPiecesTeam2 = 0;
		for (int i = 0; i < fallenPieces.Count; i++)
		{
			if (fallenPieces[i].OwnerId == 0 || fallenPieces[i].OwnerId == 2)
			{
				fallenPiecesTeam1 += 1;
			}
			else
			{
				fallenPiecesTeam2 += 1;
			}
		}

		m_InfoLabel.Text = string.Format("WINNER: {0}", fallenPiecesTeam1 > fallenPiecesTeam2 ? "Team 2" : "Team 1");
	}

	#endregion
}
