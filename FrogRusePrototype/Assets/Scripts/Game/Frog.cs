﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Frog : GamePiece 
{
	#region Variables
	[SerializeField] AceImage m_FrogImage;
	[SerializeField] AceLabel m_ValueText;
	[SerializeField] EventTrigger m_EventTrigger;
	#endregion

	#region Functions
	override public void Start()
	{

	}

	public override void RerollPieceValue (int numDieSides)
	{
		base.RerollPieceValue (numDieSides);
		m_ValueText.Text = PieceValue.ToString();
	}

	public override void SetPiecePlaced (bool isPlaced)
	{
		base.SetPiecePlaced (isPlaced);
		m_FrogImage.IsRayCastTarget = !isPlaced;
		m_EventTrigger.enabled = !isPlaced;
	}
	#endregion
}
