﻿using UnityEngine;
using System.Collections;

public class ActionController : MonoBehaviour
{
	#region Variables
	[SerializeField] GameBoard m_GameBoard;
	[SerializeField] PlayerTurnController m_TurnController;
	#endregion

	#region Functions
	public bool IsPushAllowed(GamePiece piece, GameBoardSpace space)
	{
		return true;
	}

	public bool IsClimbAllowed(GamePiece piece, GameBoardSpace space)
	{
		return space.IsOccupied && m_GameBoard.GetHigherAndEqualValueNeighbors(space) != null;
	}

	public bool IsPrisonBreakAllowed(GamePiece piece, GameBoardSpace space)
	{
		return space.IsPieceOccupying(piece) && space.GetOccupyingPiece() != piece && m_GameBoard.GetPlayerOwnedSpaces(piece.OwnerId).Count == 0;
	}

	public bool IsRerollAllowed(GamePiece piece, GameBoardSpace space)
	{
		return space.IsOccupied && m_GameBoard.IsAllPiecesPlaced();
	}
	#endregion
}
