﻿using UnityEngine;
using System.Collections;

public class LilyPad : GameBoardSpace
{
	#region Variables
	[SerializeField] float m_FrogStackPixelOffset = 5;
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{

	}

	public override void OccupySpace (GamePiece occupyingPiece)
	{
		base.OccupySpace (occupyingPiece);
		SetPiecePositions();
	}

	public override void VacateSpace ()
	{
		base.VacateSpace ();
		SetHighlighted(false, false);
		SetPiecePositions();
	}

	public override void VacateSpace (GamePiece vacatingPiece)
	{
		base.VacateSpace (vacatingPiece);
		SetPiecePositions();
	}

	void SetPiecePositions()
	{
		for (int i = 0; i < m_OccupyingPieces.Count; i++)
		{
			m_OccupyingPieces[i].transform.SetParent(transform);
			m_OccupyingPieces[i].transform.localPosition = new Vector3(m_FrogStackPixelOffset * i, 0, 0);
		}
	}

	public override void SetActive (bool active)
	{
		base.SetActive (active);
		m_GameSpaceToggle.SetCheckedWithoutCallback(false);
	}
    #endregion
}
