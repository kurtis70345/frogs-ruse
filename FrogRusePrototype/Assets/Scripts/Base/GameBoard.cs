﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameBoard : MonoBehaviour 
{
	#region Variables
	protected GameBoardSpace[] m_GameBoardSpaces;
	protected GamePiece[] m_GamePieces;
	protected GamePiece m_SelectedPiece;
	protected GameBoardSpace m_SelectedSpace;
	#endregion

	#region Properties
	public IEnumerable GameBoardSpaces { get { return m_GameBoardSpaces; } }
	public IEnumerable GamePieces { get { return m_GamePieces; } }
	public GamePiece SelectedPiece { get { return m_SelectedPiece; } }
	public GameBoardSpace SelectedSpace { get { return m_SelectedSpace; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Awake()
	{
		m_GameBoardSpaces = FindObjectsOfType<GameBoardSpace>();
		m_GamePieces = FindObjectsOfType<GamePiece>();
	}

	public virtual void Start ()
    {
        ConnectGameSpaces();
    }

    protected virtual void ConnectGameSpaces()
    {

    }

	public void SetAllBoardSpacesActive(bool active)
	{
		foreach (GameBoardSpace space in m_GameBoardSpaces)
		{
			space.SetActive(active);
		}
	}

	public List<GameBoardSpace> GetLowerValueNeighbors(GameBoardSpace space)
	{
		List<GameBoardSpace> lowerNeighbors = new List<GameBoardSpace>();
		foreach (GameBoardSpace neighbor in space.Neighbors)
		{
			if (neighbor != null && space.IsOccupied && space.SpaceValue > neighbor.SpaceValue)
			{
				lowerNeighbors.Add(neighbor);
			}

		}
		return lowerNeighbors;
	}

	public List<GameBoardSpace> GetHigherAndEqualValueNeighbors(GameBoardSpace space)
	{
		List<GameBoardSpace> higherNeighbors = new List<GameBoardSpace>();
		foreach (GameBoardSpace neighbor in space.Neighbors)
		{
			if (neighbor != null && space.IsOccupied && space.SpaceValue < neighbor.SpaceValue)
			{
				higherNeighbors.Add(neighbor);
			}
		}
		return higherNeighbors;
	}

	GamePiece[] GetPlayerPieces(int playerId)
	{
		return Array.FindAll<GamePiece>(m_GamePieces, gp => gp.OwnerId == playerId);
	}

	public bool IsAllPiecesPlaced(int playerId)
	{
		GamePiece[] playerPieces = GetPlayerPieces(playerId);
		return Array.TrueForAll<GamePiece>(playerPieces, gp => gp.IsPlaced);
	}

	public bool IsAllPiecesPlaced()
	{
		return Array.TrueForAll<GamePiece>(m_GamePieces, p => p.IsPlaced);
	}

	public void SelectGamePiece(GamePiece piece)
	{
		m_SelectedPiece = piece;
	}

	public void SelectGameBoardSpace(GameBoardSpace space)
	{
		m_SelectedSpace = space;
	}

	public virtual void MovePiece(GamePiece piece, GameBoardSpace targetSpace)
	{
		GameBoardSpace currentSpace = GetPieceLocation(piece);
		if (currentSpace != null)
		{
			currentSpace.VacateSpace(piece);
		}
		targetSpace.OccupySpace(piece);
	}

	public GameBoardSpace GetPieceLocation(GamePiece piece)
	{
		foreach (GameBoardSpace space in m_GameBoardSpaces)
		{
			if (space.IsPieceOccupying(piece))
			{
				return space;
			}
		}
		return null;
	}

	public virtual void ResetBoard()
	{

	}

	public virtual void EnablePlayerOwnedSpaces(int playerId)
	{
		foreach (GameBoardSpace space in m_GameBoardSpaces)
		{
			space.SetActive(space.IsMostRecentOccupant(playerId));
		}
	}

	public virtual List<GameBoardSpace> GetPlayerOwnedSpaces(int playerId)
	{
		List<GameBoardSpace> ownedSpaces = new List<GameBoardSpace>();
		foreach (GameBoardSpace space in m_GameBoardSpaces)
		{
			if (space.IsOccupied && space.IsMostRecentOccupant(playerId))
			{
				ownedSpaces.Add(space);
			}
		}
		return ownedSpaces;
	}
	#endregion
}
