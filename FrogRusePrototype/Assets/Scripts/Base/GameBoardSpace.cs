﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameBoardSpace : MonoBehaviour 
{
	#region Variables
	[SerializeField] int m_SpaceId;
	[SerializeField] protected GameBoardSpace[] m_ConnectedSpaces;
	[SerializeField] protected List<GamePiece> m_OccupyingPieces = new List<GamePiece>();
	bool m_IsOccupied = false;
	protected AceToggle m_GameSpaceToggle;
	#endregion

	#region Properties
	public int SpaceId
	{
		get { return m_SpaceId; }
		set { m_SpaceId = value; }
	}

	public bool IsOccupied
	{
		get { return m_IsOccupied; }
	}

	public int SpaceValue
	{
		get { return IsOccupied ? GetOccupyingPiece().PieceValue : 0; }
	}

	public IEnumerable<GameBoardSpace> Neighbors { get { return m_ConnectedSpaces; } }
	public List<GamePiece> OccupyingPieces { get { return new List<GamePiece>(m_OccupyingPieces) ; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Awake () 
	{
		m_GameSpaceToggle = GetComponent<AceToggle>();
	}

	public virtual void OccupySpace(GamePiece occupyingPiece)
	{
		m_IsOccupied = true;
		m_OccupyingPieces.Add(occupyingPiece);
		occupyingPiece.SetPiecePlaced(true);
	}

	public virtual void OccupySpace(IEnumerable<GamePiece> pieces)
	{
		foreach (GamePiece piece in pieces)
		{
			OccupySpace(piece);
		}
	}

	public virtual void VacateSpace()
	{
		m_IsOccupied = false;
		for (int i = 0; i < m_OccupyingPieces.Count; i++)
		{
			m_OccupyingPieces[i].SetPiecePlaced(false);
		}
		m_OccupyingPieces.Clear();
	}

	public virtual void VacateSpace(GamePiece vacatingPiece)
	{
		m_OccupyingPieces.Remove(vacatingPiece);
		m_IsOccupied = m_OccupyingPieces.Count > 0;
		vacatingPiece.SetPiecePlaced(false);
	}

	public virtual GamePiece GetOccupyingPiece()
	{
		GamePiece gamePiece = null;
		if (IsOccupied)
		{
			gamePiece = m_OccupyingPieces[m_OccupyingPieces.Count - 1];
		}
		return gamePiece;
	}

	public virtual void SetHighlighted(bool isHighlighted, bool useUICallbacks)
	{
		if (useUICallbacks)
		{
			m_GameSpaceToggle.IsChecked = isHighlighted;
		}
		else
		{
			m_GameSpaceToggle.SetCheckedWithoutCallback(isHighlighted);
		}
	}

	public bool IsPieceOccupying(GamePiece piece)
	{
		return m_OccupyingPieces.Contains(piece);
	}

    public bool IsMostRecentOccupant(int playerId)
    {
        return IsOccupied && m_OccupyingPieces[m_OccupyingPieces.Count - 1].OwnerId == playerId;
    }


    public bool IsEdgeSpace()
    {
        foreach (GameBoardSpace space in m_ConnectedSpaces)
        {
            if (space == null)
            {
                return true;
            }
        }
        return false;
    }

    virtual public void SetActive(bool active)
    {
        m_GameSpaceToggle.IsInteractable = active;
    }

    public virtual GameBoardSpace[] GetNeighborsOccupiedByPlayer(int playerId)
    {
        return Array.FindAll<GameBoardSpace>(m_ConnectedSpaces, cs => cs != null && cs.IsMostRecentOccupant(playerId));
    }

    public void SetNeighbor(int index, GameBoardSpace neighbor)
    {
        m_ConnectedSpaces[index] = neighbor;
    }

	public GameBoardSpace GetNeighbor(int index)
	{
		return m_ConnectedSpaces[index];
	}

	public int GetNeighborIndex(GameBoardSpace neighbor)
	{
		return Array.FindIndex<GameBoardSpace>(m_ConnectedSpaces, cs => cs == neighbor);
	}
	#endregion
}
