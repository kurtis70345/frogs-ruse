﻿using UnityEngine;
using System.Collections;

public class GamePiece : MonoBehaviour 
{
	#region Variables
	[SerializeField] int m_PieceValue;
	[SerializeField] int m_OwnerId;
	bool m_IsPlaced;

	Vector3 m_StartingPosition;
	Transform m_StartingParent;
	#endregion

	#region Properties
	public int PieceValue
	{
		get { return m_PieceValue; }
		set { m_PieceValue = value; } 
	}

	public int OwnerId
	{
		get { return m_OwnerId; }
		set { m_OwnerId = value; }
	}

	public bool IsPlaced
	{
		get { return m_IsPlaced; }
		set { m_IsPlaced = value; }
	}
	#endregion

	#region Functions
	// Use this for initialization
	public virtual void Start ()
	{
		m_StartingPosition = transform.position;
		m_StartingParent = transform.parent;
	}

	virtual public void RerollPieceValue(int numDieSides)
	{
		PieceValue = Random.Range(1, numDieSides + 1);
	}

	public void ResetPosition()
	{
		m_IsPlaced = false;
		transform.position = m_StartingPosition;
		transform.parent = m_StartingParent;
	}

	public virtual void SetPiecePlaced(bool isPlaced)
	{
		m_IsPlaced = isPlaced;
	}
	#endregion
}
