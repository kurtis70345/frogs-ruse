﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerTurnController : MonoBehaviour
{
	#region Constants
	[System.Serializable]
	public class Player
	{
		public int m_PlayerId;
		public int m_TeamId;
		public string m_Name = "";
		int m_NumTurnsTaken;

		public int NumTurnsTaken
		{
			get { return m_NumTurnsTaken; }
			set { m_NumTurnsTaken = value; }
		}
	}
	#endregion

	#region Variables;
	[SerializeField] GameStateController m_GameStateController;
	[SerializeField] TurnSelectionMenu m_TurnMenu;
	//[SerializeField] int m_NumPlayers = 4;
	[SerializeField] int m_NumTeams = 2;
	[SerializeField] int m_PiecesPerPlayer = 1;
	[SerializeField] Player[] m_Players;

	int[] m_TurnOrder; // team ids
	int m_CurrentTeamTurn; // index into m_TurnOrder
	int m_CurrentPlayerTurn;
	#endregion

	#region Properties
	public int NumPlayers { get { return m_TurnOrder.Length; } }
	public int CurrentTeamTurn { get { return m_CurrentTeamTurn; } }
	public int CurrentPlayerTurn { get { return m_CurrentPlayerTurn; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		
	}

	public virtual void SetupTurnOrder()
	{
		m_TurnOrder = new int[m_NumTeams];
		/*for (int i = 0; i < m_TurnOrder.Length; i++)
		{
			m_TurnOrder[i] = i;
		}
		m_TurnOrder.Shuffle();*/
		m_TurnOrder[0] = UnityEngine.Random.Range(0, 2);
		m_TurnOrder[1] = m_TurnOrder[0] == 1 ? 0 : 1;

		for (int i = 0; i < m_Players.Length; i++)
		{
			m_Players[i].NumTurnsTaken = 0;
		}

		m_CurrentTeamTurn = 0;
		ShowTurnMenu();
	}

	void ShowTurnMenu()
	{
		Player[] players = GetTeamPlayers();
		m_TurnMenu.Display(players[0].m_Name, players[0].m_PlayerId, players[1].m_Name, players[1].m_PlayerId);
	}

	/// <summary>
	///  returns the id of the team whos turn it will be
	/// </summary>
	/// <returns>The to next turn.</returns>
	public int AdvanceToNextTurn()
	{
		m_CurrentTeamTurn += 1;
		m_CurrentTeamTurn %= m_TurnOrder.Length;
		return m_CurrentTeamTurn;
	}

	public bool IsEqualNumTurnsForTeammates()
	{
		Player[] teamPlayers = GetTeamPlayers();
		return teamPlayers[0].NumTurnsTaken == teamPlayers[1].NumTurnsTaken;
	}

	public int GetPlayerTurn()
	{
		Player[] teamPlayers = GetTeamPlayers();
		return teamPlayers[0].NumTurnsTaken > teamPlayers[1].NumTurnsTaken
			? teamPlayers[1].m_PlayerId : teamPlayers[0].m_PlayerId;
	}

	public Player[] GetTeamPlayers()
	{
		return Array.FindAll<Player>(m_Players, p => p.m_TeamId == m_CurrentTeamTurn);
	}

	public Player[] GetTeamPlayers(int teamId)
	{
		return Array.FindAll<Player>(m_Players, p => p.m_TeamId == teamId);
	}

	Player GetPlayer(int playerId)
	{
		return Array.Find<Player>(m_Players, p => p.m_PlayerId == playerId);
	}

	public void CalculatePlayerTurn()
	{
		Player[] teamPlayers = GetTeamPlayers();
		m_TurnMenu.Display(teamPlayers[0].m_Name, teamPlayers[0].m_PlayerId, 
		                   teamPlayers[1].m_Name, teamPlayers[1].m_PlayerId);
	}

	public void SetPlayerTurn(int playerId)
	{
		m_CurrentPlayerTurn = playerId;
		Player player = GetPlayer(playerId);
		player.NumTurnsTaken += 1;
		m_GameStateController.StartPlayerTurn(playerId);
	}

	#endregion
}
