using UnityEngine;
using System.Collections;

public class GameStateController : MonoBehaviour 
{
	#region Constants
	public enum GameState
	{
		Selecting_Team,
		Placing_Pieces, // first 4 turns
		Selecting_Piece,
		Selecting_Action_Target,
	}

	enum GameMenus
	{
		Action_Selection,
		Turn_Selection,
		Frog_Selection,
	}
	#endregion

	#region Variables
	[SerializeField] Pond m_GameBoard;
	[SerializeField] PlayerTurnController m_TurnController;
	[SerializeField] AceMenu[] m_GameMenus;
	GameState m_CurrentState = GameState.Selecting_Team;
	#endregion

	#region Properties
	public GameState CurrentGameState { get { return m_CurrentState; } } 
	FrogSelectionMenu FrogSelectionMenu { get { return m_GameMenus[(int)GameMenus.Frog_Selection] as FrogSelectionMenu; } }
	TurnSelectionMenu TurnSelectionMenu { get { return m_GameMenus[(int)GameMenus.Turn_Selection] as TurnSelectionMenu; } }
	PlayerActionMenu ActionSelectionMenu { get { return m_GameMenus[(int)GameMenus.Action_Selection] as PlayerActionMenu; } }
    
	#endregion

	#region Functions
	// Use this for initialization
	void Start () 
	{
		SetupBoardGame();
	}

	void SetupBoardGame()
	{
		m_GameBoard.ResetBoard();

		// roll 5 frogs for each player
		foreach (GamePiece piece in m_GameBoard.GamePieces)
		{
			//piece.ResetPosition();
			piece.RerollPieceValue(6);
		}

		HideAllMenus();

		// decide which team goes first
		m_TurnController.SetupTurnOrder();
	}

	void HideAllMenus()
	{
		for (int i = 0; i < m_GameMenus.Length; i++)
		{
			m_GameMenus[i].Hide();
		}
	}

	public void AdvanceToNextTurn()
	{
		m_GameBoard.SetAllBoardSpacesActive(false);
		HideAllMenus();
		m_TurnController.AdvanceToNextTurn();
		if (m_TurnController.IsEqualNumTurnsForTeammates())
		{
			m_TurnController.CalculatePlayerTurn();
			m_CurrentState = GameState.Selecting_Team;
		}
		else
		{
			m_TurnController.SetPlayerTurn(m_TurnController.GetPlayerTurn());
		}
	}

	public void StartPlayerTurn(int playerId)
	{
		if (!m_GameBoard.IsAllPiecesPlaced(playerId))
		{
			FrogSelectionMenu.DisplayFrogs(playerId);
			m_CurrentState = GameState.Placing_Pieces;
            m_GameBoard.EnableEligiblePlacementSpaces(playerId);
        }
		else
		{
			//PlayerActionMenu.
			m_CurrentState = GameState.Selecting_Piece;
			m_GameBoard.EnablePlayerOwnedSpaces(playerId);
		}
	}

	public void ActionChosen(FrogsRusePlayerActionHandler.ActionType action)
    {
        m_CurrentState = GameState.Selecting_Action_Target;
		m_GameBoard.SetAllBoardSpacesActive(false);

		// enable 
		switch (action)
		{
		case FrogsRusePlayerActionHandler.ActionType.Push:
			m_GameBoard.EnableEligibleMovingSpaces(m_GameBoard.SelectedSpace);
			break;

		case FrogsRusePlayerActionHandler.ActionType.Climb:
			m_GameBoard.EnableEligibleClimbingSpaces(m_GameBoard.SelectedSpace);
			break;

		}
    }
	#endregion
}
